#!/usr/bin/env bash

set -e

echo "(1/6) Running lint on Angular Project..."
cd axon-ui
yarn install
yarn run lint

echo "(2/6) Running lint on Nest Project..."
cd ../axon-server
yarn install
yarn run lint

echo "(3/6) Running unit tests on Angular Project..."
cd ../axon-ui
yarn run test

echo "(4/6) Running unit tests on Nest Project..."
cd ../axon-server
yarn run test

echo "(5/6) Running e2e tests on Angular Project..."
cd ../axon-ui
yarn run e2e
