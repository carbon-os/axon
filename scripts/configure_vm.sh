#!/usr/bin/env bash

set -e

echo "Copying linked model dir from server to client..."
rm axon-ui/src/model/types
cp -r axon-server/src/model/types axon-ui/src/model