#!/usr/bin/env bash

set -e

echo "Checking S3 for files"
aws s3 ls carbon-os.voice.guess-game/prod/ | while read line; do echo "insert into audio_file (uuid, createdBy, lastModifiedBy, version, entityStatus,  mimeType, size, file_key, fileName, classification, description) values (uuid(), 'system', 'system', 1, 0, 'audio/wav', 105000, '$line', '$line', 1, 'This is a human reading an audio book.');"; done

