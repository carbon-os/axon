#!/usr/bin/env bash

set -e

cd axon-ui

echo "(1/8) Building Angular project..."
cd axon-ui
yarn install
yarn run build

echo "(2/8) Building Nest project..."
echo "skipping Nest build... see open ticket"
#cd ../axon-server TODO:axon-2
#yarn run build

echo "(3/8) Running lint on Angular Project..."
cd ../axon-ui
yarn install
yarn run lint

echo "(4/8) Running lint on Nest Project..."
cd ../axon-server
yarn install
yarn run lint

echo "(5/8) Running unit tests on Angular Project..."
cd ../axon-ui
yarn run test

echo "(6/8) Running unit tests on Nest Project..."
cd ../axon-server
yarn run test

echo "(7/8) Running e2e tests on Angular Project..."
cd ../axon-ui
yarn run e2e
