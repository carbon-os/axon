#!/usr/bin/env bash

ENV=$1
if [ -z "$ENV" ]; then
    echo "Please specify the environment"
    echo "ie:  ./stop-server.sh axon-stage"
    exit 0
fi

echo "Stopping app on " ${ENV}
ssh -t -t $ENV "cd /carbon-os/target/axon/axon-server && ./stop.sh"
ssh -t -t $ENV "cd /carbon-os/target/axon/nginx && ./stop.sh"
