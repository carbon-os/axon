#!/usr/bin/env bash

set -e

echo "(1/2) Building Angular project..."
cd axon-ui
yarn install
yarn run build

#echo "(2/2) Building Nest project..."
#cd /carbon-os/workspace/axon/axon-server
#yarn run build TODO:axon-2
