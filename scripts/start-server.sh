#!/usr/bin/env bash

ENV=$1
if [ -z "$ENV" ]; then
    echo "Please specify the environment"
    echo "ie:  ./start-server.sh axon-stage"
    exit 0
fi


echo "Starting app on " ${ENV}
ssh -t -t $ENV "cd /carbon-os/target/axon/axon-server && ./start.sh"
ssh -t -t $ENV "cd /carbon-os/target/axon/nginx && ./start.sh"
