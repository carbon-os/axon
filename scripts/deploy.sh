#!/bin/bash

#UI_SRC_DIR=/carbon-os/workspace/axon/axon-ui/dist
UI_SRC_DIR=axon-ui/dist
UI_TARGET_DIR=/carbon-os/target/axon/axon-ui/

#NGINX_SRC_DIR=/carbon-os/workspace/axon/nginx
NGINX_SRC_DIR=nginx
NGINX_TARGET_DIR=/carbon-os/target/axon/

#SERVER_SRC_DIR=/carbon-os/workspace/axon/axon-server
SERVER_SRC_DIR=axon-server
SERVER_TARGET_DIR=/carbon-os/target/axon/

# Validate presence of command line parameters
ENV=$1
if [ -z "$ENV" ]; then
  echo "Specify the target server alias"
  echo "ie: ./deploy.sh axon-prod"
  exit 1
fi

rsync -rcav --perms --chmod=a+r,u+w -e ssh $NGINX_SRC_DIR $ENV:$NGINX_TARGET_DIR/
rsync -rcav --perms --chmod=a+r,u+w -e ssh $UI_SRC_DIR $ENV:$UI_TARGET_DIR/
rsync -rcav --perms --chmod=a+r,u+w -e ssh $SERVER_SRC_DIR $ENV:$SERVER_TARGET_DIR/

ssh $ENV -t -t "sh /carbon-os/target/axon/axon-server/src/environments/copy_env_file.sh $ENV"