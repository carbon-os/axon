import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';

@Entity()
export class Channel extends EntityBase {
    @Column()
    name?: string;
}