import { VersionColumn, UpdateDateColumn, CreateDateColumn, Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Uuid } from '../../types/common/Uuid';
import { UserUuid } from '../../types/common/UserUuid';
import { Identifiable } from '../interface/Identifiable';
import { Auditable } from '../interface/Auditable';
import { Versioned } from '../interface/Versioned';
import { EntityStatus } from '../../types/common/EntityStatus';

@Entity()
export abstract class EntityBase implements Identifiable, Auditable, Versioned {
    @PrimaryGeneratedColumn('uuid')
    uuid?: Uuid;

    @CreateDateColumn()
    createdDate?: Date;

    @UpdateDateColumn()
    lastModifiedDate?: Date;

    @Column()
    createdBy: UserUuid;

    @Column()
    lastModifiedBy: UserUuid;

    @VersionColumn()
    version?: number;

    @Column()
    entityStatus: EntityStatus;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }
}