import { Column, Entity } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';

@Entity()
export class AttackWarning extends EntityBase {
    @Column()
    message?: string;

    @Column('mediumtext')
    source: string;

    @Column('mediumtext')
    payload: string;
}