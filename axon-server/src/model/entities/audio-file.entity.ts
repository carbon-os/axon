import { Column, Entity } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';

@Entity()
export class AudioFile extends EntityBase {
    @Column()
    file_key: string;

    @Column()
    classification: number;

    @Column()
    fileName: string;

    @Column()
    mimeType: string;

    @Column()
    size: number;

    @Column()
    description: string;

    constructor(values: any = {}) {
        super(values);
        Object.assign(this, values);
    }
}