import { Uuid } from '../../types/common/Uuid';

export interface Identifiable {
    uuid?: Uuid;
}