import { UserUuid } from '../../types/common/UserUuid';

export interface Auditable {
    createdBy: UserUuid;
    lastModifiedBy: UserUuid;
    createdDate?: Date;
    lastModifiedDate?: Date;
}