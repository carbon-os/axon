export interface Versioned {
    version?: number;
}