import { Column, Entity } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';

@Entity()
export class Address extends EntityBase {
    @Column()
    place_id?: string;

    @Column()
    street1: string;

    @Column()
    street2: string;

    @Column()
    city: string;

    @Column()
    state: string;

    @Column()
    province: string;

    @Column()
    postalCode: string;

    @Column()
    postalCodeExtension: string;

    @Column()
    country: string;

    constructor(values: any = {}) {
        super(values);
        Object.assign(this, values);
    }
}