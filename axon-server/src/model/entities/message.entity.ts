import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';
import { User } from './user.entity';

@Entity()
export class Message extends EntityBase {
    // @OneToOne(type => User)
    // @JoinColumn()
    @Column()
    user?: string;

    @Column()
    channel?: string;

    @Column('mediumtext')
    text?: string;

}