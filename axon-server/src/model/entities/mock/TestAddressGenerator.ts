import { Address } from '../address.entity';

export class TestAddressGenerator {

    public static generate(): Address {
        return new Address({
            place_id: '',
            street1: '5 Ellery St.',
            street2: 'Apt 2',
            city: 'Boston',
            state: 'MA',
            province: 'New England',
            postalCode: '02114',
            postalCodeExtension: '0261',
            country: 'USA',
        });
    }

}