import { User } from '../user.entity';
import { TestAddressGenerator } from './TestAddressGenerator';
import { AuthUtils } from '../../../modules/auth/AuthUtils';
import { TestUtils } from '../../../utils/TestUtils';
import { JsonWebTokenPayload } from '../../types/application/JsonWebTokenPayload';
import { DecodedJwt } from '../../types/application/DecodedJwt';
import { UserAuthenticationDto } from '../../types/application/UserAuthenticationDto';
import { UuidUtils } from '../../../utils/UuidUtils';

export class TestUserGenerator {

    public static readonly TEST_REDIRECT_URL = '/test';

    public static generate(): User {
        const testString = TestUtils.randomString(6);
        const salt = AuthUtils.generateSalt();
        const passwordDigest = AuthUtils.hashPassword(testString, salt);
        return new User({
            email: 'test+user.' + testString + '.@carbonos.com',
            passwordDigest,
            salt,
            emailVerified: false,
            mobileNumber: '5555555555',
            mobileNumberVerified: false,
            emailTokenDigest: null,
            emailTokenExpiration: null,
            firstName: 'Justin',
            lastName: 'Vernon',
            iconUrl: '',
            dob: new Date(Date.now() - (20 * 1000)), // 20 seconds ago heh
            address: TestAddressGenerator.generate(),
            passwordAttempts: 0,
            lastAttempt: null,
            accountLocked: false,
        });
    }

    public static generateWithUuid(): User {
        const user = TestUserGenerator.generate();
        user.uuid = UuidUtils.newUuid();
        return user;
    }

    public static getTestPasswordFromEmail(email: string) {
        const splits = email.split('.');
        return splits[ 1 ];
    }

    public static getUadFromUser(user: User): UserAuthenticationDto {
        return {
            email: user.email,
            password: TestUserGenerator.getTestPasswordFromEmail(user.email),
            oldPassword: TestUserGenerator.getTestPasswordFromEmail(user.email),
            redirectUrl: TestUserGenerator.TEST_REDIRECT_URL,
        };
    }
}