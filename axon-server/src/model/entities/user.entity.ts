import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';
import { Address } from './address.entity';

@Entity()
export class User extends EntityBase {
    @Column()
    email: string;

    @Column({nullable: true})
    passwordDigest?: string;

    @Column()
    salt: string;

    @Column()
    emailVerified: boolean;

    @Column({nullable: true})
    mobileNumber?: string;

    @Column()
    mobileNumberVerified: boolean;

    @Column({nullable: true})
    emailTokenDigest?: string;

    @Column({nullable: true})
    emailTokenExpiration?: Date;

    @Column({nullable: true})
    firstName?: string;

    @Column({nullable: true})
    lastName?: string;

    @Column({nullable: true})
    iconUrl?: string;

    @Column({nullable: true})
    dob?: Date;

    @OneToOne(type => Address)
    @JoinColumn()
    address?: Address;

    @Column()
    passwordAttempts: number;

    @Column()
    lastAttempt: Date;

    @Column()
    accountLocked: boolean;

    constructor(values: any = {}) {
        super(values);
        Object.assign(this, values);
    }
}