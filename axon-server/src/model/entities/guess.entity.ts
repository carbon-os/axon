import { Column, Entity } from 'typeorm';
import { EntityBase } from './base/entity-base.entity';

@Entity()
export class Guess extends EntityBase {
    @Column()
    email: string;

    @Column()
    file_key: string;

    @Column()
    classification: number;

    @Column()
    carbonGuess: number;

    @Column()
    playerGuess: number;

    @Column()
    toDelete: boolean;

    constructor(values: any = {}) {
        super(values);
        Object.assign(this, values);
    }
}