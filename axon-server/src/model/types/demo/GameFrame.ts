import { AudioFileDto } from './AudioFileDto';

export interface GameFrame {
    file: AudioFileDto;
    playerGuess: number;
    carbonGuess: number;
    toDelete: boolean;
}
