export interface GuessDto {
    id: string;
    email: string;
    file_key: string;
    classification: number;
    carbonGuess: number;
    playerGuess: number;
    toDelete: boolean;
}
