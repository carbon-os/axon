export interface AudioFileDto {
    id: string;
    file_key: string;
    classification: number;
    fileName: string;
    mimeType: string;
    size: number;
    description: string;
}
