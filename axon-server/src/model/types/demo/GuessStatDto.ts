export interface GuessStatDto {
    humanBattingAverage: number;
    carbonBattingAverage: number;

    rh: { file_key: string, count: number }[];
    rr: { file_key: string, count: number }[];
    hr: { file_key: string, count: number }[];
    hh: { file_key: string, count: number }[];
}
