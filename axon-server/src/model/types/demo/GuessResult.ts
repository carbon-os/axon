export type GuessResult = 'right' | 'wrong' | 'tbd';
