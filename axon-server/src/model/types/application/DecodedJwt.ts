export interface DecodedJwt {
    email?: string;
    uuid?: string;
}
