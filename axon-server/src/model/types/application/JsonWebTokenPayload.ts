import { EncodedJwt } from './EncodedJwt';

export interface JsonWebTokenPayload {
    uuid?: string;
    email?: string;
    estimated_expiration?: number;
    hasPassword: boolean;

    access_token?: EncodedJwt;
}
