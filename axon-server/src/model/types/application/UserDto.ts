import { AddressDto } from '../place/AddressDto';

export interface UserDto {
    id: string;
    email: string;
    firstName?: string;
    lastName?: string;
    iconUrl?: string;
    dob?: Date;
    address?: AddressDto;
}
