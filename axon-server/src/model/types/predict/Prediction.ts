export interface Prediction {
    prediction: number[];
    error: string;
    predictionTime: number;
    extractionTime: number;
}
