export class MessageDto {
    id: string;
    timestamp?: number;
    // user?: UserDto;
    user?: string;
    channel?: string;
    text?: string;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }
}
