export interface MessageContentDto {
    text?: string;
    command?: string;
}
