import { MessageContentDto } from './MessageContentDto';

export interface NewMessageDto {
    jwt?: string;
    threadUuid?: string;
    content?: MessageContentDto;
}
