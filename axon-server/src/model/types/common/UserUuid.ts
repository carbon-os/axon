import { Uuid } from './Uuid';

export type UserUuid = Uuid;
