export interface AddressDto {
    id?: string;
    place_id?: string;
    street1?: string;
    street2?: string;
    city?: string;
    state?: string;
    province?: string;
    postalCode?: string;
    postalCodeExtension?: string;
    country?: string;
}
