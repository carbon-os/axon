import { AddressComponentType } from './AddressComponentType';

export interface AddressComponent {
    longName?: string;
    shortName?: string;
    types?: AddressComponentType[];
}
