export enum AddressComponentType {
    STREET_ADDRESS,
    ROUTE,
    INTERSECTION,
    POLITICAL,
    COUNTRY,
    ADMINISTRATIVE_AREA_LEVEL_1,
    ADMINISTRATIVE_AREA_LEVEL_2,
    ADMINISTRATIVE_AREA_LEVEL_3,
    ADMINISTRATIVE_AREA_LEVEL_4,
    ADMINISTRATIVE_AREA_LEVEL_5,
    COLLOQUIAL_AREA,
    LOCALITY,
    WARD,
    SUBLOCALITY,
    NEIGHBORHOOD,
    PREMISE,
    SUBPREMISE,
    POSTAL_CODE,
    NATURAL_FEATURE,
    AIRPORT,
    PARK,
    POINT_OF_INTEREST,
}
