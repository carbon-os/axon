import { AddressComponent } from './AddressComponent';

export interface GooglePlace {
    place_id?: string;
    address_components?: AddressComponent[];
}
