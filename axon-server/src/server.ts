import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './modules/app.module';
import * as bodyParser from 'body-parser';
import 'reflect-metadata';
import { environment } from './environments/environment';
import { HttpExceptionFilter } from './modules/common/exception.filter';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {

    const app = await NestFactory.create(ApplicationModule);
    app.setGlobalPrefix('/api/v1.0');
    app.use(bodyParser.json());
    app.useGlobalFilters(new HttpExceptionFilter());

    const options = new DocumentBuilder()
        .setTitle('CarbonOS API')
        .setDescription('')
        .setVersion('1.0')
        .addTag('carbon-os')
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('/api-docs', app, document);

    console.log('app listening on ' + environment.api_host + ':' + environment.api_port);

    await app.listen(environment.api_port);
}

bootstrap();
