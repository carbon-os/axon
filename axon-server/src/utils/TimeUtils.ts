import * as delay from 'delay';
import { RandomUtils } from './RandomUtils';

export class TimeUtils {

    private static readonly DELAY_MIN = 50;
    private static readonly DELAY_MAX = 100;

    /**
     * adds seconds to Date.now()
     *
     * @param {number} seconds
     * @returns {Date}
     */
    public static getNowPlusSeconds(seconds: number): Date {
        if (!seconds) {
            throw new Error('Cannot add null seconds to a Date');
        }
        let dateObj = Date.now();
        dateObj += 1000 * seconds;
        return new Date(dateObj);
    }

    public static async randomTimeVariationToPreventInformationLeak(): Promise<void> {
        const time = RandomUtils.randomIntBetweenInclusive(TimeUtils.DELAY_MIN, TimeUtils.DELAY_MAX);
        await delay(time);
    }
}