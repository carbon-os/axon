import 'jasmine';
import { TimeUtils } from '../TimeUtils';
import * as delay from 'delay';

describe('TimeUtils', () => {

    beforeEach(() => {

    });

    describe('getNowPlusSeconds', () => {
        it('should return a time that is in the future by the same amount of seconds it was given', () => {
            const futureSeconds = 100;
            const futureDate = TimeUtils.getNowPlusSeconds(futureSeconds);

            expect(futureDate.getTime()).toBeGreaterThan(Date.now() + (1000 * 99));
            expect(futureDate.getTime()).toBeLessThan(Date.now() + (1000 * 101));
        });
    });

    describe('getNowPlusSeconds', () => {
        it('should return a time that is in the past by the same amount of negative seconds it was given', () => {
            const pastSeconds = -100;
            const futureDate = TimeUtils.getNowPlusSeconds(pastSeconds);

            expect(futureDate.getTime()).toBeLessThan(Date.now() - (1000 * 99));
            expect(futureDate.getTime()).toBeGreaterThan(Date.now() - (1000 * 101));
        });
    });

    describe('getNowPlusSeconds', () => {
        it('should throw if given a null value', () => {
            expect(() => {TimeUtils.getNowPlusSeconds(null);})
                .toThrow('Cannot add null seconds to a Date');
        });
    });
});