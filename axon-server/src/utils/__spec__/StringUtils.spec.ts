import 'jasmine';
import { StringUtils } from '../StringUtils';

describe('StringUtils', () => {

    beforeEach(() => {

    });

    describe('isBlank', () => {
        it('should return true if the string is null', () => {
            const someString = null;
            const actual = StringUtils.isBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return true if the string is undefined', () => {
            const actual = StringUtils.isBlank(undefined);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return true if the string is empty', () => {
            const someString = '';
            const actual = StringUtils.isBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return true if the string is whitespace with spaces', () => {
            const someString = ' ';
            const actual = StringUtils.isBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return true if the string is whitespace with tabs', () => {
            const someString = '    ';
            const actual = StringUtils.isBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return false if the string contains alphabetic characters', () => {
            const someString = 'abcde';
            const actual = StringUtils.isBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return false if the string contains numeric characters', () => {
            const someString = '123312';
            const actual = StringUtils.isBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isBlank', () => {
        it('should return false if the string contains whitespace and alphanumeric characters', () => {
            const someString = '    asdfs    ';
            const actual = StringUtils.isBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return false if the string is null', () => {
            const someString = null;
            const actual = StringUtils.isNotBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return false if the string is undefined', () => {
            const actual = StringUtils.isNotBlank(undefined);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return false if the string is empty', () => {
            const someString = '';
            const actual = StringUtils.isNotBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return false if the string is whitespace with spaces', () => {
            const someString = ' ';
            const actual = StringUtils.isNotBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return false if the string is whitespace with tabs', () => {
            const someString = '    ';
            const actual = StringUtils.isNotBlank(someString);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return true if the string contains alphabetic characters', () => {
            const someString = 'abcde';
            const actual = StringUtils.isNotBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return true if the string contains numeric characters', () => {
            const someString = '123312';
            const actual = StringUtils.isNotBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isNotBlank', () => {
        it('should return true if the string contains whitespace and alphanumeric characters', () => {
            const someString = '    asdfs    ';
            const actual = StringUtils.isNotBlank(someString);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });
});