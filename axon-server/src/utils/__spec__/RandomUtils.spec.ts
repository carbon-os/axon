import 'jasmine';
import { TimeUtils } from '../TimeUtils';
import { RandomUtils } from '../RandomUtils';

describe('RandomUtils', () => {

    beforeEach(() => {

    });

    describe('randomIntBetweenInclusive', () => {
        it('should always return an integer in between the two given', () => {

            for (let i = 0; i < 1000; i++) {
                const randomInt = RandomUtils.randomIntBetweenInclusive(0, 10);
                expect(randomInt).toBeLessThanOrEqual(10);
                expect(randomInt).toBeGreaterThanOrEqual(0);
            }

        });
    });

    it('should throw if min is higher than max', () => {
        expect(() => {RandomUtils.randomIntBetweenInclusive(10, 0);})
            .toThrow('Parameters out of range: [10, 0]');
    });

    it('should only ever return the same number if min and max are equal', () => {
        for (let i = 0; i < 1000; i++) {
            const randomInt = RandomUtils.randomIntBetweenInclusive(5, 5);
            expect(randomInt).toEqual(5);
        }
    });
});