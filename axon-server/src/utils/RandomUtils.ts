export class RandomUtils {
    public static randomIntBetweenInclusive(min: number, max: number) {
        if (min > max) {
            throw  new Error('Parameters out of range: [' + min + ', ' + max + ']');
        }
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}
