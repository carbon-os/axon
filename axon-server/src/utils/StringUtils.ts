export class StringUtils {

    public static isBlank(s: string): boolean {
        return !StringUtils.isNotBlank(s);
    }

    public static isNotBlank(s: string): boolean {
        return !!s && /\S/.test(s);
    }

}