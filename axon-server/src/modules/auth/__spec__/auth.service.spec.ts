import 'jasmine';
import 'jest';

import { Test } from '@nestjs/testing';
import { AuthUtils } from '../AuthUtils';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';
import { DecodedJwt } from '../../../model/types/application/DecodedJwt';
import { UserService } from '../user.service';
import { EmailService } from '../../email/email.service';
import { AuthService } from '../auth.service';
import { SecurityService } from '../../security/security.service';
import { MockDatabaseModule } from '../../database/mock-database.module';
import { WELCOME_STATE } from '../../../model/states/welcome.state';
import { EncodedJwt } from '../../../model/types/application/EncodedJwt';
import { UuidUtils } from '../../../utils/UuidUtils';
import { Exceptions } from '../../common/exceptions';
import * as HttpStatus from 'http-status-codes';
import { UserUtils } from '../user.utils';

describe('AuthService', () => {
    let userService: UserService;
    let authService: AuthService;
    let emailService: EmailService;
    let securityService: SecurityService;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MockDatabaseModule,
            ],
            providers: [
                UserService,
                AuthService,
                EmailService,
                SecurityService,
            ],
        }).compile();

        authService = module.get<AuthService>(AuthService);
        userService = module.get<UserService>(UserService);
        emailService = module.get<EmailService>(EmailService);
        securityService = module.get<SecurityService>(SecurityService);
    });

    describe('requestLoginLink', async () => {
        it('should throw newUnauthenticatedError exception if given a uad without an email', async () => {
            const uad: UserAuthenticationDto = {
                redirectUrl: '/test',
            };

            await expect(authService.requestLoginLink(uad))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('requestLoginLink', async () => {
        it('should throw newUnauthenticatedError exception if given a uad without an email', async () => {
            await expect(authService.requestLoginLink(null))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('requestLoginLink', async () => {
        it('should call user service to load user, and if found, save user, then call email service to send login link', async () => {
            const user = TestUserGenerator.generate();
            const givenRedirectUrl = '/test';

            const uad: UserAuthenticationDto = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const createUser = jest.spyOn(userService, 'createUser').mockImplementation(() => user);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => user);
            const sendLoginLinkEmail = jest.spyOn(emailService, 'sendLoginLinkEmail').mockImplementation(() => null);

            await authService.requestLoginLink(uad);

            const expectedUad = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
                emailToken: expect.any(String),
            };

            const expectedUser = user;
            expectedUser.emailTokenDigest = expect.any(String);
            expectedUser.emailTokenExpiration = expect.any(Date);
            expectedUser.lastAttempt = expect.any(Date);

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(saveUser).toHaveBeenCalledWith(expectedUser);
            expect(sendLoginLinkEmail).toHaveBeenCalledWith(expectedUad);
        });
    });

    describe('requestLoginLink', async () => {
        it('should call user service to load user, and if not found, create user, then call email service to send login link', async () => {
            const user = TestUserGenerator.generate();
            const givenRedirectUrl = '/test';

            const uad: UserAuthenticationDto = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => null);
            const createUser = jest.spyOn(userService, 'createUser').mockImplementation(() => user);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => user);
            const sendLoginLinkEmail = jest.spyOn(emailService, 'sendLoginLinkEmail').mockImplementation(() => null);

            await authService.requestLoginLink(uad);

            const expectedUad = {
                email: user.email,
                redirectUrl: WELCOME_STATE,
                emailToken: expect.any(String),
            };

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(createUser).toHaveBeenCalledWith(expectedUad);
            expect(sendLoginLinkEmail).toHaveBeenCalledWith(expectedUad);
        });
    });

    describe('generateJsonWebToken', async () => {
        it('should throw if the given uad does not have an email', async () => {
            const uad: UserAuthenticationDto = {
                redirectUrl: '/test',
            };

            await expect(authService.generateJsonWebToken(uad))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('generateJsonWebToken', async () => {
        it('should throw if the given uad is null', async () => {
            await expect(authService.generateJsonWebToken(null))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('generateJsonWebToken', async () => {
        it('should throw if the loaded user is null', async () => {
            const user = TestUserGenerator.generate();
            const givenRedirectUrl = '/test';

            const uad: UserAuthenticationDto = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => null);
            const validateUser = jest.spyOn(authService, 'validateUser').mockImplementation(() => false);

            await expect(authService.generateJsonWebToken(uad))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('generateJsonWebToken', async () => {
        it('should throw an newUnauthenticatedError exception if it determines that the user/uad is invalid', async () => {
            const user = TestUserGenerator.generate();
            user.uuid = UuidUtils.newUuid();
            const givenRedirectUrl = '/test';

            const uad: UserAuthenticationDto = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const validateUser = jest.spyOn(authService, 'validateUser').mockImplementation(() => false);

            await expect(authService.generateJsonWebToken(uad))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('generateJsonWebToken', async () => {
        it('should return a new valid JsonWebToken payload', async () => {
            const user = TestUserGenerator.generate();
            user.uuid = UuidUtils.newUuid();
            const givenRedirectUrl = '/test';

            const uad: UserAuthenticationDto = {
                email: user.email,
                redirectUrl: givenRedirectUrl,
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const validateUser = jest.spyOn(authService, 'validateUser').mockImplementation(() => true);

            const payload = await authService.generateJsonWebToken(uad);

            expect(payload.uuid).toEqual(user.uuid);
            expect(payload.email).toEqual(user.email);
            expect(payload.estimated_expiration).toBeGreaterThan(Date.now());
            expect(payload.hasPassword).toEqual(true);
            expect(payload.access_token).not.toBeNull();

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(validateUser).toHaveBeenCalledWith(user, uad);
        });
    });

    describe('validateUserGivenJwt', async () => {
        it('should throw newUnauthenticatedError if jwt is invalid', async () => {
            const user = TestUserGenerator.generate();
            user.uuid = UuidUtils.newUuid();

            const jwt: EncodedJwt = 'some.invalid.jwt';

            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => true);

            await expect(authService.validateUserGivenJwt(jwt))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('validateUserGivenJwt', async () => {
        it('should return false if user is not valid', async () => {
            const user = TestUserGenerator.generate();
            user.uuid = UuidUtils.newUuid();

            const jwt: EncodedJwt = 'some.mock.jwt';
            const decodedJwt: DecodedJwt = {
                email: user.email,
                uuid: user.uuid,
            };

            const validateUser = jest.spyOn(AuthUtils, 'verifyAndDecodeJwt').mockImplementation(() => decodedJwt);
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => false);

            const valid = await authService.validateUserGivenJwt(jwt);

            expect(valid).toEqual(false);

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(validateUser).toHaveBeenCalledWith(jwt);
            expect(isUserValid).toHaveBeenCalledWith(user);
        });
    });

    describe('validateUserGivenJwt', async () => {
        it('should return a new valid JsonWebToken payload', async () => {
            const user = TestUserGenerator.generate();
            user.uuid = UuidUtils.newUuid();

            const jwt: EncodedJwt = 'some.mock.jwt';
            const decodedJwt: DecodedJwt = {
                email: user.email,
                uuid: user.uuid,
            };

            const validateUser = jest.spyOn(AuthUtils, 'verifyAndDecodeJwt').mockImplementation(() => decodedJwt);
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => true);

            const valid = await authService.validateUserGivenJwt(jwt);

            expect(valid).toEqual(true);

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(validateUser).toHaveBeenCalledWith(jwt);
        });
    });

    describe('updatePassword', async () => {
        it('should throw a BAD_REQUEST error if given a null uad', async () => {
            await expect(authService.updatePassword(null))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('updatePassword', async () => {
        it('should throw a BAD_REQUEST error if given a uad with no password', async () => {
            const user = TestUserGenerator.generate();

            const uad: UserAuthenticationDto = {
                email: user.email,
                oldPassword: TestUserGenerator.getTestPasswordFromEmail(user.email),
            };

            await expect(authService.updatePassword(uad))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('updatePassword', async () => {
        it('should throw a newUnauthenticatedError error if the userService returns a null user', async () => {
            const user = TestUserGenerator.generate();

            const uad: UserAuthenticationDto = {
                email: user.email,
                oldPassword: TestUserGenerator.getTestPasswordFromEmail(user.email),
                password: 'new-password',
            };

            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => null);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => false);

            await expect(authService.updatePassword(uad))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('updatePassword', async () => {
        it('should throw a Forbidden error if the given old password does not match the existing one', async () => {
            const user = TestUserGenerator.generate();

            const uad: UserAuthenticationDto = {
                email: user.email,
                oldPassword: 'bad password',
                password: 'new-password',
            };

            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => true);

            await expect(authService.updatePassword(uad))
                .rejects
                .toThrow('That existing password doesn\'t seem correct...');
        });
    });

    describe('updatePassword', async () => {
        it('should save a user with a hash of the new password', async () => {
            const user = TestUserGenerator.generate();
            user.passwordAttempts = 2;

            const uad: UserAuthenticationDto = {
                email: user.email,
                oldPassword: TestUserGenerator.getTestPasswordFromEmail(user.email),
                password: 'new-password',
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => true);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => user);

            await authService.updatePassword(uad);

            const expectedUser = user;
            expectedUser.passwordDigest = AuthUtils.hashPassword(uad.password, user.salt);
            expectedUser.passwordAttempts = 0;

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(isUserValid).toHaveBeenCalledWith(user);
            expect(saveUser).toHaveBeenCalledWith(expectedUser);
        });
    });

    describe('updatePassword', async () => {
        it('should not require an old password if the user has not set a password', async () => {
            const user = TestUserGenerator.generate();
            user.passwordAttempts = 2;
            user.passwordDigest = null;

            const uad: UserAuthenticationDto = {
                email: user.email,
                password: 'new-password',
            };
            const loadUserByEmail = jest.spyOn(userService, 'getUserByEmail').mockImplementation(() => user);
            const isUserValid = jest.spyOn(AuthUtils, 'isUserValid').mockImplementation(() => true);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => user);

            await authService.updatePassword(uad);

            const expectedUser = user;
            expectedUser.passwordDigest = AuthUtils.hashPassword(uad.password, user.salt);
            expectedUser.passwordAttempts = 0;

            expect(loadUserByEmail).toHaveBeenCalledWith(user.email);
            expect(isUserValid).toHaveBeenCalledWith(user);
            expect(saveUser).toHaveBeenCalledWith(expectedUser);
        });
    });

    describe('validateUserGivenEmailToken', async () => {
        it('should throw a BAD_REQUEST exception if given a null user', async () => {
            const user = null;

            await expect(authService.validateUserGivenEmailToken(user, 'some-token'))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('validateUserGivenEmailToken', async () => {
        it('should throw a BAD_REQUEST exception if given a blank password', async () => {
            const user = TestUserGenerator.generate();
            const emailToken = '      ';

            await expect(authService.validateUserGivenEmailToken(user, emailToken))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('validateUserGivenEmailToken', async () => {
        it('should call incrementFailedAttempt and return false if the emailToken does not match', async () => {
            const user = TestUserGenerator.generate();
            user.emailTokenDigest = AuthUtils.hashPassword(AuthUtils.generateSecureEmailToken(), user.salt);
            const emailTokenAttempt = 'some-bad-emailtoken';

            const isUserValid = jest.spyOn(AuthUtils, 'isEmailTokenValid').mockImplementation(() => false);
            const incrementFailedAttempt = jest.spyOn(authService, 'incrementFailedAttempt').mockImplementation(() => null);

            const valid = await authService.validateUserGivenEmailToken(user, emailTokenAttempt);

            expect(valid).toEqual(false);
            expect(incrementFailedAttempt).toHaveBeenCalledWith(user);
        });
    });

    describe('validateUserGivenEmailToken', async () => {
        it('should call saveUser and return true if the emailToken matches', async () => {
            const user = TestUserGenerator.generate();
            const emailToken = AuthUtils.generateSecureEmailToken();
            user.emailTokenDigest = AuthUtils.hashPassword(emailToken, user.salt);

            const isEmailTokenValid = jest.spyOn(AuthUtils, 'isEmailTokenValid').mockImplementation(() => true);
            const incrementFailedAttempt = jest.spyOn(authService, 'incrementFailedAttempt').mockImplementation(() => null);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => null);

            const valid = await authService.validateUserGivenEmailToken(user, emailToken);

            expect(valid).toEqual(true);
            expect(incrementFailedAttempt).not.toHaveBeenCalled();
            expect(saveUser).toHaveBeenCalledWith(user);
            expect(user.emailVerified).toEqual(true);
            expect(user.passwordAttempts).toEqual(0);
            expect(user.lastAttempt.getTime()).toBeLessThanOrEqual(Date.now());
            expect(user.lastAttempt.getTime()).toBeGreaterThanOrEqual(Date.now() - 1000);
        });
    });

    describe('validateUserGivenPassword', async () => {
        it('should throw a BAD_REQUEST exception if given a null user', async () => {
            const user = null;

            await expect(authService.validateUserGivenPassword(user, 'some-password'))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('validateUserGivenPassword', async () => {
        it('should throw a BAD_REQUEST exception if given a blank password', async () => {
            const user = TestUserGenerator.generate();
            const password = '     ';

            await expect(authService.validateUserGivenPassword(user, password))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('validateUserGivenPassword', async () => {
        it('should call incrementFailedAttempt and return false if the password does not match', async () => {
            const user = TestUserGenerator.generate();
            const password = 'some-bad-password';

            const passwordMatches = jest.spyOn(AuthUtils, 'passwordMatches').mockImplementation(() => false);
            const incrementFailedAttempt = jest.spyOn(authService, 'incrementFailedAttempt').mockImplementation(() => null);

            const valid = await authService.validateUserGivenPassword(user, password);

            expect(valid).toEqual(false);
            expect(incrementFailedAttempt).toHaveBeenCalledWith(user);
        });
    });


    describe('validateUserGivenPassword', async () => {
        it('should call saveUser and return true if the password matches', async () => {
            const user = TestUserGenerator.generate();
            const password = TestUserGenerator.getTestPasswordFromEmail(user.email);

            const passwordMatches = jest.spyOn(AuthUtils, 'passwordMatches').mockImplementation(() => true);
            const incrementFailedAttempt = jest.spyOn(authService, 'incrementFailedAttempt').mockImplementation(() => null);
            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => null);

            const valid = await authService.validateUserGivenPassword(user, password);

            expect(valid).toEqual(true);
            expect(incrementFailedAttempt).not.toHaveBeenCalled();
            expect(saveUser).toHaveBeenCalledWith(user);
            expect(user.emailVerified).toEqual(false);
            expect(user.passwordAttempts).toEqual(0);
            expect(user.lastAttempt.getTime()).toBeLessThanOrEqual(Date.now());
            expect(user.lastAttempt.getTime()).toBeGreaterThanOrEqual(Date.now() - 1000
        });
    });

    describe('validateUser', async () => {
        it('should return false if given a null uad', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = null;

            const valid = await authService.validateUser(user, uad);
            expect(valid).toEqual(false);
        });
    });

    describe('validateUser', async () => {
        it('should return false if given a null user', async () => {
            const user = null;
            const uad = {
                email: 'some@email.com',
                password: 'some-password',
            };

            const valid = await authService.validateUser(user, uad);
            expect(valid).toEqual(false);
        });
    });

    describe('validateUser', async () => {
        it('should return false if given a user that is not logginable', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = {
                email: user.email,
                password: TestUserGenerator.getTestPasswordFromEmail(user.email),
            };

            const isUserLogginable = jest.spyOn(AuthUtils, 'isUserLogginable').mockImplementation(() => false);

            const valid = await authService.validateUser(user, uad);
            expect(valid).toEqual(false);
        });
    });

    describe('validateUser', async () => {
        it('should validate with emailToken if the uad comes with an emailToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = {
                email: user.email,
                emailToken: 'some-email-token',
            };

            const isUserLogginable = jest.spyOn(AuthUtils, 'isUserLogginable').mockImplementation(() => true);
            const validateUserGivenEmailToken = jest.spyOn(authService, 'validateUserGivenEmailToken').mockImplementation(() => true);

            const valid = await authService.validateUser(user, uad);

            expect(validateUserGivenEmailToken).toHaveBeenCalledWith(user, expect.any(String));
            expect(valid).toEqual(true);
        });
    });

    describe('validateUser', async () => {
        it('should validate with password if the uad comes without an emailToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = {
                email: user.email,
                password: TestUserGenerator.getTestPasswordFromEmail(user.email),
            };

            const isUserLogginable = jest.spyOn(AuthUtils, 'isUserLogginable').mockImplementation(() => true);
            const validateUserGivenEmailToken = jest.spyOn(authService, 'validateUserGivenEmailToken').mockImplementation(() => true);
            const validateUserGivenPassword = jest.spyOn(authService, 'validateUserGivenPassword').mockImplementation(() => true);

            const valid = await authService.validateUser(user, uad);

            expect(validateUserGivenEmailToken).not.toHaveBeenCalled();
            expect(validateUserGivenPassword).toHaveBeenCalledWith(user, expect.any(String));
            expect(valid).toEqual(true);
        });
    });

    describe('incrementFailedAttempt', async () => {
        it('should throw a BAD_REQUEST exception if given a null user', async () => {
            const user = null;

            await expect(authService.incrementFailedAttempt(user))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('incrementFailedAttempt', async () => {
        it('should call \'saveUser\' with a user that has an incremented password attempt', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const expectedUser = Object.assign(user, {});

            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => {});

            await authService.incrementFailedAttempt(user);

            expectedUser.passwordAttempts = user.passwordAttempts + 1;
            expect(saveUser).toHaveBeenCalledWith(expectedUser);
        });
    });

    describe('incrementFailedAttempt', async () => {
        it('should call \'sendAccountLockedWarningEmail\' if the password increment locks the user', async () => {
            const user = TestUserGenerator.generateWithUuid();
            user.passwordAttempts = AuthUtils.ATTEMPT_LIMIT;
            const expectedUser = Object.assign(user, {});

            const saveUser = jest.spyOn(userService, 'saveUser').mockImplementation(() => {});
            const logWarning = jest.spyOn(securityService, 'logWarning').mockImplementation(() => {});
            const sendAccountLockedWarningEmail = jest.spyOn(emailService, 'sendAccountLockedWarningEmail').mockImplementation(() => {});

            await authService.incrementFailedAttempt(user);

            expectedUser.passwordAttempts = user.passwordAttempts + 1;
            expectedUser.accountLocked = true;
            expectedUser.emailTokenDigest = expect.any(String);
            expectedUser.emailTokenExpiration = expect.any(Date);

            expect(logWarning).toHaveBeenCalledWith(expect.any(String), expectedUser);
            expect(sendAccountLockedWarningEmail).toHaveBeenCalledWith(expectedUser, expect.any(String));
            expect(saveUser).toHaveBeenCalledWith(expectedUser);
        });
    });
});