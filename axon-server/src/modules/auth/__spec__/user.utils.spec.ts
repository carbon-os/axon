import 'jest';
import 'jest-extended';
import { UserUtils } from '../user.utils';
import { Exceptions } from '../../common/exceptions';
import * as HttpStatus from 'http-status-codes';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';
import { Address } from '../../../model/entities/address.entity';
import { EntityStatus } from '../../../model/types/common/EntityStatus';
import { AuthUtils } from '../AuthUtils';
import { AddressDto } from '../../../model/types/place/AddressDto';

describe('UserUtils', () => {

    beforeEach(() => {
        // const module = await Test.createTestingModule({}).compile();
    });

    describe('newUser', () => {
        it('should throw a BAD REQUEST exception if given a uad that doesn\'t exist', () => {
            expect(() => { UserUtils.newUser(null); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('newUser', () => {
        it('should throw a BAD REQUEST exception if given a uad that doesn\'t have an email', () => {
            const uad = {};

            expect(() => { UserUtils.newUser(uad); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('newUser', () => {
        it('should return a user with a blank password if not given a password', () => {
            const uad = {
                email: 'newUser@useremail.com',
            };

            const newUser = UserUtils.newUser(uad);

            expect(newUser.email).toEqual(uad.email);
            expect(newUser.passwordDigest).toBeNull();
            expect(newUser.salt).toBeString();
            expect(newUser.emailVerified).toEqual(false);
            expect(newUser.mobileNumber).toBeNull();
            expect(newUser.mobileNumberVerified).toEqual(false);
            expect(newUser.emailTokenDigest).toBeNull();
            expect(newUser.emailTokenExpiration).toBeNull();
            expect(newUser.firstName).toBeNull();
            expect(newUser.lastName).toBeNull();
            expect(newUser.iconUrl).toBeNull();
            expect(newUser.dob).toBeNull();
            expect(newUser.address).toSatisfy(a => a instanceof Address);
            expect(newUser.passwordAttempts).toEqual(0);
            expect(newUser.lastAttempt.getTime()).toBeLessThanOrEqual(Date.now());
            expect(newUser.accountLocked).toEqual(false);
            expect(newUser.uuid).toBeNull();
            expect(newUser.createdDate).toBeNull();
            expect(newUser.lastModifiedDate).toBeNull();
            expect(newUser.createdBy).toEqual('self');
            expect(newUser.lastModifiedBy).toEqual('self');
            expect(newUser.version).toBeNull();
            expect(newUser.entityStatus).toEqual(EntityStatus.ACTIVE);
        });
    });

    describe('newUser', () => {
        it('should return a user with a hashed password if given a password', () => {
            const uad = {
                email: 'newUser@useremail.com',
                password: 'new-password',
            };

            const newUser = UserUtils.newUser(uad);

            expect(newUser.email).toEqual(uad.email);
            expect(newUser.passwordDigest).toEqual(AuthUtils.hashPassword(uad.password, newUser.salt));
            expect(newUser.salt).toBeString();
            expect(newUser.emailVerified).toEqual(false);
            expect(newUser.mobileNumber).toBeNull();
            expect(newUser.mobileNumberVerified).toEqual(false);
            expect(newUser.emailTokenDigest).toBeNull();
            expect(newUser.emailTokenExpiration).toBeNull();
            expect(newUser.firstName).toBeNull();
            expect(newUser.lastName).toBeNull();
            expect(newUser.iconUrl).toBeNull();
            expect(newUser.dob).toBeNull();
            expect(newUser.address).toSatisfy(a => a instanceof Address);
            expect(newUser.passwordAttempts).toEqual(0);
            expect(newUser.lastAttempt.getTime()).toBeLessThanOrEqual(Date.now());
            expect(newUser.accountLocked).toEqual(false);
            expect(newUser.uuid).toBeNull();
            expect(newUser.createdDate).toBeNull();
            expect(newUser.lastModifiedDate).toBeNull();
            expect(newUser.createdBy).toEqual('self');
            expect(newUser.lastModifiedBy).toEqual('self');
            expect(newUser.version).toBeNull();
            expect(newUser.entityStatus).toEqual(EntityStatus.ACTIVE);
        });
    });

    describe('newUser', () => {
        it('should return a user with a hashed email token and expiration date if given an email token', () => {
            const uad = {
                email: 'newUser@useremail.com',
                emailToken: 'some-email-token',
            };

            const newUser = UserUtils.newUser(uad);

            expect(newUser.email).toEqual(uad.email);
            expect(newUser.passwordDigest).toBeNull();
            expect(newUser.salt).toBeString();
            expect(newUser.emailVerified).toEqual(false);
            expect(newUser.mobileNumber).toBeNull();
            expect(newUser.mobileNumberVerified).toEqual(false);
            expect(newUser.emailTokenDigest).toEqual(AuthUtils.hashPassword(uad.emailToken, newUser.salt));
            expect(newUser.emailTokenExpiration.getTime()).toBeGreaterThan(Date.now());
            expect(newUser.firstName).toBeNull();
            expect(newUser.lastName).toBeNull();
            expect(newUser.iconUrl).toBeNull();
            expect(newUser.dob).toBeNull();
            expect(newUser.address).toSatisfy(a => a instanceof Address);
            expect(newUser.passwordAttempts).toEqual(0);
            expect(newUser.lastAttempt.getTime()).toBeLessThanOrEqual(Date.now());
            expect(newUser.accountLocked).toEqual(false);
            expect(newUser.uuid).toBeNull();
            expect(newUser.createdDate).toBeNull();
            expect(newUser.lastModifiedDate).toBeNull();
            expect(newUser.createdBy).toEqual('self');
            expect(newUser.lastModifiedBy).toEqual('self');
            expect(newUser.version).toBeNull();
            expect(newUser.entityStatus).toEqual(EntityStatus.ACTIVE);
        });
    });

    describe('userToUserDto', () => {
        it('should throw a BAD REQUEST exception if given a null user', () => {
            const user = null;

            expect(() => { UserUtils.userToUserDto(user); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('userToUserDto', () => {
        it('should throw a BAD REQUEST exception if given a user without a uuid', () => {
            const user = TestUserGenerator.generate();
            user.uuid = null;

            expect(() => { UserUtils.userToUserDto(user); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('userToUserDto', () => {
        it('should return a userDto with the same fields copied over from the user', () => {
            const user = TestUserGenerator.generateWithUuid();

            const userDto = UserUtils.userToUserDto(user);

            expect(userDto.id).toEqual(user.uuid);
            expect(userDto.email).toEqual(user.email);
            expect(userDto.address).toBeDefined();
            expect(userDto.dob).toEqual(user.dob);
            expect(userDto.firstName).toEqual(user.firstName);
            expect(userDto.lastName).toEqual(user.lastName);
            expect(userDto.iconUrl).toEqual(user.iconUrl);
        });
    });

    describe('mergeUserDtoIntoUser', () => {
        it('should throw a BAD REQUEST exception if given a null user', () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);

            expect(() => { UserUtils.mergeUserDtoIntoUser(null, userDto); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('mergeUserDtoIntoUser', () => {
        it('should throw a BAD REQUEST exception if given a null userDto', () => {
            const user = TestUserGenerator.generateWithUuid();

            expect(() => { UserUtils.mergeUserDtoIntoUser(user, null); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('mergeUserDtoIntoUser', () => {
        it('should throw a BAD REQUEST exception if given a userDto without an id', () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            userDto.id = '    ';

            expect(() => { UserUtils.mergeUserDtoIntoUser(user, userDto); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('mergeUserDtoIntoUser', () => {
        it('should throw a BAD REQUEST exception if given a userDto id does not match user id', () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            userDto.id = 'some-other-id';

            expect(() => { UserUtils.mergeUserDtoIntoUser(user, userDto); })
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('mergeUserDtoIntoUser', () => {
        it('should return a userDto with the same fields copied over from the user', () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            userDto.firstName = 'newFirstName';
            userDto.lastName = 'newLastName';
            userDto.iconUrl = 'newIconUrl';
            userDto.dob = new Date();

            const mergedUser = UserUtils.mergeUserDtoIntoUser(user, userDto);

            expect(mergedUser.address).toBeDefined();
            expect(mergedUser.dob).toEqual(userDto.dob);
            expect(mergedUser.firstName).toEqual(userDto.firstName);
            expect(mergedUser.lastName).toEqual(userDto.lastName);
            expect(mergedUser.iconUrl).toEqual(userDto.iconUrl);
        });
    });
});