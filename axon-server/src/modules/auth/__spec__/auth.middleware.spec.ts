import 'jasmine';
import 'jest';

import { Test } from '@nestjs/testing';
import { AuthUtils } from '../AuthUtils';
import { UserService } from '../user.service';
import { EmailService } from '../../email/email.service';
import { AuthService } from '../auth.service';
import { SecurityService } from '../../security/security.service';
import { MockDatabaseModule } from '../../database/mock-database.module';
import { AuthMiddleware } from '../auth.middleware';

describe('AuthMiddleware', () => {
    let userService: UserService;
    let authService: AuthService;
    let emailService: EmailService;
    let securityService: SecurityService;
    let authMiddleware: AuthMiddleware;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MockDatabaseModule,
            ],
            providers: [
                UserService,
                AuthService,
                EmailService,
                SecurityService,
                AuthMiddleware,
            ],
        }).compile();

        authService = module.get<AuthService>(AuthService);
        userService = module.get<UserService>(UserService);
        emailService = module.get<EmailService>(EmailService);
        securityService = module.get<SecurityService>(SecurityService);
        authMiddleware = module.get<AuthMiddleware>(AuthMiddleware);
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a null headers object', async () => {
            const headers = null;

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a blank authorization header', async () => {
            const headers = {
                authorization: '     ',
            };

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a null authorization header', async () => {
            const headers = {
                authorization: null,
            };

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a malformed Bearer auth header', async () => {
            const headers = {
                authorization: 'Bearer some malformed string',
            };

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a Bearer auth header with no value', async () => {
            const headers = {
                authorization: 'Bearer ',
            };

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should throw newUnauthenticatedError exception if given a non-bearer header schema', async () => {
            const headers = {
                authorization: 'SomethingElse some.mock.jwt',
            };

            expect(() => {AuthMiddleware.extractJwtFromHeaders(headers);})
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should return the parsed jwt if given a Bearer auth header', async () => {
            const expectedJwt = 'some.mock.jwt';
            const headers = {
                authorization: 'Bearer ' + expectedJwt,
            };

            const actualJwt = AuthMiddleware.extractJwtFromHeaders(headers);

            expect(actualJwt).toEqual(expectedJwt);
        });
    });

    describe('extractJwtFromHeaders', async () => {
        it('should return the parsed jwt if given a Bearer auth header with a capitalized \'A\'', async () => {
            const expectedJwt = 'some.mock.jwt';
            const headers = {
                Authorization: 'Bearer ' + expectedJwt,
            };

            const actualJwt = AuthMiddleware.extractJwtFromHeaders(headers);

            expect(actualJwt).toEqual(expectedJwt);
        });
    });

    describe('resolve', async () => {
        it('should return a function which throws if given a null request', async () => {
            const resolveFn = authMiddleware.resolve();
            const next = jest.fn();
            const req = null;

            await expect(resolveFn(req, null, next))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('resolve', async () => {
        it('should return a function which throws if given a request with null headers', async () => {
            const resolveFn = authMiddleware.resolve();
            const next = jest.fn();
            const req = {
                headers: null,
            };

            await expect(resolveFn(req, null, next))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('resolve', async () => {
        it('should return a function which throws if the auth service can\'t validate the token', async () => {
            const resolveFn = authMiddleware.resolve();
            const next = jest.fn();

            const jwtString = 'some.mock.jwt';
            const authHeader = 'Bearer ' + jwtString;

            const req = {
                headers: {
                    authorization: authHeader,
                },
            };

            const extractJwtFromHeaders = jest.spyOn(AuthMiddleware, 'extractJwtFromHeaders').mockImplementation(() => jwtString);
            const validateUserGivenJwt = jest.spyOn(authService, 'validateUserGivenJwt').mockImplementation(() => false);

            await expect(resolveFn(req, null, next))
                .rejects
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('resolve', async () => {
        it('should return a function which calls next() if the headers parse correctly and the validation function works', async () => {
            const resolveFn = authMiddleware.resolve();
            const next = jest.fn();

            const jwtString = 'some.mock.jwt';
            const authHeaders = {
                Authorization: 'Bearer ' + jwtString,
            };

            const req = {
                headers: authHeaders,
            };

            const extractJwtFromHeaders = jest.spyOn(AuthMiddleware, 'extractJwtFromHeaders').mockImplementation(() => jwtString);
            const validateUserGivenJwt = jest.spyOn(authService, 'validateUserGivenJwt').mockImplementation(() => true);

            await resolveFn(req, null, next);

            expect(extractJwtFromHeaders).toHaveBeenCalledWith(authHeaders);
            expect(validateUserGivenJwt).toHaveBeenCalledWith(jwtString);
            expect(next).toHaveBeenCalled();
        });
    });
});