import 'jasmine';
import 'jest';

import { Test } from '@nestjs/testing';
import { UserService } from '../user.service';
import { EmailService } from '../../email/email.service';
import { AuthService } from '../auth.service';
import { SecurityService } from '../../security/security.service';
import { MockDatabaseModule } from '../../database/mock-database.module';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';
import { UserController } from '../user.controller';
import { UserUtils } from '../user.utils';

describe('UserController', () => {
    let userService: UserService;
    let authService: AuthService;
    let emailService: EmailService;
    let securityService: SecurityService;
    let userController: UserController;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MockDatabaseModule,
            ],
            providers: [
                UserService,
                AuthService,
                EmailService,
                SecurityService,
                UserController,
            ],
        }).compile();

        authService = module.get<AuthService>(AuthService);
        userService = module.get<UserService>(UserService);
        emailService = module.get<EmailService>(EmailService);
        securityService = module.get<SecurityService>(SecurityService);
        userController = module.get<UserController>(UserController);
    });

    describe('loadUser', async () => {
        it('should call userService loadUser', async () => {
            const userDto = UserUtils.userToUserDto(TestUserGenerator.generateWithUuid());

            const loadUser = jest.spyOn(userService, 'loadUser').mockImplementation(() => userDto);

            const returnedUser = await userController.loadUser(userDto.id);

            expect(loadUser).toHaveBeenCalledWith(userDto.id);
            expect(userDto).toEqual(returnedUser);
        });
    });

    describe('updateUserDto', async () => {
        it('should call userService updateUserDto', async () => {
            const userDto = UserUtils.userToUserDto(TestUserGenerator.generateWithUuid());

            const updateUser = jest.spyOn(userService, 'updateUser').mockImplementation(() => userDto);

            const returnedUser = await userController.updateUser(userDto.id, userDto);

            expect(updateUser).toHaveBeenCalledWith(userDto.id, userDto);
            expect(userDto).toEqual(returnedUser);
        });
    });

    describe('deleteUser', async () => {
        it('should call userService deleteUser', async () => {
            const userDto = UserUtils.userToUserDto(TestUserGenerator.generateWithUuid());

            const deleteUser = jest.spyOn(userService, 'deleteUser').mockImplementation(() => {});

            await userController.deleteUser(userDto.id);

            expect(deleteUser).toHaveBeenCalledWith(userDto.id);
        });
    });
});