import 'jasmine';
import 'jest';

import { Test } from '@nestjs/testing';
import { UserService } from '../user.service';
import { MockDatabaseModule } from '../../database/mock-database.module';
import { DATA_REPOSITORY, DataRepository } from '../../database/data.repository';
import { Exceptions } from '../../common/exceptions';
import * as HttpStatus from 'http-status-codes';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';
import { UserUtils } from '../user.utils';
import { MockDataRepository } from '../../database/mock-data.repository';
import { User } from '../../../model/entities/user.entity';
import { Q } from '../../database/QueryUtils';
import { EntityStatus } from '../../../model/types/common/EntityStatus';

describe('UserService', () => {
    let userService: UserService;
    let dataRepository: DataRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [],
            providers: [
                {
                    provide: DATA_REPOSITORY,
                    useClass: MockDataRepository,
                },
                UserService,
            ],
        }).compile();

        dataRepository = module.get<DataRepository>(DATA_REPOSITORY);
        userService = module.get<UserService>(UserService);
    });

    describe('createUser', async () => {
        it('should throw if given a null uad', async () => {
            const uad = null;

            await expect(userService.createUser(uad))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('createUser', async () => {
        it('should throw if given a uad with no email', async () => {
            const uad = {
                emailToken: 'some-email-token',
            };

            await expect(userService.createUser(uad))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('createUser', async () => {
        it('should throw if given a uad with no emailToken', async () => {
            const uad = {
                email: 'some@new.email',
            };

            await expect(userService.createUser(uad))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('createUser', async () => {
        it('should call datarepository with user to create', async () => {
            const user = TestUserGenerator.generate();
            const uad = {
                email: user.email,
                emailToken: 'some-email-token',
            };

            const newUser = jest.spyOn(UserUtils, 'newUser').mockImplementation(() => user);
            const save = jest.spyOn(dataRepository, 'save').mockImplementation(() => user);

            await userService.createUser(uad);

            expect(newUser).toHaveBeenCalledWith(uad);
            expect(save).toHaveBeenCalledWith(User, user);
        });
    });

    describe('saveUser', async () => {
        it('should throw if given a null user', async () => {
            const user = null;

            await expect(userService.saveUser(user))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('saveUser', async () => {
        it('should call datarepository with user to save', async () => {
            const user = TestUserGenerator.generate();

            const save = jest.spyOn(dataRepository, 'save').mockImplementation(() => user);

            await userService.saveUser(user);

            expect(save).toHaveBeenCalledWith(User, user);
        });
    });

    describe('updateUser', async () => {
        it('should throw if given a null user', async () => {
            const userDto = null;
            const id = 'some-id';

            await expect(userService.updateUser(id, userDto))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('updateUser', async () => {
        it('should throw if given a blank id', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            const id = '    ';

            await expect(userService.updateUser(id, userDto))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('updateUser', async () => {
        it('should throw if given an id that doesn\'t match the userDto', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            const id = 'some-id';

            await expect(userService.updateUser(id, userDto))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('updateUser', async () => {
        it('should throw a NOT_FOUND exception if no user comes back from the repository', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);

            const findOneById = jest.spyOn(dataRepository, 'findOneById').mockImplementation(() => null);

            await expect(userService.updateUser(user.uuid, userDto))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.NOT_FOUND ]);
        });
    });

    describe('updateUser', async () => {
        it('should call datarepository with user to save', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const userDto = UserUtils.userToUserDto(user);
            userDto.lastName = 'newField';

            const findOneById = jest.spyOn(dataRepository, 'findOneById').mockImplementation(() => user);
            const save = jest.spyOn(dataRepository, 'save').mockImplementation(() => user);

            const updatedUser = await userService.updateUser(user.uuid, userDto);

            expect(findOneById).toHaveBeenCalledWith(User, user.uuid, Q.active<User>());

            user.lastName = userDto.lastName;
            expect(save).toHaveBeenCalledWith(User, user);
            expect(updatedUser).toEqual(userDto);
        });
    });

    describe('deleteUser', async () => {
        it('should throw if given a blank id', async () => {
            const id = '     ';

            await expect(userService.deleteUser(id))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.BAD_REQUEST ]);
        });
    });

    describe('deleteUser', async () => {
        it('should throw a NOT_FOUND exception if no user comes back from the repository', async () => {
            const user = TestUserGenerator.generateWithUuid();

            const findOneById = jest.spyOn(dataRepository, 'findOneById').mockImplementation(() => null);

            await expect(userService.deleteUser(user.uuid))
                .rejects
                .toThrow(Exceptions.DEFAULT_EXCEPTION_MESSAGES[ HttpStatus.NOT_FOUND ]);
        });
    });

    describe('deleteUser', async () => {
        it('should call datarepository with user to save', async () => {
            const user = TestUserGenerator.generateWithUuid();

            const findOneById = jest.spyOn(dataRepository, 'findOneById').mockImplementation(() => user);
            const save = jest.spyOn(dataRepository, 'save').mockImplementation(() => user);

            await userService.deleteUser(user.uuid);

            expect(findOneById).toHaveBeenCalledWith(User, user.uuid, Q.active<User>());

            user.entityStatus = EntityStatus.DELETED;
            expect(save).toHaveBeenCalledWith(User, user);
        });
    });
});