import 'jasmine';
import 'jest';

import { Test } from '@nestjs/testing';
import { AuthUtils } from '../AuthUtils';
import { UserService } from '../user.service';
import { EmailService } from '../../email/email.service';
import { AuthService } from '../auth.service';
import { SecurityService } from '../../security/security.service';
import { MockDatabaseModule } from '../../database/mock-database.module';
import { AuthController } from '../auth.controller';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';

describe('AuthController', () => {
    let userService: UserService;
    let authService: AuthService;
    let emailService: EmailService;
    let securityService: SecurityService;
    let authController: AuthController;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MockDatabaseModule,
            ],
            providers: [
                UserService,
                AuthService,
                EmailService,
                SecurityService,
                AuthController,
            ],
        }).compile();

        authService = module.get<AuthService>(AuthService);
        userService = module.get<UserService>(UserService);
        emailService = module.get<EmailService>(EmailService);
        securityService = module.get<SecurityService>(SecurityService);
        authController = module.get<AuthController>(AuthController);
    });

    describe('getToken', async () => {
        it('should call authService generateJsonWebToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = TestUserGenerator.getUadFromUser(user);
            const expectedJwt = AuthUtils.generateJsonWebTokenPayload(user);

            const generateJsonWebToken = jest.spyOn(authService, 'generateJsonWebToken').mockImplementation(() => expectedJwt);

            const returnedJwt = await authController.getToken(uad);

            expect(generateJsonWebToken).toHaveBeenCalledWith(uad);
            expect(expectedJwt).toEqual(returnedJwt);
        });
    });

    describe('verifyEmail', async () => {
        it('should call authService generateJsonWebToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const email = user.email;
            const emailToken = 'mock-email-token';
            const redirectUrl = TestUserGenerator.TEST_REDIRECT_URL;

            const expectedJwt = AuthUtils.generateJsonWebTokenPayload(user);
            const uad = {email, emailToken, redirectUrl};

            const generateJsonWebToken = jest.spyOn(authService, 'generateJsonWebToken').mockImplementation(() => expectedJwt);

            const returnedJwt = await authController.verifyEmail(email, emailToken, redirectUrl);

            expect(generateJsonWebToken).toHaveBeenCalledWith(uad);
            expect(expectedJwt).toEqual(returnedJwt);
        });
    });

    describe('requestLoginLink', async () => {
        it('should call authService generateJsonWebToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = TestUserGenerator.getUadFromUser(user);
            const expectedJwt = AuthUtils.generateJsonWebTokenPayload(user);

            const requestLoginLink = jest.spyOn(authService, 'requestLoginLink').mockImplementation(() => expectedJwt);

            await authController.requestLoginLink(uad);

            expect(requestLoginLink).toHaveBeenCalledWith(uad);
        });
    });

    describe('updatePassword', async () => {
        it('should call authService generateJsonWebToken', async () => {
            const user = TestUserGenerator.generateWithUuid();
            const uad = TestUserGenerator.getUadFromUser(user);
            const expectedJwt = AuthUtils.generateJsonWebTokenPayload(user);

            const updatePassword = jest.spyOn(authService, 'updatePassword').mockImplementation(() => expectedJwt);

            await authController.updatePassword(uad);

            expect(updatePassword).toHaveBeenCalledWith(uad);
        });
    });
});