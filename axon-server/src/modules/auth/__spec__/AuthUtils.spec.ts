import 'jasmine';

import { AuthUtils } from '../AuthUtils';
import { TestUserGenerator } from '../../../model/entities/mock/TestUserGenerator';
import { TestUtils } from '../../../utils/TestUtils';
import * as jsonwebtoken from 'jsonwebtoken';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';
import { DecodedJwt } from '../../../model/types/application/DecodedJwt';

describe('AuthUtils', () => {

    beforeEach(() => {
        // const module = await Test.createTestingModule({}).compile();
    });

    describe('generateSalt', () => {
        it('should generate a random base64 string with 172 characters', () => {
            const salt = AuthUtils.generateSalt();
            expect(TestUtils.BASE64.test(salt)).toEqual(true);
            expect(salt.length).toEqual(172);
        });
    });

    describe('generateSecureEmailToken', () => {
        it('should generate a random hex string with 128 characters', () => {
            const token = AuthUtils.generateSecureEmailToken();
            expect(TestUtils.HEX.test(token)).toEqual(true);
            expect(token.length).toEqual(128);
        });
    });

    describe('hashPassword', () => {
        it('should hash the password and salt into a valid 64 character hex string', () => {
            const passwordText = TestUtils.randomString(6);
            const salt = AuthUtils.generateSalt();
            const passwordDigest = AuthUtils.hashPassword(passwordText, salt);
            expect(TestUtils.HEX.test(passwordDigest)).toEqual(true);
            expect(passwordDigest.length).toEqual(64);
        });
    });

    describe('hashPassword', () => {
        it('should throw an exception if passed a null salt', () => {
            const passwordText = TestUtils.randomString(6);
            expect(() => { AuthUtils.hashPassword(passwordText, null); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('hashPassword', () => {
        it('should throw an exception if passed a null password', () => {
            const salt = AuthUtils.generateSalt();
            expect(() => { AuthUtils.hashPassword(null, salt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('hashPassword', () => {
        it('should throw an exception if passed a blank password', () => {
            const salt = AuthUtils.generateSalt();
            expect(() => { AuthUtils.hashPassword('   ', salt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('passwordMatches', () => {
        it('should return true if the hashed value matches the given password', () => {
            const salt = AuthUtils.generateSalt();
            const password = TestUtils.randomString(6);
            const passwordDigest = AuthUtils.hashPassword(password, salt);
            const matches = AuthUtils.passwordMatches(password, salt, passwordDigest);
            expect(matches).toEqual(true);
        });
    });

    describe('passwordMatches', () => {
        it('should return false if the hashed value does not match the given password', () => {
            const salt = AuthUtils.generateSalt();
            const password = TestUtils.randomString(6);
            const passwordDigest = AuthUtils.hashPassword(password, salt);
            const matches = AuthUtils.passwordMatches(password + 'a', salt, passwordDigest);
            expect(matches).toEqual(false);
        });
    });

    describe('passwordMatches', () => {
        it('should return false if the given password is blank', () => {
            const salt = AuthUtils.generateSalt();
            const password = TestUtils.randomString(6);
            const passwordDigest = AuthUtils.hashPassword(password, salt);
            const matches = AuthUtils.passwordMatches('', salt, passwordDigest);
            expect(matches).toEqual(false);
        });
    });

    describe('passwordMatches', () => {
        it('should return false if the salt is blank', () => {
            const salt = AuthUtils.generateSalt();
            const password = TestUtils.randomString(6);
            const passwordDigest = AuthUtils.hashPassword(password, salt);
            const matches = AuthUtils.passwordMatches(password, '', passwordDigest);
            expect(matches).toEqual(false);
        });
    });

    describe('passwordMatches', () => {
        it('should return false if the given digest is blank', () => {
            const salt = AuthUtils.generateSalt();
            const password = TestUtils.randomString(6);
            const matches = AuthUtils.passwordMatches(password, salt, '');
            expect(matches).toEqual(false);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return true if the token is not expired and matches the digest', () => {
            const salt = AuthUtils.generateSalt();
            const password = AuthUtils.generateSecureEmailToken();
            const digest = AuthUtils.hashPassword(password, salt);
            const expiration = AuthUtils.getExpirationForSignup();
            const matches = AuthUtils.isEmailTokenValid(password, salt, expiration, digest);
            expect(matches).toEqual(true);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return false if the token is expired and matches the digest', () => {
            const salt = AuthUtils.generateSalt();
            const password = AuthUtils.generateSecureEmailToken();
            const digest = AuthUtils.hashPassword(password, salt);
            const expiration = new Date(Date.now() - 5000);
            const matches = AuthUtils.isEmailTokenValid(password, salt, expiration, digest);
            expect(matches).toEqual(false);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return false if the token is not expired but does not match the digest', () => {
            const salt = AuthUtils.generateSalt();
            const password = AuthUtils.generateSecureEmailToken();
            const digest = AuthUtils.hashPassword(password, salt);
            const expiration = AuthUtils.getExpirationForSignup();
            const matches = AuthUtils.isEmailTokenValid(password + 'a', salt, expiration, digest);
            expect(matches).toEqual(false);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return false if the token and digest are valid but the password is blank', () => {
            const salt = AuthUtils.generateSalt();
            const password = '     ';
            const digest = '7879981D4F226A8F0191D36730C07205D7A5FF1C780FCA9B2F905F25264CF636'; // valid sha256 of 5 spaces
            const expiration = AuthUtils.getExpirationForSignup();
            const matches = AuthUtils.isEmailTokenValid(password, salt, expiration, digest);
            expect(matches).toEqual(false);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return false if the expiration is null', () => {
            const salt = AuthUtils.generateSalt();
            const password = AuthUtils.generateSecureEmailToken();
            const digest = AuthUtils.hashPassword(password, salt);
            const expiration = null;
            const matches = AuthUtils.isEmailTokenValid(password, salt, expiration, digest);
            expect(matches).toEqual(false);
        });
    });

    describe('isEmailTokenValid', () => {
        it('should return false if the salt is null', () => {
            const salt = AuthUtils.generateSalt();
            const password = AuthUtils.generateSecureEmailToken();
            const digest = AuthUtils.hashPassword(password, salt);
            const expiration = AuthUtils.getExpirationForSignup();
            const matches = AuthUtils.isEmailTokenValid(password, null, expiration, digest);
            expect(matches).toEqual(false);
        });
    });

    describe('getExpirationForLoginLink', () => {
        it('should return a timestamp that is not more than 28 days into the future', () => {
            const notTooFarIntoFuture = 1000 * 60 * 60 * 24 * 28; // 28 days
            const loginExpiration = AuthUtils.getExpirationForLoginLink();
            expect(loginExpiration.getTime()).toBeLessThanOrEqual(Date.now() + notTooFarIntoFuture);
        });
    });

    describe('getExpirationForLoginLink', () => {
        it('should return a timestamp that is not less than 20 minutes into the future', () => {
            const tooShortIntoTheFuture = 1000 * 60 * 20; // twenty minutes
            const loginExpiration = AuthUtils.getExpirationForLoginLink();
            expect(loginExpiration.getTime()).toBeGreaterThan(Date.now() + tooShortIntoTheFuture);
        });
    });

    describe('getExpirationForSignup', () => {
        it('should return a timestamp that is not more than 28 days into the future', () => {
            const notTooFarIntoFuture = 1000 * 60 * 60 * 24 * 28; // 28 days
            const signupExpiration = AuthUtils.getExpirationForSignup();
            expect(signupExpiration.getTime()).toBeLessThanOrEqual(Date.now() + notTooFarIntoFuture);
        });
    });

    describe('getExpirationForSignup', () => {
        it('should return a timestamp that is not less than 2 days into the future', () => {
            const tooShortIntoTheFuture = 1000 * 60 * 20; // 2 days
            const signupExpiration = AuthUtils.getExpirationForSignup();
            expect(signupExpiration.getTime()).toBeGreaterThan(Date.now() + tooShortIntoTheFuture);
        });
    });

    describe('recentlyAttempted', () => {
        it('should return true if given date that was less than .8 seconds before', () => {
            const recentAttemptMin = 1000 * .8;
            const date = new Date(Date.now() - recentAttemptMin);
            const actual = AuthUtils.recentlyAttempted(date);
            expect(actual).toEqual(true);
        });
    });

    describe('recentlyAttempted', () => {
        it('should return false if given date was more than one and a half seconds before', () => {
            const recentAttemptMax = 1000 * 1.5;
            const date = new Date(Date.now() - recentAttemptMax);
            const actual = AuthUtils.recentlyAttempted(date);
            expect(actual).toEqual(false);
        });
    });

    describe('recentlyAttempted', () => {
        it('should return false if the given date is null', () => {
            const actual = AuthUtils.recentlyAttempted(null);
            expect(actual).toEqual(false);
        });
    });

    describe('isUserValid', () => {
        it('should return true if the user exists and does not have their account locked', () => {
            const user = TestUserGenerator.generate();
            const actual = AuthUtils.isUserValid(user);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isUserValid', () => {
        it('should return false if user is null', () => {
            const actual = AuthUtils.isUserValid(null);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isUserValid', () => {
        it('should return false if user account is locked', () => {
            const user = TestUserGenerator.generate();
            user.accountLocked = true;
            const actual = AuthUtils.isUserLogginable(user);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('isUserLogginable', () => {
        it('should return true if the user is valid and has not recently attempted login', () => {
            const user = TestUserGenerator.generate();
            const actual = AuthUtils.isUserLogginable(user);
            const expected = true;
            expect(actual).toEqual(expected);
        });
    });

    describe('isUserLogginable', () => {
        it('should return false if last login attempt was less than 1 second before', () => {
            const user = TestUserGenerator.generate();
            user.lastAttempt = new Date(Date.now());
            const actual = AuthUtils.isUserLogginable(user);
            const expected = false;
            expect(actual).toEqual(expected);
        });
    });

    describe('mapUserToJwt', () => {
        it('should return a JwtPayload with the user email and uuid set correctly', () => {
            const user = TestUserGenerator.generate();
            const jwt = AuthUtils.mapUserToJwt(user);
            expect(jwt.email).toEqual(user.email);
            expect(jwt.uuid).toEqual(user.uuid);
        });
    });

    describe('mapUserToJwt', () => {
        it('should throw if given a null user', () => {
            expect(() => { AuthUtils.mapUserToJwt(null); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('createJwtPayload', () => {
        it('should return a JwtPayload with the jwt token and an estimated expiration set to now + default expiration time', () => {
            const user = TestUserGenerator.generate();
            const jwt = 'some.json.webtoken';
            const payload = AuthUtils.createJwtPayload(user, jwt);
            expect(payload.access_token).toEqual(jwt);
            expect(payload.estimated_expiration).toBeLessThanOrEqual(Date.now() + (AuthUtils.JWT_TOKEN_EXPIRATION_TIME_SECONDS * 1000));
            expect(payload.estimated_expiration).toBeGreaterThanOrEqual(Date.now() + (AuthUtils.JWT_TOKEN_EXPIRATION_TIME_SECONDS * 1000) - 1000);
        });
    });

    describe('createJwtPayload', () => {
        it('should return a JwtPayload with the user email and uuid', () => {
            const user = TestUserGenerator.generate();
            const jwt = 'some.json.webtoken';
            const payload = AuthUtils.createJwtPayload(user, jwt);
            expect(payload.email).toEqual(user.email);
            expect(payload.uuid).toEqual(user.uuid);
        });
    });

    describe('createJwtPayload', () => {
        it('should return a JwtPayload with hasPassword set to false if the user does not have a password', () => {
            const user = TestUserGenerator.generate();
            user.passwordDigest = null;
            const jwt = 'some.json.webtoken';
            const payload = AuthUtils.createJwtPayload(user, jwt);
            expect(payload.hasPassword).toEqual(false);
        });
    });

    describe('createJwtPayload', () => {
        it('should return a JwtPayload with hasPassword set to true if the user has a password', () => {
            const user = TestUserGenerator.generate();
            user.passwordDigest = AuthUtils.hashPassword('somePassword', 'someSalt');
            const jwt = 'some.json.webtoken';
            const payload = AuthUtils.createJwtPayload(user, jwt);
            expect(payload.hasPassword).toEqual(true);
        });
    });

    describe('createJwtPayload', () => {
        it('should throw an HttpException if the given user is null', () => {
            const user = null;
            const jwt = 'some.json.webtoken';
            expect(() => { AuthUtils.createJwtPayload(user, jwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('createJwtPayload', () => {
        it('should throw an HttpException if the given encodedJwt is blank', () => {
            const user = TestUserGenerator.generate();
            const jwt = '  ';
            expect(() => { AuthUtils.createJwtPayload(user, jwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('encodeAndSignJwt', () => {
        it('should return an encoded and signed JsonWebToken which matches the inputs when it gets decoded', () => {
            const jwt: DecodedJwt = {
                email: 'a@b.c',
                uuid: 'uuid3',
            };
            const encodedJwt = AuthUtils.encodeAndSignJwt(jwt);
            const decodedJwt = AuthUtils.verifyAndDecodeJwt(encodedJwt);
            expect(decodedJwt.email).toEqual(jwt.email);
            expect(decodedJwt.uuid).toEqual(jwt.uuid);
        });
    });

    describe('encodeAndSignJwt', () => {
        it('should throw an HttpException if given a null jwt', () => {
            expect(() => { AuthUtils.encodeAndSignJwt(null); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('verifyAndDecodeJwt', () => {
        it('should throw an HttpException if given a blank encodedJwt', () => {
            const encodedJwt = '   ';
            expect(() => { AuthUtils.verifyAndDecodeJwt(encodedJwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('verifyAndDecodeJwt', () => {
        it('should throw an HttpException if given an invalid encodedJwt', () => {
            const encodedJwt = 'an.invalid.jwt';
            expect(() => { AuthUtils.verifyAndDecodeJwt(encodedJwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('verifyAndDecodeJwt', () => {
        it('should successfully decode an encoded jsonwebtoken that has been signed with the default secret', () => {
            const secret = 'JWT_SECREF_3fj-c9dj281m-cc0alxxjNHq-kkM9A991';
            const jwt: DecodedJwt = {
                email: 'a@b.c',
                uuid: 'uuid3',
            };
            const options: jsonwebtoken.SignOptions = {
                expiresIn: 50, // 50 seconds
            };
            const encodedJwt = jsonwebtoken.sign(jwt, secret, options);
            const decodedJwt = AuthUtils.verifyAndDecodeJwt(encodedJwt);
            expect(decodedJwt.email).toEqual(jwt.email);
            expect(decodedJwt.uuid).toEqual(jwt.uuid);
        });
    });

    describe('verifyAndDecodeJwt', () => {
        it('should throw if asked to verify an expired token', () => {
            const secret = 'JWT_SECRET_3fj9dj28j019djnm12308SDFF';
            const jwt: DecodedJwt = {
                email: 'a@b.c',
                uuid: 'uuid3',
            };
            const options: jsonwebtoken.SignOptions = {
                expiresIn: -1, // 1 second ago
            };
            const encodedJwt = jsonwebtoken.sign(jwt, secret, options);
            expect(() => { AuthUtils.verifyAndDecodeJwt(encodedJwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('verifyAndDecodeJwt', () => {
        it('should throw if given an encoded JsonWebToken that was of type DecodedJwt before it was signed', () => {
            const secret = 'JWT_SECRET_3fj9dj28j019djnm12308SDFF';
            const weirdJwt = {
                something: 'a@b.c',
                somethingElse: 'asdfasdfasdf',
                somethingWorse: 'asdfasdf',
            };
            const options: jsonwebtoken.SignOptions = {
                expiresIn: 50, // 50 seconds
            };
            const encodedJwt = jsonwebtoken.sign(weirdJwt, secret, options);
            expect(() => { AuthUtils.verifyAndDecodeJwt(encodedJwt); })
                .toThrow(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('cleanUadAndFail', () => {
        it('should return false and wipe password, oldPassword, and emailToken', () => {
            const someString = 'something';
            const uad: UserAuthenticationDto = {
                email: someString,
                password: someString,
                emailToken: someString,
                oldPassword: someString,
                redirectUrl: someString,
            };
            const result = AuthUtils.cleanUadAndFail(uad);
            expect(result).toEqual(false);
            expect(uad.password).not.toContain(someString);
            expect(uad.emailToken).not.toContain(someString);
            expect(uad.oldPassword).not.toContain(someString);
        });
    });

    describe('cleanUadAndSucceed', () => {
        it('should return true and wipe password, oldPassword, and emailToken', () => {
            const someString = 'something';
            const uad: UserAuthenticationDto = {
                email: someString,
                password: someString,
                emailToken: someString,
                oldPassword: someString,
                redirectUrl: someString,
            };
            const result = AuthUtils.cleanUadAndSucceed(uad);
            expect(result).toEqual(true);
            expect(uad.password).not.toContain(someString);
            expect(uad.emailToken).not.toContain(someString);
            expect(uad.oldPassword).not.toContain(someString);
        });
    });

    describe('overwritePassword', () => {
        it('should return without throwing if the given uad is null', () => {
            AuthUtils.overwritePassword(null);
        });
    });

    describe('overwritePassword', () => {
        it('should wipe password, oldPassword, and emailToken and appends a random number of 0s', () => {
            const someString = 'something';
            const uad: UserAuthenticationDto = {
                email: someString,
                password: someString,
                emailToken: someString,
                oldPassword: someString,
                redirectUrl: someString,
            };
            AuthUtils.overwritePassword(uad);
            expect(uad.password).not.toContain(someString);
            expect(uad.emailToken).not.toContain(someString);
            expect(uad.oldPassword).not.toContain(someString);
        });
    });

    describe('newUnauthenticatedError', () => {
        it('should return an HttpException with a 401 error code and a message that doesn\'t leak information', () => {
            const error = AuthUtils.newUnauthenticatedError();
            expect(error.getStatus()).toEqual(401);
            expect(error.message).toEqual('Naw tho');
        });
    });

    describe('setEmailTokenOnUser', () => {
        it('should set the digest of the requested emailToken on the user and update the lastAttempt', () => {
            const user = TestUserGenerator.generate();
            user.lastAttempt = null;
            user.emailTokenDigest = null;
            user.emailTokenExpiration = null;
            const emailToken = AuthUtils.generateSecureEmailToken();
            AuthUtils.setEmailTokenOnUser(user, emailToken);
            expect(user.emailTokenDigest).toEqual(AuthUtils.hashPassword(emailToken, user.salt));
            expect(user.emailTokenExpiration.getTime()).toBeGreaterThan(AuthUtils.getExpirationForLoginLink().getTime() - 1000);
            expect(user.emailTokenExpiration.getTime()).toBeLessThan(AuthUtils.getExpirationForLoginLink().getTime() + 1000);
            expect(user.lastAttempt.getTime()).toBeGreaterThan(Date.now() - 1000);
            expect(user.lastAttempt.getTime()).toBeLessThan(Date.now() + 1000);
        });
    });

    describe('setEmailTokenOnUser', () => {
        it('should throw an HttpException if the given emailToken is null', () => {
            const user = TestUserGenerator.generate();
            expect(() => { AuthUtils.setEmailTokenOnUser(user, null); })
                .toThrowError(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('setEmailTokenOnUser', () => {
        it('should throw an HttpException if the given emailToken is blank', () => {
            const user = TestUserGenerator.generate();
            expect(() => { AuthUtils.setEmailTokenOnUser(user, '    '); })
                .toThrowError(AuthUtils.newUnauthenticatedError().message);
        });
    });

    describe('setEmailTokenOnUser', () => {
        it('should throw an HttpException if the given user is null', () => {
            const emailToken = 'someString';
            expect(() => { AuthUtils.setEmailTokenOnUser(null, emailToken); })
                .toThrowError(AuthUtils.newUnauthenticatedError().message);
        });
    });
});