import { Injectable, Inject } from '@nestjs/common';
import { User } from '../../model/entities/user.entity';
import { EntityStatus } from '../../model/types/common/EntityStatus';
import { Q } from '../database/QueryUtils';
import { UserDto } from '../../model/types/application/UserDto';
import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { AuthUtils } from './AuthUtils';
import { DATA_REPOSITORY, DataRepository } from '../database/data.repository';
import { UserUtils } from './user.utils';
import { StringUtils } from '../../utils/StringUtils';
import { Exceptions } from '../common/exceptions';
import * as HttpStatus from 'http-status-codes';

@Injectable()
export class UserService {

    constructor(@Inject(DATA_REPOSITORY) private readonly dataRepository: DataRepository) {
    }

    public async createUser(uad: UserAuthenticationDto): Promise<User> {
        if (!uad || StringUtils.isBlank(uad.email) || StringUtils.isBlank(uad.emailToken)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create new user with given uad: ' + JSON.stringify(uad));
        }

        const user = UserUtils.newUser(uad);
        // AuthUtils.overwritePassword(uad);
        return await this.dataRepository.save(User, user);
    }

    /**
     * Saves (creates or updates) a {User}
     *
     * @param {string} user
     * @param {UserDto} userDto
     * @returns {Promise<User>}
     */
    public async saveUser(user: User): Promise<User> {
        if (!user) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot save null user');
        }
        return await this.dataRepository.save(User, user);
    }

    /**
     * Updates an existing User given the limited field set in the {UserDto}
     *
     * @param {string} id
     * @param {UserDto} userDto
     * @returns {Promise<User>}
     */
    public async updateUser(id: string, userDto: UserDto): Promise<UserDto> {
        if (StringUtils.isBlank(id) || !userDto || userDto.id !== id) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot update user (id=' + id + ') with UserDto: ' + JSON.stringify(userDto));
        }

        const existingUser = await this.dataRepository.findOneById(User, id, Q.active<User>());
        if (!existingUser) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not load user with id: ' + id);
        }

        const user = UserUtils.mergeUserDtoIntoUser(existingUser, userDto);

        const updatedUser = await this.dataRepository.save(User, user);
        return UserUtils.userToUserDto(updatedUser);
    }

    /**
     * Soft deletes a {User} by id
     *
     * @param {string} id
     * @returns {Promise<void>}
     */
    public async deleteUser(id: string): Promise<void> {
        if (StringUtils.isBlank(id)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot delete user with blank id');
        }

        const user = await this.dataRepository.findOneById(User, id, Q.active<User>());
        if (!user) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not load user with id: ' + id);
        }
        user.entityStatus = EntityStatus.DELETED;
        await this.dataRepository.save(User, user);
    }

    /**
     * Loads a {User} from the database given the unique id
     *
     * @param {string} id
     * @returns {Promise<UserDto>}
     */
    public async loadUser(id: string): Promise<UserDto> {
        if (StringUtils.isBlank(id)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Could not load user with blank id');
        }

        const user = await this.dataRepository.findOneById(User, id, Q.active<User>());
        if (!user) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not load user with id: ' + id);
        }
        return UserUtils.userToUserDto(user);
    }

    /**
     * Loads a {User} from the database given an email
     *
     * @param {string} email
     * @returns {Promise<User>}
     */
    public async getUserByEmail(email: string): Promise<User> {
        if (StringUtils.isBlank(email)) {
            return null;
        }

        return await this.dataRepository.findOne(User, Q.active<User>({where: {email}}));
    }
}