import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { AuthUtils } from './AuthUtils';
import { EntityStatus } from '../../model/types/common/EntityStatus';
import { Address } from '../../model/entities/address.entity';
import { User } from '../../model/entities/user.entity';
import { UserDto } from '../../model/types/application/UserDto';
import { AddressDto } from '../../model/types/place/AddressDto';
import { StringUtils } from '../../utils/StringUtils';
import { Exceptions } from '../common/exceptions';
import * as HttpStatus from 'http-status-codes';

export class UserUtils {

    /**
     * Creates a new {User} object given a {UserAuthenticationDto}
     *
     * @param {UserAuthenticationDto} uad
     * @returns {User}
     */
    public static newUser(uad: UserAuthenticationDto): User {
        if (!uad || StringUtils.isBlank(uad.email)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Got an invalid uad when trying to create a new User: ' + JSON.stringify(uad));
        }

        const salt = AuthUtils.generateSalt();
        return {
            email: uad.email,
            passwordDigest: uad.password ? AuthUtils.hashPassword(uad.password, salt) : null,
            salt,
            emailVerified: false,
            mobileNumber: null,
            mobileNumberVerified: false,
            emailTokenDigest: uad.emailToken ? AuthUtils.hashPassword(uad.emailToken, salt) : null,
            emailTokenExpiration: uad.emailToken ? AuthUtils.getExpirationForSignup() : null,
            firstName: null,
            lastName: null,
            iconUrl: null,
            dob: null,
            address: new Address(),
            passwordAttempts: 0,
            lastAttempt: new Date(Date.now()),
            accountLocked: false,
            uuid: null,
            createdDate: null,
            lastModifiedDate: null,
            createdBy: 'self',
            lastModifiedBy: 'self',
            version: null,
            entityStatus: EntityStatus.ACTIVE,
        };
    }

    /**
     * Maps a {User} to a {UserDto}
     *
     * The resulting fields are the fields that are safe/meaningful to return to the end user
     *
     * @param {User} user
     * @returns {UserDto}
     */
    public static userToUserDto(user: User): UserDto {
        if (!user || StringUtils.isBlank(user.uuid)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create a UserDto out of: ' + JSON.stringify(user));
        }

        return {
            id: user.uuid,
            email: user.email,
            address: UserUtils.addressDtoToAddress(user.address),
            dob: user.dob,
            firstName: user.firstName,
            lastName: user.lastName,
            iconUrl: user.iconUrl,
        };
    }

    /**
     * Merges a {UserDto} into the given {User}
     *
     * Convenient for updating an existing user, while preventing the caller from overwriting fields they should not have access to
     *
     * @param {User} user
     * @param {UserDto} userDto
     * @returns {User}
     */
    public static mergeUserDtoIntoUser(user: User, userDto: UserDto): User {
        if (!user || !userDto || StringUtils.isBlank(userDto.id) || userDto.id !== user.uuid) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot merge UserDto fields into User. UserDto: ' + JSON.stringify(userDto) + '; User: ' + JSON.stringify(user));
        }

        user.address = UserUtils.addressDtoToAddress(userDto.address);
        user.dob = userDto.dob;
        user.firstName = userDto.firstName;
        user.lastName = userDto.lastName;
        user.iconUrl = userDto.iconUrl;
        return user;
    }

    /**
     * Maps an {Address} to an {AddressDto} for transfer over the network
     *
     * @param {Address} address
     * @returns {AddressDto}
     */
    public static addressToAddressDto(address: Address): AddressDto {
        return {
            id: address.uuid,
            place_id: address.place_id,
            street1: address.street1,
            street2: address.street2,
            city: address.city,
            state: address.state,
            province: address.province,
            postalCode: address.postalCode,
            postalCodeExtension: address.postalCodeExtension,
            country: address.country,
        };
    }

    /**
     * Maps an {AddressDto} to an {Address} for persistence
     *
     * @param {AddressDto} addressDto
     * @returns {Address}
     */
    public static addressDtoToAddress(addressDto: AddressDto): Address {
        if (!addressDto) {
            return null;
        }
        return new Address({
            uuid: addressDto.id,
            place_id: addressDto.place_id,
            street1: addressDto.street1,
            street2: addressDto.street2,
            city: addressDto.city,
            state: addressDto.state,
            province: addressDto.province,
            postalCode: addressDto.postalCode,
            postalCodeExtension: addressDto.postalCodeExtension,
            country: addressDto.country,
        });
    }
}