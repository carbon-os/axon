import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';
import { AuthService } from './auth.service';
import { EncodedJwt } from '../../model/types/application/EncodedJwt';
import { AuthUtils } from './AuthUtils';
import { StringUtils } from '../../utils/StringUtils';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

    constructor(private authService: AuthService) {
    }

    public static readonly BEARER_STRING = 'Bearer';

    public resolve(...args: any[]): MiddlewareFunction {
        return async (req, res, next) => {
            if (!req) {
                throw AuthUtils.newUnauthenticatedError();
            }

            const jwtString = AuthMiddleware.extractJwtFromHeaders(req.headers);
            const isValid = await this.authService.validateUserGivenJwt(jwtString);

            if (!isValid) {
                throw AuthUtils.newUnauthenticatedError();
            }
            next();
        };
    }

    public static extractJwtFromHeaders(headers: any): EncodedJwt {
        if (!headers) {
            throw AuthUtils.newUnauthenticatedError();
        }
        const authHeader = headers.authorization || headers.Authorization;

        if (StringUtils.isBlank(authHeader)) {
            throw AuthUtils.newUnauthenticatedError();
        }

        const bearerSplit = authHeader.split(' ');
        if (bearerSplit.length !== 2 || bearerSplit[ 0 ] !== AuthMiddleware.BEARER_STRING || StringUtils.isBlank(bearerSplit[ 1 ])) {
            throw AuthUtils.newUnauthenticatedError();
        }

        return bearerSplit[ 1 ] as EncodedJwt;
    }
}