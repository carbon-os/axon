import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { User } from '../../model/entities/user.entity';
import * as crypto from 'crypto';
import * as jsonwebtoken from 'jsonwebtoken';
import { StringUtils } from '../../utils/StringUtils';
import { TimeUtils } from '../../utils/TimeUtils';
import { DecodedJwt } from '../../model/types/application/DecodedJwt';
import { EncodedJwt } from '../../model/types/application/EncodedJwt';
import { JsonWebTokenPayload } from '../../model/types/application/JsonWebTokenPayload';
import { Exceptions } from '../common/exceptions';

export class AuthUtils {
    private static readonly JWT_SECRET = 'JWT_SECREF_3fj-c9dj281m-cc0alxxjNHq-kkM9A991'; // TODO
    public static readonly JWT_TOKEN_EXPIRATION_TIME_SECONDS: number = 60 * 60 * 24 * 7; // 1 week JWT expiration
    public static readonly ATTEMPT_RATE_LIMIT: number = 1000; // min 1 seconds in between tries
    public static readonly ATTEMPT_LIMIT: number = 50; // 50 failed attempts before locking account (if we hit this we know there's an organized attack)

    private static readonly LOGIN_LINK_EXPIRATION_SECONDS: number = 60 * 60 * 24 * 28; // 28 day email token expiration
    private static readonly SIGNUP_EMAIL_EXPIRATION_SECONDS: number = 60 * 60 * 24 * 28; // 28 day email token expiration

    public static readonly DEFAULT_JWT_OPTIONS: jsonwebtoken.SignOptions = {
        expiresIn: AuthUtils.JWT_TOKEN_EXPIRATION_TIME_SECONDS,
    };

    /**
     * generates random, 128-byte base64-encoded salt
     *
     * @returns {string}
     */
    public static generateSalt(): string {
        return crypto.randomBytes(128).toString('base64');
    }

    /**
     * generates a secure, 64 byte hex-encoded password
     *
     * @returns {string}
     */
    public static generateSecureEmailToken(): string {
        return crypto.randomBytes(64).toString('hex');
    }

    /**
     * hashes password and salt
     *
     * @param {string} password
     * @param {string} salt
     * @returns {string}
     */
    public static hashPassword(password: string, salt: string): string {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(salt)) {
            throw AuthUtils.newUnauthenticatedError('Cannot hash a blank password and/or blank salt');
        }
        return crypto.createHmac('sha256', password + salt).digest('hex');
    }

    /**
     * checks to see if some password/salt combination matches some existing digest
     *
     * overwrites the password with zeroes as soon as the
     *
     * @param {string} attemptedPassword
     * @param {string} salt
     * @param {string} digest
     * @returns {boolean}
     */
    public static passwordMatches(attemptedPassword: string, salt: string, digest: string): boolean {
        if (StringUtils.isBlank(attemptedPassword) || StringUtils.isBlank(salt) || StringUtils.isBlank(digest)) {
            return false;
        }
        return AuthUtils.hashPassword(attemptedPassword, salt) === digest;
    }

    /**
     * Checks to see if the given emailToken is both unexpired and matches the digest
     *
     * @returns {Date}
     */
    public static isEmailTokenValid(emailToken: string, salt: string, expiration: Date, digest: string): boolean {
        if (!expiration || StringUtils.isBlank(salt) || StringUtils.isBlank(digest) || StringUtils.isBlank(emailToken)) {
            return false;
        }
        if (expiration.getTime() < Date.now()) {
            return false;
        }
        return AuthUtils.passwordMatches(emailToken, salt, digest);
    }

    /**
     * Returns the default Expiration Date for password reset emails
     *
     * @returns {Date}
     */
    public static getExpirationForLoginLink(): Date {
        return TimeUtils.getNowPlusSeconds(AuthUtils.LOGIN_LINK_EXPIRATION_SECONDS);
    }

    /**
     * Returns the default Expiration Date for email verification emails
     *
     * @returns {Date}
     */
    public static getExpirationForSignup(): Date {
        return TimeUtils.getNowPlusSeconds(AuthUtils.SIGNUP_EMAIL_EXPIRATION_SECONDS);
    }

    /**
     * checks to see if the lastAttempt date is "too recent"
     *
     * That is: is it within the ATTEMPT_RATE_LIMIT of Date.now()?
     *
     * @param {Date} lastAttempt
     * @returns {boolean}
     */
    public static recentlyAttempted(lastAttempt: Date): boolean {
        if (!lastAttempt) {
            return false;
        }
        return lastAttempt.getTime() + AuthUtils.ATTEMPT_RATE_LIMIT > Date.now();
    }

    /**
     * checks to see if the user can even attempt to log in
     *
     * @param {User} user
     * @returns {boolean}
     */
    public static isUserValid(user: User): boolean {
        return !!user && !user.accountLocked;
    }

    /**
     * checks to see if the user can even attempt to log in
     *
     * @param {User} user
     * @returns {boolean}
     */
    public static isUserLogginable(user: User): boolean {
        if (!user) {
            return false;
        }
        return AuthUtils.isUserValid(user) && !AuthUtils.recentlyAttempted(user.lastAttempt);
    }

    /**
     * given a user, returns a decoded JsonWebToken
     *
     * @param {User} user
     * @returns {boolean}
     */
    public static mapUserToJwt(user: User): DecodedJwt {
        if (!user) {
            throw AuthUtils.newUnauthenticatedError('Cannot map null user to jwt');
        }
        return {
            email: user.email,
            uuid: user.uuid,
        };
    }

    /**
     * signs and encodes a {DecodedJwt} into an {EncodedJwt}
     *
     * @param {DecodedJwt} decodedJwt
     * @returns {EncodedJwt}
     */
    public static encodeAndSignJwt(decodedJwt: DecodedJwt): EncodedJwt {
        if (!decodedJwt || !decodedJwt.email || !decodedJwt.uuid) {
            throw AuthUtils.newUnauthenticatedError('Cannot encode and sign a jwt: ' + JSON.stringify(decodedJwt));
        }

        return jsonwebtoken.sign(decodedJwt, AuthUtils.JWT_SECRET, AuthUtils.DEFAULT_JWT_OPTIONS);
    }

    /**
     * given a user and an encodedJwt, returns a new {JsonWebTokenPayload} with the default expiration date
     *
     * @param {User} user
     * @param {EncodedJwt} encodedJwt
     * @returns {JsonWebTokenPayload}
     */
    public static createJwtPayload(user: User, encodedJwt: EncodedJwt): JsonWebTokenPayload {
        if (!user || StringUtils.isBlank(encodedJwt)) {
            throw AuthUtils.newUnauthenticatedError('Cannot create jwt payload with user: ' + JSON.stringify(user) + '; and jwt: ' + encodedJwt);
        }

        return {
            hasPassword: !!user.passwordDigest,
            estimated_expiration: Date.now() + (AuthUtils.JWT_TOKEN_EXPIRATION_TIME_SECONDS * 1000),
            uuid: user.uuid,
            email: user.email,
            access_token: encodedJwt,
        };
    }

    /**
     * Creates a JsonWebTokenPayload given a User
     *
     * @param {User} user
     * @returns {JsonWebTokenPayload}
     */
    public static generateJsonWebTokenPayload(user: User): JsonWebTokenPayload {
        const decodedJwt = AuthUtils.mapUserToJwt(user);
        const encodedJwt = AuthUtils.encodeAndSignJwt(decodedJwt);

        return AuthUtils.createJwtPayload(user, encodedJwt);
    }

    /**
     * decodes and verifies the JsonWebToken
     *
     * throws an HttpException of type 401 if the JWT is invalid
     *
     * @param {EncodedJwt} encodedJwt
     * @returns {DecodedJwt}
     */
    public static verifyAndDecodeJwt(encodedJwt: EncodedJwt): DecodedJwt {
        if (StringUtils.isBlank(encodedJwt)) {
            throw AuthUtils.newUnauthenticatedError('Cannot verify or decode a blank encoded jwt');
        }

        let unknownPayload = null;
        try {
            unknownPayload = jsonwebtoken.verify(encodedJwt, AuthUtils.JWT_SECRET) as any;
        } catch (e) {
            throw AuthUtils.newUnauthenticatedError('Error decoding JWT: ' + encodedJwt + '; error: ' + e.message);
        }

        if (!!unknownPayload && unknownPayload.email && unknownPayload.uuid) {
            return unknownPayload as DecodedJwt;
        }

        throw AuthUtils.newUnauthenticatedError('Unknown issue with decoding jwt payload: ' + unknownPayload);
    }

    /**
     * Zeroes out the password and returns false
     *
     * useful for exiting many auth service methods
     *
     * @param {UserAuthenticationDto} uad
     * @returns {any}
     */
    public static cleanUadAndFail(uad: UserAuthenticationDto): false {
        AuthUtils.overwritePassword(uad);
        return false;
    }

    /**
     * Zeroes out the password and returns true
     *
     * useful for exiting many auth service methods
     *
     * @param {UserAuthenticationDto} uad
     * @returns {any}
     */
    public static cleanUadAndSucceed(uad: UserAuthenticationDto): true {
        AuthUtils.overwritePassword(uad);
        return true;
    }

    /**
     * Zeroes out the password and emailToken and appends a random number of extra 0s to conceal length
     *
     * @param {UserAuthenticationDto} uad
     */
    public static overwritePassword(uad: UserAuthenticationDto): void {
        if (!uad) {
            return;
        }
        if (uad.password) {
            uad.password = uad.password.replace(/./g, '0').concat(Array(Math.floor(Math.random() * (40 - 1 + 1) + 1)).join('0'));
        }
        if (uad.oldPassword) {
            uad.oldPassword = uad.oldPassword.replace(/./g, '0').concat(Array(Math.floor(Math.random() * (40 - 1 + 1) + 1)).join('0'));
        }
        if (uad.emailToken) {
            uad.emailToken = uad.emailToken.replace(/./g, '0').concat(Array(Math.floor(Math.random() * (40 - 1 + 1) + 1)).join('0'));
        }
    }

    /**
     * Log message will be written to the logs, but not returned to the user.  All 401 errors should return
     * with the same user message so as to not leak information.
     *
     * Do not put private information in log messages
     *
     * @param {string} logMessage
     * @returns {Error}
     */
    public static newUnauthenticatedError(logMessage?: string): Error {
        return Exceptions.new(401, logMessage);
    }

    public static setEmailTokenOnUser(user: User, emailToken: string): void {
        if (StringUtils.isBlank(emailToken) || !user) {
            throw AuthUtils.newUnauthenticatedError('Cannot set emailToken on user. Either the emailToken is blank or the user is null.');
        }
        user.emailTokenDigest = AuthUtils.hashPassword(emailToken, user.salt);
        user.emailTokenExpiration = AuthUtils.getExpirationForLoginLink();
        user.lastAttempt = new Date(Date.now());
    }
}