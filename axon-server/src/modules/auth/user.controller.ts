import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Put } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { UserDto } from '../../model/types/application/UserDto';

@Controller('user')
export class UserController {
    constructor(private readonly authService: AuthService,
                private readonly userService: UserService) {
    }

    @Get(':id')
    @HttpCode(HttpStatus.OK)
    public async loadUser(@Param('id') id: string): Promise<UserDto> {
        return this.userService.loadUser(id);
    }

    @Put(':id')
    @HttpCode(HttpStatus.OK)
    public async updateUser(@Param('id') id: string, @Body() user: UserDto): Promise<UserDto> {
        return this.userService.updateUser(id, user);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.OK)
    public async deleteUser(@Param('id') id: string): Promise<void> {
        return this.userService.deleteUser(id);
    }
}