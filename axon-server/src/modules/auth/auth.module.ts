import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { DatabaseModule } from '../database/database.module';
import { EmailModule } from '../email/email.module';
import { SecurityModule } from '../security/security.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { AuthMiddleware } from './auth.middleware';
import { MessageController } from '../chat/message.controller';

@Module({
    imports: [
        DatabaseModule,
        EmailModule,
        SecurityModule,
    ],
    providers: [
        AuthService,
        UserService,
    ],
    controllers: [
        AuthController,
        UserController,
    ],
})
export class AuthModule implements NestModule {
    public configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .forRoutes(
                MessageController,
                UserController,
            );
    }
}