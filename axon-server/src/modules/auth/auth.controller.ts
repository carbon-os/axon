import { Body, Controller, Get, HttpCode, HttpStatus, Post, Put, Query } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { JsonWebTokenPayload } from '../../model/types/application/JsonWebTokenPayload';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('jwt')
    @HttpCode(HttpStatus.OK)
    public async getToken(@Body() uad: UserAuthenticationDto): Promise<JsonWebTokenPayload> {
        return await this.authService.generateJsonWebToken(uad);
    }

    @Get('jwt')
    @HttpCode(HttpStatus.OK)
    public async verifyEmail(@Query('e') email: string,
                             @Query('t') emailToken: string,
                             @Query('r') redirectUrl: string): Promise<JsonWebTokenPayload> {
        const uad: UserAuthenticationDto = {email, emailToken, redirectUrl};
        return await this.authService.generateJsonWebToken(uad);
    }

    @Post('get-login-link')
    @HttpCode(HttpStatus.OK)
    public async requestLoginLink(@Body() uad: UserAuthenticationDto): Promise<void> {
        return this.authService.requestLoginLink(uad);
    }

    @Put('update-password')
    @HttpCode(HttpStatus.OK)
    public async updatePassword(@Body() uad: UserAuthenticationDto): Promise<void> {
        return this.authService.updatePassword(uad);
    }
}