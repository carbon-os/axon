import { Injectable, HttpException } from '@nestjs/common';
import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { User } from '../../model/entities/user.entity';
import { EmailService } from '../email/email.service';
import { SecurityService } from '../security/security.service';
import { JsonWebTokenPayload } from '../../model/types/application/JsonWebTokenPayload';
import { WELCOME_STATE } from '../../model/states/welcome.state';
import { DecodedJwt } from '../../model/types/application/DecodedJwt';
import { EncodedJwt } from '../../model/types/application/EncodedJwt';
import { AuthUtils } from './AuthUtils';
import { UserService } from './user.service';
import { TimeUtils } from '../../utils/TimeUtils';
import { Exceptions } from '../common/exceptions';
import * as HttpStatus from 'http-status-codes';
import { StringUtils } from '../../utils/StringUtils';

@Injectable()
export class AuthService {

    constructor(private readonly emailService: EmailService,
                private readonly userService: UserService,
                private readonly securityService: SecurityService) {
    }

    /**
     * Creates a new {User} if a user with the requested email does not already exist.
     *
     * If the user already exists, we send a login link to the existing user.
     *
     * @param {UserAuthenticationDto} uad requires email and password to be set
     * @returns {Promise<void>}
     */
    public async requestLoginLink(uad: UserAuthenticationDto): Promise<void> {
        if (!uad || StringUtils.isBlank(uad.email)) {
            throw AuthUtils.newUnauthenticatedError();
        }

        const user = await this.userService.getUserByEmail(uad.email);

        uad.emailToken = AuthUtils.generateSecureEmailToken();
        if (!user) {
            uad.redirectUrl = WELCOME_STATE;
            this.userService.createUser(uad);
        } else {
            AuthUtils.setEmailTokenOnUser(user, uad.emailToken);
            this.userService.saveUser(user);
        }

        return await this.emailService.sendLoginLinkEmail(uad);
    }

    /**
     * Generates a JWT given a {UserAuthenticationDto}
     *
     * @param {UserAuthenticationDto} uad
     * @returns {Promise<JsonWebTokenPayload>}
     */
    public async generateJsonWebToken(uad: UserAuthenticationDto): Promise<JsonWebTokenPayload> {
        if (!uad || StringUtils.isBlank(uad.email)) {
            throw AuthUtils.newUnauthenticatedError();
        }

        const user = await this.userService.getUserByEmail(uad.email);
        const valid = await this.validateUser(user, uad);

        if (!valid) {
            throw AuthUtils.newUnauthenticatedError();
        }

        return AuthUtils.generateJsonWebTokenPayload(user);
    }

    /**
     * validate Json Web Token
     *
     * @param {JsonWebTokenPayload} jwt
     * @returns {Promise<boolean>}
     */
    public async validateUserGivenJwt(jwt: EncodedJwt): Promise<boolean> {
        const payload: DecodedJwt = AuthUtils.verifyAndDecodeJwt(jwt);

        const user = await this.userService.getUserByEmail(payload.email);

        return AuthUtils.isUserValid(user);
    }

    /**
     * Sets a new password on the requested user.
     *
     * uad.password must be set to the new password
     * uad.oldPassword must be set to the existing password
     *
     * @param {UserAuthenticationDto} uad
     * @returns {Promise<void>}
     */
    public async updatePassword(uad: UserAuthenticationDto): Promise<void> {
        if (!uad || StringUtils.isBlank(uad.password)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot update password without a uad or password');
        }

        const user = await this.userService.getUserByEmail(uad.email);
        if (!AuthUtils.isUserValid(user)) {
            throw AuthUtils.newUnauthenticatedError();
        }

        if (user.passwordDigest && !AuthUtils.passwordMatches(uad.oldPassword, user.salt, user.passwordDigest)) {
            throw Exceptions.new(HttpStatus.FORBIDDEN, undefined, 'That existing password doesn\'t seem correct...');
        }

        user.passwordDigest = AuthUtils.hashPassword(uad.password, user.salt);
        user.passwordAttempts = 0;

        await this.userService.saveUser(user);
    }

    /**
     * Validates user, use with emailToken or password
     *
     * Checks that user exists, password matches, account is not locked, rate limit is not surpassed
     *
     * Increments the attempt number if the user exists but the password is wrong
     *
     * @param {User} user
     * @param {UserAuthenticationDto} uad
     * @returns {Promise<boolean>}
     */
    public async validateUser(user: User, uad: UserAuthenticationDto): Promise<boolean> {
        if (!AuthUtils.isUserLogginable(user) || !uad) {
            await TimeUtils.randomTimeVariationToPreventInformationLeak();
            return AuthUtils.cleanUadAndFail(uad);
        }

        let validated = false;
        if (uad.emailToken) {
            validated = await this.validateUserGivenEmailToken(user, uad.emailToken);
        } else {
            validated = await this.validateUserGivenPassword(user, uad.password);
        }

        AuthUtils.overwritePassword(uad);
        return validated;
    }

    /**
     * increments the passwordAttempts counter and locks the account if we've hit the max
     *
     * @param {User} user
     * @returns {Promise<void>}
     */
    public async incrementFailedAttempt(user: User): Promise<void> {
        if (!user) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot increment the passwordAttempts on a null user');
        }
        user.passwordAttempts++;
        user.accountLocked = user.passwordAttempts > AuthUtils.ATTEMPT_LIMIT;

        if (user.accountLocked) {
            this.securityService.logWarning('A user\'s account has been locked', user);
            const emailToken = AuthUtils.generateSecureEmailToken();
            user.emailTokenDigest = AuthUtils.hashPassword(emailToken, user.salt);
            user.emailTokenExpiration = AuthUtils.getExpirationForLoginLink();
            this.emailService.sendAccountLockedWarningEmail(user, emailToken);
        }

        await this.userService.saveUser(user);
    }

    /**
     * Compares the password to the password digest on the user.  If successful, resets the relevant fields on the user
     * if not, it increments the failed attempt count
     *
     * @param {User} user
     * @param {string} emailToken
     * @returns {Promise<boolean>}
     */
    public async validateUserGivenEmailToken(user: User, emailToken: string): Promise<boolean> {
        if (!user || StringUtils.isBlank(emailToken)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot validate user given null user or blank email token');
        }

        if (!AuthUtils.isEmailTokenValid(emailToken, user.salt, user.emailTokenExpiration, user.emailTokenDigest)) {
            this.incrementFailedAttempt(user);
            return false;
        }

        user.emailVerified = true;
        user.passwordAttempts = 0;
        user.lastAttempt = new Date();
        await this.userService.saveUser(user);
        return true;
    }

    /**
     * Compares the emailToken to the email token digest on the user.  If successful, resets the relevant fields on the user
     * if not, it increments the failed attempt count
     *
     * @param {User} user
     * @param {string} password
     * @returns {Promise<boolean>}
     */
    public async validateUserGivenPassword(user: User, password: string): Promise<boolean> {
        if (!user || StringUtils.isBlank(password)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot validate user given null user or blank password');
        }

        if (!AuthUtils.passwordMatches(password, user.salt, user.passwordDigest)) {
            this.incrementFailedAttempt(user);
            return false;
        }

        user.passwordAttempts = 0;
        user.lastAttempt = new Date();
        await this.userService.saveUser(user);
        return true;
    }
}