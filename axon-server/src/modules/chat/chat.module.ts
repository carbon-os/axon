import { Module } from '@nestjs/common';
import { ChatGateway } from './chat.gateway';
import { DatabaseModule } from '../database/database.module';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Module({
    imports: [
        DatabaseModule,
    ],
    providers: [
        // ChatGateway,
        MessageService,
    ],
    controllers: [
        MessageController,
    ],
})
export class ChatModule {}