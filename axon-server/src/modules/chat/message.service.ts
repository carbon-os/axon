import { Injectable, Inject } from '@nestjs/common';
import { Q } from '../database/QueryUtils';
import { Message } from '../../model/entities/message.entity';
import { MessageDto } from '../../model/types/chat/MessageDto';
import { DATA_REPOSITORY, DataRepository } from '../database/data.repository';

@Injectable()
export class MessageService {

    constructor(@Inject(DATA_REPOSITORY) private readonly dataRepository: DataRepository) {
    }

    /**
     * Creates a new {Message}
     *
     *
     * @param {Message} message
     * @returns {Promise<void>}
     */
    public async createMessage(message: Message): Promise<Message> {
        console.log('saving new message: ' + message);
        return this.dataRepository.save(Message, message);
    }

    /**
     * Loads a {Message} from database by id
     *
     * @param {string} id
     * @returns {Promise<MessageDto>}
     */
    public async loadMessage(id: string): Promise<MessageDto> {
        const message = await this.dataRepository.findOneById(Message, id, Q.active<Message>());
        if (!message) {
            throw Error('404');
        }
        console.log('loaded message: ' + id);
        return MessageService.messageToMessageDto(message);
    }

    /**
     * Lists {Message} using PagingQuery
     *
     * @returns {Promise<MessageDto[]>}
     */
    public async listMessages(): Promise<MessageDto[]> {
        const message = await this.dataRepository.find(Message, Q.active<Message>());
        if (!message) {
            throw Error('404');
        }
        console.log('found ' + message.length + ' messages');
        return message.map(m => MessageService.messageToMessageDto(m)).sort((a, b) => a.timestamp - b.timestamp);
    }

    /**
     * Maps a {Message} to a {MessageDto}
     *
     * The resulting fields are the fields that are safe/meaningful to return to the end user
     *
     * @param {Message} msg
     * @returns {MessageDto}
     */
    public static messageToMessageDto(msg: Message): MessageDto {
        return new MessageDto({
            id: msg.uuid,
            timestamp: Date.parse(msg.createdDate as any),
            user: msg.user,
            channel: msg.channel,
            text: msg.text,
        });
    }
}