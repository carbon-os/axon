import { SubscribeMessage, WebSocketGateway, WebSocketServer, OnGatewayInit } from '@nestjs/websockets';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { ChatReducers } from './chat.reducers';
import { MessageService } from './message.service';
import { Message } from '../../model/entities/message.entity';
import { EntityStatus } from '../../model/types/common/EntityStatus';
import { NewMessageDto } from '../../model/types/chat/NewMessageDto';
import { AuthUtils } from '../auth/AuthUtils';

// @WebSocketGateway(3001, {namespace: 'ws/chat'})
export class ChatGateway implements OnGatewayInit {

    constructor(private messageService: MessageService) {
    }

    @WebSocketServer()
    private server;

    async afterInit(server) {
        console.log('initializing gateway...');
    }

    @SubscribeMessage('add-message')
    async onAddMessage(client, data: NewMessageDto): Promise<void> {
        if (!data.content) {
            return;
        }

        const jwtPayload = AuthUtils.verifyAndDecodeJwt(data.jwt);

        let messageText = data.content.text;

        console.log(messageText);

        messageText = ChatReducers.addPtoWordsThatStartWithS(messageText);
        messageText = ChatReducers.randomlyInsertUndertaker(messageText);
        messageText = ChatReducers.randomlyInsertRr(messageText);

        let message: Message = new Message({
            user: jwtPayload.email,
            createdBy: jwtPayload.uuid,
            lastModifiedBy: jwtPayload.uuid,
            channel: 'chat',
            text: messageText,
            entityStatus: EntityStatus.ACTIVE,
        });

        message = await this.messageService.createMessage(message);

        console.log(message);
        this.server.emit('message', MessageService.messageToMessageDto(message));
    }

    @SubscribeMessage('connection')
    onConnection(client): void {
        console.log('new connection from: ' + ChatGateway.resolveRemoteAddress(client));

        this.server.emit('connect', {
            user: ChatGateway.resolveRemoteAddress(client),
            message: 'connected',
        });
    }

    @SubscribeMessage('disconnect')
    onDisconnect(client, data): void {

        console.log(data);
        console.log(ChatGateway.resolveRemoteAddress(client) + ' disconnected');

        this.server.emit('disconnect', {
            user: ChatGateway.resolveRemoteAddress(client),
            message: 'disconnected',
        });
    }

    private static resolveRemoteAddress(client) {
        return client.request.connection.remoteAddress.replace('::ffff:', '');
    }
}