export class ChatReducers {

    public static addPtoWordsThatStartWithS(message: string) {
        if (!message) {
            return;
        }

        if (ChatReducers.getRandomInt(1, 50) === 4) {
            const wordsToReplace = message.split(/[ ,]+/).filter(w => w.length > 1 && w[ 0 ] === 's');

            for (const word of wordsToReplace) {
                message = message.replace(word, 'p' + word);
            }
        }

        return message;
    }

    public static randomlyInsertUndertaker(message: string) {
        if (!message) {
            return;
        }

        if (ChatReducers.getRandomInt(1, 1000) === 42) {
            message = 'that\'s what the undertaker told JOHN CENA AT SUNDAYS WWE SUPPPPERRRR SLAMMMMMMMMMM';
        }

        return message;
    }

    public static randomlyInsertRr(message: string) {
        if (!message) {
            return;
        }

        if (ChatReducers.getRandomInt(1, 1000) === 42) {
            message = 'https://bit.ly/2yM6JGj';
        }

        return message;
    }

    private static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}