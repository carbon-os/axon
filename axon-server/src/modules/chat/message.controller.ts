import { Controller, Get, HttpCode, HttpStatus, Param } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageDto } from '../../model/types/chat/MessageDto';

@Controller('message')
export class MessageController {
    constructor(private readonly messageService: MessageService) {
    }

    @Get(':id')
    @HttpCode(HttpStatus.OK)
    public async loadMessage(@Param('id') id: string): Promise<MessageDto> {
        return this.messageService.loadMessage(id);
    }

    @Get()
    @HttpCode(HttpStatus.OK)
    public async listMessages(): Promise<MessageDto[]> {
        console.log('listing messages');
        return this.messageService.listMessages();
    }
}