import { HttpException } from '@nestjs/common';

export class Exceptions {

    public static DEFAULT_EXCEPTION_MESSAGES = {
        400: 'Oops! We detected something wrong with your request!',
        401: 'Naw tho',
        403: 'You don\'t have the authorization to do that',
        404: 'Hmmm... we couldn\'t find that',
        default: 'Something went wrong',
    };

    /**
     * Default initializer for a new Exception.  If messages aren't passed in, it will use the default messages for that http status code.
     *
     * @param {number} httpStatus is the http status code which will be returned as part of the request
     * @param {string} logMessage is the message that we will write to the logs to give a debugger better insight. Include (nonsensitive) data!
     * @param {string} userMessage is the message that we will send back to the user
     * @returns {Error}
     */
    public static new(httpStatus: number, logMessage?: string, userMessage?: string): Error {
        const message = userMessage || Exceptions.DEFAULT_EXCEPTION_MESSAGES[ httpStatus ] || Exceptions.DEFAULT_EXCEPTION_MESSAGES.default;
        const log = logMessage || message;
        console.error(log);
        return Exceptions.newHttpException(message, httpStatus);
    }

    private static newHttpException(message: string, httpStatus: number): Error {
        console.error(httpStatus + '; user message: ' + message);
        return new HttpException(message, httpStatus);
    }
}