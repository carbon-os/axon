import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';
import { StringUtils } from '../../utils/StringUtils';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

    private static REFERER = 'referer';

    private static CENSORED_FIELDS = [
        'password',
        'emailToken',
    ];

    public resolve(...args: any[]): MiddlewareFunction {
        return (req, res, next) => {
            console.log('p================== New Request ====================q');
            console.log('');
            console.log('         ' + req.method);
            console.log('    to   ' + req.originalUrl);
            console.log('  from   ' + req.headers[ LoggerMiddleware.REFERER ]);
            console.log('');

            if (req.body) {
                console.log(LoggerMiddleware.censorBody(req.body));
            }

            console.log('');
            console.log('|=== End Metadata ==============');

            next();
        };
    }

    private static censorBody(body: any): any {
        // censor fields that need censoring
        const fields = LoggerMiddleware.CENSORED_FIELDS.filter(f => StringUtils.isNotBlank(body[ f ]));

        const censoredBody = {
            ...body,
        };

        for (const f of fields) {
            // console.log('censoring field: ' + f);
            censoredBody[ f ] = '<censored>';
        }

        return censoredBody;
    }
}