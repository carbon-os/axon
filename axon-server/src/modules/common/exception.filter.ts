import { Catch, ExceptionFilter, HttpException } from '@nestjs/common';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host) {
        const status = exception.getStatus();
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        console.log('p=========== Exception Response ========q');
        console.log(exception.getStatus() + ': ' + JSON.stringify(exception.message));

        response
            .status(status)
            .json({
                statusCode: status,
                message: exception.message,
            });
    }
}