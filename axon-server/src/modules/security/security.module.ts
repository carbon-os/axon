import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { SecurityService } from './security.service';

@Module({
    imports: [ DatabaseModule ],
    providers: [
        SecurityService,
    ], exports: [
        SecurityService,
    ],
})
export class SecurityModule {
}