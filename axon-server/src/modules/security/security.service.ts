import { Injectable, Inject } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { PSLAB_DB_TOKEN } from '../database/database.providers';
import { AttackWarning } from '../../model/entities/attack-warning.entity';
import { EntityStatus } from '../../model/types/common/EntityStatus';
import { DATA_REPOSITORY, DataRepository } from '../database/data.repository';

@Injectable()
export class SecurityService {

    constructor(@Inject(DATA_REPOSITORY) private readonly dataRepository: DataRepository) {
    }

    async logWarning(message: string, ...payload: any[]): Promise<AttackWarning> {
        console.log(message);
        const attackWarning = {
            source: new Error().stack,
            message,
            payload: JSON.stringify(payload),
            createdBy: 'self',
            lastModifiedBy: 'self',
            entityStatus: EntityStatus.ACTIVE,
        };
        return await this.dataRepository.save(AttackWarning, attackWarning);
    }
}