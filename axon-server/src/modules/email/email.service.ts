import { Injectable } from '@nestjs/common';
import { User } from '../../model/entities/user.entity';

import * as SES from 'aws-sdk/clients/ses';
import { AWSError } from 'aws-sdk';
import { environment } from '../../environments/environment';
import { SesConstants } from './ses.constants';
import * as ejs from 'ejs';
import { UserAuthenticationDto } from '../../model/types/application/UserAuthenticationDto';
import { StringUtils } from '../../utils/StringUtils';
import { Exceptions } from '../common/exceptions';

@Injectable()
export class EmailService {
    private static sesService: SES = new SES({
        apiVersion: 'latest',
        region: 'us-east-1',
        credentials: {
            accessKeyId: environment.aws_client_id,
            secretAccessKey: environment.aws_client_secret,
        },
    });

    private static readonly EMAILS_TYPES = {
        LOGIN_LINK: {
            template: '/templates/login-link.ejs',
            subject: 'Rowland Has Granted You Access to CarbonOS',
            type: 'login-link',
        },
        ACCOUNT_LOCKED: {
            template: '/templates/account-locked.ejs',
            subject: 'CarbonOS Locked',
            type: 'account-locked',
        },
    };

    constructor() {
    }

    public sendLoginLinkEmail(uad: UserAuthenticationDto): void {
        if (!uad || StringUtils.isBlank(uad.email) || StringUtils.isBlank(uad.emailToken)) {
            throw Exceptions.new(401, 'Will not send the email link without a token', 'Cannot Send Login Email');
        }
        const templateData = {
            baseUrl: environment.api_host + ':' + environment.external_webapp_port,
            email: uad.email,
            emailToken: uad.emailToken,
            redirectUrl: uad.redirectUrl,
        };

        EmailService.sendEmail(uad.email, EmailService.EMAILS_TYPES.LOGIN_LINK, templateData);
    }

    public sendAccountLockedWarningEmail(user: User, emailToken: string): void {
        const templateData = {
            baseUrl: environment.api_host + ':' + environment.external_webapp_port,
            email: 'dan.pk.bar@gmail.com', // TODO-axon-18
            emailToken,
        };

        EmailService.sendEmail(user.email, EmailService.EMAILS_TYPES.ACCOUNT_LOCKED, templateData);
    }

    private static sendEmail(destination: string, emailConfig: EmailConfig, templateData: any) {
        ejs.renderFile(__dirname + emailConfig.template, templateData, (err, renderedHtmlString) => {
            if (err) {
                throw Error('Could not render email template: ' + err);
            }

            const emailRequest = EmailService.buildSesEmail(
                destination,
                emailConfig.subject,
                renderedHtmlString,
                emailConfig.type,
            );

            EmailService.sesService.sendEmail(emailRequest, EmailService.onEmailSend);
        });
    }

    private static buildSesEmail(destination: string, subject: string, body: string, type: string): SES.Types.SendEmailRequest {
        return {
            Source: SesConstants.SENDER_EMAIL,
            Destination: {
                ToAddresses: [ destination ],
            },
            Message: {
                Subject: {Data: subject},
                Body: {
                    Html: {Data: body},
                },
            },
            ReplyToAddresses: [ SesConstants.REPLIES_EMAIL ],
            ReturnPath: SesConstants.BOUNCES_EMAIL,
            SourceArn: SesConstants.SENDER_ARN,
            ReturnPathArn: SesConstants.BOUNCES_ARN,
            Tags: [
                {Name: 'type', Value: type},
            ],
        };
    }

    private static onEmailSend(err: AWSError, data: SES.Types.SendEmailResponse) {
        if (err) {
            console.log('Error occurred while sending email: ' + err);
        } else {
            console.log('Email sent successfully! ' + JSON.stringify(data));
        }
    }
}

export interface EmailConfig {
    template: string;
    subject: string;
    type: string;
}