export class SesConstants {
    public static readonly SENDER_EMAIL = 'Rowland Graus <rowland@carbonos.io>';
    public static readonly SENDER_ARN = 'arn:aws:ses:us-east-1:014703791453:identity/rowland@carbonos.io';

    public static readonly BOUNCES_EMAIL = 'rowland+bounces@carbonos.io';
    public static readonly BOUNCES_ARN = 'arn:aws:ses:us-east-1:014703791453:identity/rowland+bounces@carbonos.io';

    public static readonly REPLIES_EMAIL = 'rowland+replies@carbonos.io';
}
