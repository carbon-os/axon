import { HttpModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { ChatModule } from './chat/chat.module';
import { LoggerMiddleware } from './common/logger.middleware';
import { DemoModule } from './demo/demo.module';

@Module({
    imports: [
        AuthModule,
        ChatModule,
        DemoModule,
    ],
})
export class ApplicationModule {
    configure(consumer: MiddlewareConsumer): void {
        consumer
            .apply([ LoggerMiddleware ])
            .with('ApplicationModule')
            .forRoutes('*');
    }
}