import { HttpModule, Module } from '@nestjs/common';
import { DemoController } from './demo.controller';
import { DatabaseModule } from '../database/database.module';
import { EmailModule } from '../email/email.module';
import { SecurityModule } from '../security/security.module';
import { DemoService } from './demo.service';

@Module({
    imports: [
        DatabaseModule,
        EmailModule,
        SecurityModule,
    ],
    providers: [
        DemoService,
    ],
    controllers: [
        DemoController,
    ],
})
export class DemoModule {}
