import { Inject, Injectable } from '@nestjs/common';
import { DATA_REPOSITORY, DataRepository } from '../database/data.repository';
import * as HttpStatus from 'http-status-codes';
import { StringUtils } from '../../utils/StringUtils';
import { Exceptions } from '../common/exceptions';
import { Guess } from '../../model/entities/guess.entity';
import { Q } from '../database/QueryUtils';
import { GuessStatDto } from '../../model/types/demo/GuessStatDto';
import { GuessDto } from '../../model/types/demo/GuessDto';
import { Prediction } from '../../model/types/predict/Prediction';
import '../rxjs-operators';
import * as request from 'request';
import * as stream from 'stream';
import { S3 } from 'aws-sdk';

@Injectable()
export class DemoService {

    constructor(
        @Inject(DATA_REPOSITORY) private readonly dataRepository: DataRepository,
    ) {
    }

    public async create(guessDto: GuessDto): Promise<GuessDto> {
        if (!guessDto || StringUtils.isBlank(guessDto.file_key) || guessDto.classification == null) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create new guessDto: ' + JSON.stringify(guessDto));
        }

        if (guessDto.id) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create accept new guess with uuid already set: ' + JSON.stringify(guessDto));
        }

        const guess = new Guess({
            entityStatus: 0,
            createdDate: Date.now(),
            createdBy: 'unknown-user',
            lastModifiedBy: 'unknown-user',
            lastModifiedDate: Date.now(),
            email: guessDto.email,
            file_key: guessDto.file_key,
            classification: guessDto.classification,
            carbonGuess: guessDto.carbonGuess,
            playerGuess: guessDto.playerGuess,
            toDelete: guessDto.toDelete,
        });

        const createdGuess = await this.dataRepository.save(Guess, guess);

        return {
            id: createdGuess.uuid,
            email: createdGuess.email,
            file_key: createdGuess.file_key,
            classification: createdGuess.classification,
            carbonGuess: createdGuess.carbonGuess,
            playerGuess: createdGuess.playerGuess,
            toDelete: createdGuess.toDelete,
        };
    }

    public async load(id: string): Promise<GuessDto> {
        if (StringUtils.isBlank(id)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Could not load guess with blank id');
        }

        const guess = await this.dataRepository.findOneById(Guess, id, Q.active<Guess>());
        if (!guess) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not load guess with id: ' + id);
        }
        return {
            id: guess.uuid,
            email: guess.email,
            file_key: guess.file_key,
            classification: guess.classification,
            carbonGuess: guess.carbonGuess,
            playerGuess: guess.playerGuess,
            toDelete: guess.toDelete,
        };
    }

    public async listHistoricalStats(): Promise<GuessStatDto> {
        const totalQuery = 'select count(*) from axon.guess;';
        const carbonQuery = 'select count(*) from axon.guess where carbonGuess = classification;';
        const playerQuery = 'select count(*) from axon.guess where playerGuess = classification;';

        const totalResult = await this.dataRepository.query<number>(totalQuery);
        const carbonResult = await this.dataRepository.query<number>(carbonQuery);
        const playerResult = await this.dataRepository.query<number>(playerQuery);

        const totalGuesses = totalResult && totalResult.length ? totalResult[ 0 ][ 'count(*)' ] : 1;
        const carbonCorrect = carbonResult && carbonResult.length ? carbonResult[ 0 ][ 'count(*)' ] : 1;
        const playerCorrect = playerResult && playerResult.length ? playerResult[ 0 ][ 'count(*)' ] : 1;

        const rhQuery = 'select file_key, COUNT(*) as count from axon.guess where playerGuess != classification and classification = 0 GROUP BY file_key ORDER BY count DESC;';
        const rrQuery = 'select file_key, COUNT(*) as count from axon.guess where carbonGuess != classification and classification = 0 GROUP BY file_key ORDER BY count DESC;';
        const hrQuery = 'select file_key, COUNT(*) as count from axon.guess where carbonGuess != classification and classification = 1 GROUP BY file_key ORDER BY count DESC;';
        const hhQuery = 'select file_key, COUNT(*) as count from axon.guess where playerGuess != classification and classification = 1 GROUP BY file_key ORDER BY count DESC;';

        const rhResult = await this.dataRepository.query<{ file_key: string, count: number }>(rhQuery);
        const rrResult = await this.dataRepository.query<{ file_key: string, count: number }>(rrQuery);
        const hrResult = await this.dataRepository.query<{ file_key: string, count: number }>(hrQuery);
        const hhResult = await this.dataRepository.query<{ file_key: string, count: number }>(hhQuery);

        return {
            humanBattingAverage: +(playerCorrect / totalGuesses).toFixed(3),
            carbonBattingAverage: +(carbonCorrect / totalGuesses).toFixed(3),
            rh: rhResult.sort((a, b) => b.count - a.count),
            rr: rrResult.sort((a, b) => b.count - a.count),
            hr: hrResult.sort((a, b) => b.count - a.count),
            hh: hhResult.sort((a, b) => b.count - a.count),
        };
    }

    public async predict(data: S3.Body): Promise<Prediction> {
        const url = 'http://localhost:5000/ai/predict';
        return await this.sendFile<Prediction>(url, data);
    }

    public async healthcheck(): Promise<void> {
        const query = 'show databases;';
        const tables = await this.dataRepository.query<any>(query);
        if (tables == null || !tables.length) {
            throw Exceptions.new(500, 'Could not find any tables in database');
        }

        const url = 'http://localhost:5000/healthcheck';
        const response = await this.get(url);
        if (!response || response.status != 200) {
            throw Exceptions.new(500, 'Got a bad response from flask healthcheck endpoint');
        }
    }

    private get(url: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            request.get(url, (err, res, body) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        });
    }

    private sendFile<T>(url: string, data: S3.Body): Promise<T> {
        return new Promise<T>((resolve, reject) => {

            const req = request.post(url, (err, res, body) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(body);
                }
            });

            const form = req.form();
            form.append('file', data, {
                filename: 'censored-name.wav',
                contentType: 'audio/wav',
            });
        });
    }

    private streamFile<T>(url: string, data: stream.Readable): Promise<T> {
        return new Promise<T>((resolve, reject) => {

            const req = request.post({url, formData: {file: data}}, (err, res, body) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(body);
                }
            });

            // const form = req.form();
            // form.append('file', data, {
            //     filename: 'censored-name.wav',
            //     contentType: 'audio/wav',
            // });
        });
    }
}