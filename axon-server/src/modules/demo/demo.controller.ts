import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Req, Res } from '@nestjs/common';
import { JsonWebTokenPayload } from '../../model/types/application/JsonWebTokenPayload';
import { FileService } from '../database/file.service';
import { DemoService } from './demo.service';
import { GuessStatDto } from '../../model/types/demo/GuessStatDto';
import { AudioFileDto } from '../../model/types/demo/AudioFileDto';
import { Exceptions } from '../common/exceptions';
import { GuessDto } from '../../model/types/demo/GuessDto';
import { Prediction } from '../../model/types/predict/Prediction';

@Controller('demo')
export class DemoController {
    constructor(private readonly fileService: FileService,
                private readonly demoService: DemoService) {}

    @Post('stream')
    @HttpCode(HttpStatus.OK)
    public async upload(@Req() request, @Res() response): Promise<JsonWebTokenPayload> {
        return this.fileService.upload(request, response);
    }

    @Get('stream/:id')
    @HttpCode(HttpStatus.OK)
    public async download(@Param('id') id: string, @Res() response) {
        const stream = await this.fileService.streamDownload(id);
        stream.pipe(response);
    }

    @Get('audio-file/:id')
    @HttpCode(HttpStatus.OK)
    public async loadAudioFile(@Param('id') id: string): Promise<AudioFileDto> {
        return this.fileService.loadWrapper(id);
    }

    @Get('random-audio-file/:n')
    @HttpCode(HttpStatus.OK)
    public async listRandomAudioFiles(@Param('n') n: string): Promise<AudioFileDto[]> {
        if (isNaN(parseInt(n, 10))) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'n must be an integer');
        }
        return this.fileService.listRandomFiles(parseInt(n, 10));
    }

    @Post('audio-file')
    @HttpCode(HttpStatus.OK)
    public async createAudioFile(@Body() audioFile: AudioFileDto): Promise<AudioFileDto> {
        return this.fileService.createWrapper(audioFile);
    }

    @Get('guess/:id')
    @HttpCode(HttpStatus.OK)
    public async loadGuess(@Param('id') id: string): Promise<GuessDto> {
        return this.demoService.load(id);
    }

    @Post('guess')
    @HttpCode(HttpStatus.OK)
    public async createGuess(@Body() guess: GuessDto): Promise<GuessDto> {
        return this.demoService.create(guess);
    }

    @Get('guess-stats')
    @HttpCode(HttpStatus.OK)
    public async listHistoricalStats(): Promise<GuessStatDto> {
        return this.demoService.listHistoricalStats();
    }

    @Get('predict/:file_key')
    @HttpCode(HttpStatus.OK)
    public async predict(@Param('file_key') file_key: string): Promise<Prediction> {
        const body = await this.fileService.download(file_key);
        return await this.demoService.predict(body);
    }

    @Get('healthcheck')
    @HttpCode(HttpStatus.OK)
    public async healtcheck() {
        return await this.demoService.healthcheck();
    }
}