import { Module } from '@nestjs/common';
import { DATA_REPOSITORY } from './data.repository';
import { MockDataRepository } from './mock-data.repository';

@Module({
    providers: [
        {
            provide: DATA_REPOSITORY,
            useClass: MockDataRepository,
        },
    ],
    exports: [
        {
            provide: DATA_REPOSITORY,
            useClass: MockDataRepository,
        },
    ],
})
export class MockDatabaseModule {
}