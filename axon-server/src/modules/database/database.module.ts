import { Module } from '@nestjs/common';
import { databaseProviders } from './database.providers';
import { DATA_REPOSITORY } from './data.repository';
import { MysqlDataRepository } from './mysql-data.repository';
import { FileService } from './file.service';

@Module({
    providers: [
        {
            provide: DATA_REPOSITORY,
            useClass: MysqlDataRepository,
        },
        ...databaseProviders,
        FileService,
    ],
    exports: [
        {
            provide: DATA_REPOSITORY,
            useClass: MysqlDataRepository,
        },
        FileService,
    ],
})
export class DatabaseModule {
}
