import { DeepPartial, FindManyOptions, FindOneOptions, ObjectType, SaveOptions } from 'typeorm';

export const DATA_REPOSITORY = 'DATA_REPOSITORY';

export interface DataRepository {

    query<T>(query: string, parameters?: any[]): Promise<T[]>;

    save<Entity, T extends DeepPartial<Entity>>(targetOrEntity: ObjectType<Entity> | string, entity: T, options?: SaveOptions): Promise<T>;

    findOneById<Entity>(entityClass: ObjectType<Entity> | string, id: any, options?: FindOneOptions<Entity>): Promise<Entity | undefined>;

    findOne<Entity>(entityClass: ObjectType<Entity> | string, options?: FindOneOptions<Entity>): Promise<Entity | undefined>;

    find<Entity>(entityClass: ObjectType<Entity> | string, options?: FindManyOptions<Entity>): Promise<Entity[]>;
}