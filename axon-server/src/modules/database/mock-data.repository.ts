import { Injectable } from '@nestjs/common';
import { DeepPartial, FindManyOptions, FindOneOptions, ObjectType, SaveOptions } from 'typeorm';
import { DataRepository } from './data.repository';

// TODO-axon-14 = make these methods behave with more fidelity
@Injectable()
export class MockDataRepository implements DataRepository {

    constructor() {
    }

    public async query<T>(query: string, parameters?: any[]): Promise<T[]> {
        return null;
    }

    public async save<Entity, T extends DeepPartial<Entity>>(targetOrEntity: ObjectType<Entity> | string, entity: T, options?: SaveOptions): Promise<T> {
        return null;
    }

    public async findOneById<Entity>(entityClass: ObjectType<Entity> | string, id: any, options?: FindOneOptions<Entity>): Promise<Entity | undefined> {
        return null;
    }

    public async findOne<Entity>(entityClass: ObjectType<Entity> | string, options?: FindOneOptions<Entity>): Promise<Entity | undefined> {
        return null;
    }

    public async find<Entity>(entityClass: ObjectType<Entity> | string, options?: FindManyOptions<Entity>): Promise<Entity[]> {
        return null;
    }
}