import { Injectable, Inject } from '@nestjs/common';
import { EntityManager, DeepPartial, ObjectType, SaveOptions, FindOneOptions, FindManyOptions } from 'typeorm';
import { PSLAB_DB_TOKEN } from './database.providers';
import { DataRepository } from './data.repository';

@Injectable()
export class MysqlDataRepository implements DataRepository {

    constructor(@Inject(PSLAB_DB_TOKEN) private readonly axonDbEntityManager: EntityManager) {
    }

    public async query<T>(query: string, parameters?: any[]): Promise<T[]> {
        return this.axonDbEntityManager.query(query, parameters) as Promise<T[]>;
    }

    public async save<Entity, T extends DeepPartial<Entity>>(targetOrEntity: ObjectType<Entity> | string, entity: T, options?: SaveOptions): Promise<T> {
        return this.axonDbEntityManager.save(targetOrEntity, entity, options);
    }

    public async findOneById<Entity>(entityClass: ObjectType<Entity> | string, id: any, options?: FindOneOptions<Entity>): Promise<Entity | undefined> {
        return this.axonDbEntityManager.findOneById(entityClass, id, options);
    }

    public async findOne<Entity>(entityClass: ObjectType<Entity> | string, options?: FindOneOptions<Entity>): Promise<Entity | undefined> {
        return this.axonDbEntityManager.findOne(entityClass, options);
    }

    public async find<Entity>(entityClass: ObjectType<Entity> | string, options?: FindManyOptions<Entity>): Promise<Entity[]> {
        return this.axonDbEntityManager.find(entityClass, options);
    }
}
