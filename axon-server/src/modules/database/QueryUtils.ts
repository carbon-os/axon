import { FindOneOptions } from 'typeorm';
import { EntityStatus } from '../../model/types/common/EntityStatus';
import { EntityBase } from '../../model/entities/base/entity-base.entity';

export class Q {

    public static active<T extends EntityBase>(query?: FindOneOptions<T>): FindOneOptions<T> {
        const newQuery = query || {};
        newQuery.where = newQuery.where || {};
        return Object.assign(newQuery.where, {entityStatus: EntityStatus.ACTIVE});
    }

}