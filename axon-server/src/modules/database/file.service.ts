import { Inject, Injectable, Req, Res } from '@nestjs/common';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';
import { environment } from '../../environments/environment';
import { DATA_REPOSITORY, DataRepository } from './data.repository';
import * as HttpStatus from 'http-status-codes';
import { StringUtils } from '../../utils/StringUtils';
import { Q } from './QueryUtils';
import { Exceptions } from '../common/exceptions';
import { AudioFile } from '../../model/entities/audio-file.entity';
import { AudioFileDto } from '../../model/types/demo/AudioFileDto';
import S3 = require('aws-sdk/clients/s3');
import * as stream from 'stream';

@Injectable()
export class FileService {
    private static AWS_S3_BUCKET_NAME = environment.aws_s3_bucket;
    private s3Service: S3 = new S3({
        apiVersion: 'latest',
        region: 'us-east-1',
        credentials: {
            accessKeyId: environment.aws_client_id,
            secretAccessKey: environment.aws_client_secret,
        },
    });

    constructor(@Inject(DATA_REPOSITORY) private readonly dataRepository: DataRepository) {}

    public async upload(@Req() req, @Res() res) {
        try {
            this.uploader(req, res, (error) => {
                if (error) {
                    console.log(error);
                    return res.status(404).json(`Failed to upload file: ${error}`);
                }
                return res.status(201).json(req.files[ 0 ].location);
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(`Failed to upload file: ${error}`);
        }
    }

    public streamDownload(id: string): stream.Readable {
        const params = {
            Bucket: FileService.AWS_S3_BUCKET_NAME,
            Key: id,
        };

        return this.s3Service.getObject(params).createReadStream();
    }

    public async download(id: string): Promise<S3.Body> {
        const params = {
            Bucket: FileService.AWS_S3_BUCKET_NAME,
            Key: id,
        };

        const response = await this.s3Service.getObject(params).promise();

        return response.Body;
    }

    public async createWrapper(fileDto: AudioFileDto): Promise<AudioFileDto> {
        if (!fileDto || StringUtils.isBlank(fileDto.file_key) || fileDto.classification == null) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create new file: ' + JSON.stringify(fileDto));
        }

        if (fileDto.id) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Cannot create accept new guess with uuid already set: ' + JSON.stringify(fileDto));
        }

        const file = new AudioFile({
            entityStatus: 0,
            createdDate: Date.now(),
            createdBy: 'system-user',
            lastModifiedBy: 'system-user',
            lastModifiedDate: Date.now(),
            file_key: fileDto.file_key,
            classification: fileDto.classification,
            fileName: fileDto.fileName,
            mimeType: fileDto.mimeType,
            size: fileDto.size,
            description: fileDto.description,
        });

        const createdFile = await this.dataRepository.save(AudioFile, file);

        return {
            id: createdFile.uuid,
            file_key: createdFile.file_key,
            classification: createdFile.classification,
            fileName: createdFile.fileName,
            mimeType: createdFile.mimeType,
            size: createdFile.size,
            description: createdFile.description,
        };
    }

    public async loadWrapper(id: string): Promise<AudioFileDto> {
        if (StringUtils.isBlank(id)) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Could not load guess with blank id');
        }

        const file = await this.dataRepository.findOneById(AudioFile, id, Q.active<AudioFile>());
        if (!file) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not load file with id: ' + id);
        }

        return {
            id: file.uuid,
            file_key: file.file_key,
            classification: file.classification,
            fileName: file.fileName,
            mimeType: file.mimeType,
            size: file.size,
            description: file.description,
        };
    }

    public async listRandomFiles(n: number): Promise<AudioFileDto[]> {
        if (!n) {
            throw Exceptions.new(HttpStatus.BAD_REQUEST, 'Could not load random files with a null parameter');
        }

        const allFiles = await this.dataRepository.find(AudioFile);
        if (!allFiles) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Could not find any audio files.');
        }

        if (n > allFiles.length) {
            throw Exceptions.new(HttpStatus.NOT_FOUND, 'Asked for more files (' + n + ') than we have (' + length + ')');
        }

        let robotFiles = allFiles.filter(f => f.classification === 0);
        let humanFiles = allFiles.filter(f => f.classification === 1);

        console.log('picking ' + Math.ceil(n / 2) + ' robots');
        console.log('picking ' + Math.floor(n / 2) + ' humans');

        const robotIndices = FileService.randomUniqueIndices(Math.ceil(n / 2), robotFiles.length); // round up
        const humanIndices = FileService.randomUniqueIndices(Math.floor(n / 2), humanFiles.length); // round down


        console.log('picking robots: ' + robotIndices);
        console.log('picking humans: ' + humanIndices);

        const selectedRobotFiles = robotIndices.map(i => robotFiles[ i ]);
        const selectedFiles = humanIndices.map(i => humanFiles[ i ]);
        selectedFiles.push(...selectedRobotFiles);

        console.log(selectedRobotFiles);
        console.log(selectedFiles);

        FileService.shuffleArray(selectedFiles);

        return selectedFiles.map(s => {
            return {
                id: s.uuid,
                file_key: s.file_key,
                classification: s.classification,
                fileName: s.fileName,
                mimeType: s.mimeType,
                size: s.size,
                description: s.description,
            };
        });
    }

    private static randomUniqueIndices(n, length) {
        const selectedIndices: number[] = [];
        while (selectedIndices.length < n) {
            const index = FileService.getRandomInt(0, length - 1);
            if (selectedIndices.indexOf(index) < 0) {
                selectedIndices.push(index);
            }
        }
        return selectedIndices;
    }

    private static getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private static shuffleArray(a: any[]) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [ a[ i ], a[ j ] ] = [ a[ j ], a[ i ] ];
        }
        return a;
    }

    private uploader = multer({
        storage: multerS3({
            s3: this.s3Service,
            bucket: FileService.AWS_S3_BUCKET_NAME,
            // acl: 'public-read',
            key: (request, file, callback) => {
                callback(null, `${Date.now().toString()} - ${file.originalname}`);
            },
        }),
    }).array('upload', 1);
}