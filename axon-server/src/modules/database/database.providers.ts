import { Connection, createConnection, getManager } from 'typeorm';

export const DB_CONNECTION_TOKEN = 'DB_CONNECTION_TOKEN';
export const PSLAB_DB_TOKEN = 'PSLAB_DB_TOKEN';

export const databaseProviders = [
    {
        provide: DB_CONNECTION_TOKEN,
        useFactory: async () => await createConnection({
            type: 'mysql',
            // url: '',
            host: 'localhost',
            port: 3306,
            username: 'root',
            password: 'root',
            database: 'axon',
            synchronize: true,
            debug: false,
            logging: false,
            trace: true,
            charset: 'UTF8_GENERAL_CI',
            timezone: 'local',
            connectTimeout: 10000,
            insecureAuth: false,
            supportBigNumbers: true,
            bigNumberStrings: false,
            dateStrings: false,
            multipleStatements: false,
            flags: null, // https://github.com/mysqljs/mysql#connection-flags
            ssl: null, // https://github.com/mysqljs/mysql#ssl-options
            entities: [
                '**/model/entities/**.entity.ts',
            ],
        }),
    },
    {
        provide: PSLAB_DB_TOKEN,
        useFactory: (connection: Connection) => getManager(),
        inject: [ DB_CONNECTION_TOKEN ],
    },
];
