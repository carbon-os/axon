#!/usr/bin/env bash

ENV=$1
if [ -z "$ENV" ]; then
  echo "Specify the target server alias"
  echo "ie: ./copy_env_file.sh axon-stage"
  exit 1
fi

mv /carbon-os/target/axon/axon-server/src/environments/environment.$ENV.ts /carbon-os/target/axon/axon-server/src/environments/environment.ts