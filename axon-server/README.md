# Angular UI

Quickstart:
```bash
yarn install
```

Develop:
```bash
yarn dev:watch
```

Test:
```bash
yarn lint
yarn test
yarn e2e
```

Build:
```bash
yarn build
```

To start in production:
```bash
yarn start
```