#!/usr/bin/env bash

set -e

rm -f /carbon-os/workspace/axon/axon-server/dist/environments/environment.js

cp /carbon-os/workspace/axon/axon-server/dist/environments/environment.prod.js /carbon-os/workspace/axon/axon-server/dist/environments/environment.js
