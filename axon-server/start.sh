#!/usr/bin/env bash

forever start \
    --pidFile /carbon-os/target/axon/axon-server/axon-server.pid \
    -a \
    -l /carbon-os/logs/axon/axon-server/forever.log \
    -o /carbon-os/logs/all.log \
    -e /carbon-os/logs/all.log \
    index.js




#    to use dist:
#forever start \
#    --pidFile /carbon-os/target/axon/axon-server/axon-server.pid \
#    -a \
#    -l /carbon-os/logs/axon/axon-server/forever.log \
#    -o /carbon-os/logs/axon/axon-server/out.log \
#    -e /carbon-os/logs/axon/axon-server/out.log \
#    dist/server.js
#
