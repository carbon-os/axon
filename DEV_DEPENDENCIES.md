
# Developer Machine Dependencies
*assuming Mac OS X*

Make workspace directory:
```
sudo mkdir /carbon-os
sudo chown $(whoami):wheel /carbon-os
cd /carbon-os
mkdir workspace
```

Make sure you have dependencies
* node v9.2.0
* angular CLI v1.7.3
* git (anything above 2)

to check:
```bash
node -v
yarn --version
ng -v
```

##### Node
If you're missing these or have the wrong versions, install them:

Check if you have nvm (`nvm --version`) and install it for easy node management if you don't:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
. ~/.nvm/nvm.sh
```

Once you have it, use nvm to install node (stable=9.2.0 right now):
```bash
nvm install stable
```

##### Yarn

```bash
brew install yarn --without-node
```

##### Angular CLI
```bash
yarn global add @angular/cli@1.7.3
```