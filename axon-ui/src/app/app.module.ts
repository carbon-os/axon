import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { MatModule } from './mat/mat.module';
import { AppRoutes } from './app.routes';
import { NavigationLibModule } from './navigation/navigation-lib.module';
import { UserLibModule } from './user/user-lib.module';
import { AuthLibModule } from './auth/auth-lib.module';
import { CookieModule } from 'ngx-cookie';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { ChatLibModule } from './chat/chat-lib.module';
import { DemoLibModule } from './demo/demo-lib.module';
import { VoiceLibModule } from './voice/voice-lib.module';
import { LensLibModule } from './lens/lens-lib.module';

const APP_MODULES = [
  MatModule,
  UserLibModule,
  NavigationLibModule,
  AuthLibModule,
  // ChatLibModule,
  DemoLibModule,
  VoiceLibModule,
  LensLibModule,
  CoreModule,
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
    CookieModule.forRoot(),
    ...APP_MODULES
  ],
  providers: [
  ],
  bootstrap: [ AppComponent ],
})
export class AppModule {
}
