import { Component, OnInit } from '@angular/core';
import { USER_SECURITY_ROUTE } from '../user.routes';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: [ './user-profile.component.scss' ],
})
export class UserProfileComponent implements OnInit {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  public routeToUserSecurity() {
    this.router.navigate([ '..', USER_SECURITY_ROUTE ], {relativeTo: this.activatedRoute});
  }
}
