import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HOME_PAGE_ROUTE, USER_MODULE_ROUTE } from '../../app.routes';
import { USER_SECURITY_ROUTE } from '../user.routes';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: [ './welcome.component.scss' ],
})
export class WelcomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  public routeToSecuritySettings() {
    this.router.navigate([ USER_MODULE_ROUTE, USER_SECURITY_ROUTE ]);
  }

  public routeToHome() {
    this.router.navigate([ HOME_PAGE_ROUTE ]);
  }

}
