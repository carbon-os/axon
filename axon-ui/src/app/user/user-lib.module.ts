import { NgModule } from '@angular/core';
import { MatModule } from '../mat/mat.module';
import { RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserSecurityFormComponent } from './user-security-form/user-security-form.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserService } from './service/user.service';
import { UserSecurityProfileComponent } from './user-security-profile/user-security-profile.component';

@NgModule({
  imports: [
    RouterModule,
    MatModule,
  ],
  declarations: [
    WelcomeComponent,
    UserFormComponent,
    UserSecurityFormComponent,
    UserSecurityProfileComponent,
    UserProfileComponent,
  ],
  exports: [],
  providers: [
    UserService,
  ],
})
export class UserLibModule {
}
