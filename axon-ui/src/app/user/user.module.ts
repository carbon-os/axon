import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user-routing.module';
import { UserLibModule } from './user-lib.module';

@NgModule({
  imports: [
    UserLibModule,
    UserRoutingModule,
  ],
  exports: [
    UserLibModule,
    UserRoutingModule,
  ],
})
export class UserModule {
}
