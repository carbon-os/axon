import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { UserDto } from '../../../model/types/application/UserDto';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: [ './user-form.component.scss' ],
})
export class UserFormComponent implements OnInit {
  public userForm: FormGroup;
  private currentUser: UserDto;
  public error: string;
  public displaySuccess = false;

  constructor(private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.initializeFormGroup();
    this.loadCurrentUser();
  }

  public initializeFormGroup() {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [ Validators.required ]),
      lastName: new FormControl('', []),
    });
  }

  public submit() {
    this.currentUser.firstName = this.userForm.get('firstName').value;
    this.currentUser.lastName = this.userForm.get('lastName').value;
    this.userService
      .update(this.currentUser)
      .subscribe(
        user => {
          this.currentUser = user;
          this.flashSuccessMessage();
        },
        err => this.error = err.error,
      );
  }

  private loadCurrentUser() {
    this.userService
      .getCurrentUser()
      .subscribe(
        user => {
          this.currentUser = user;
          this.setValues(this.currentUser);
        },
        err => this.error = err.error,
      );
  }

  private flashSuccessMessage() {
    this.displaySuccess = true;
    setTimeout(() => {
      this.displaySuccess = false;
    }, 3000);
  }

  private setValues(user: UserDto) {
    if (!user) {
      return;
    }
    this.userForm.setValue({
      firstName: user.firstName,
      lastName: user.lastName,
    });
  }
}
