import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatModule } from '../../mat/mat.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';
import { UserSecurityFormComponent } from './user-security-form.component';

describe('UserSecurityFormComponent', () => {
  let component: UserSecurityFormComponent;
  let fixture: ComponentFixture<UserSecurityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSecurityFormComponent ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        RouterTestingModule,
        HttpClientModule, HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        AuthService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSecurityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
