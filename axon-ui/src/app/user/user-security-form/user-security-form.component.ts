import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';

@Component({
  selector: 'app-user-security-form',
  templateUrl: './user-security-form.component.html',
  styleUrls: [ './user-security-form.component.scss' ],
})
export class UserSecurityFormComponent implements OnInit {
  public passwordResetForm: FormGroup;
  public alreadyHasPassword: boolean;
  public hidePassword = true;
  public error = null;
  public displaySuccess = false;

  get passwordErrorMessage() {
    return this.passwordResetForm.get('newPasswordControl').hasError('minlength') ? 'Must be at least 8 characters' : '';
  }

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.initializeFormGroup();
    const jwt = this.authService.getJwtPayload();
    this.alreadyHasPassword = jwt && jwt.hasPassword;
  }

  public initializeFormGroup() {
    this.passwordResetForm = new FormGroup({
      oldPasswordControl: new FormControl('', []),
      newPasswordControl: new FormControl('', [ Validators.required, Validators.minLength(8) ]),
    });
  }

  public submit() {
    this.error = null;
    const jwt = this.authService.getJwtPayload();
    const uad: UserAuthenticationDto = {
      email: jwt != null ? jwt.email : null,
      password: this.passwordResetForm.get('newPasswordControl').value,
      oldPassword: this.passwordResetForm.get('oldPasswordControl').value,
    };
    this.authService
      .resetPassword(uad)
      .subscribe(
        _ => this.onSuccess(),
        err => this.error = err.error,
      );
  }

  private onSuccess() {
    this.authService.setPasswordUpdate();
    this.displaySuccess = true;
    setTimeout(() => {
      this.displaySuccess = false;
      this.alreadyHasPassword = true;
      this.passwordResetForm.reset();
    }, 5000);
  }
}
