import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserSecurityProfileComponent } from './user-security-profile/user-security-profile.component';
import { USER_PROFILE_ROUTE, USER_SECURITY_ROUTE, WELCOME_ROUTE } from './user.routes';
import { AuthGuard } from '../auth/service/auth.guard';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

/**
 * Default route: Welcome Page
 *
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: WELCOME_ROUTE,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: WELCOME_ROUTE,
    component: WelcomeComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: USER_PROFILE_ROUTE,
    component: UserProfileComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: USER_SECURITY_ROUTE,
    component: UserSecurityProfileComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class UserRoutingModule {}
