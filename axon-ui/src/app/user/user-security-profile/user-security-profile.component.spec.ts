import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSecurityProfileComponent } from './user-security-profile.component';
import { MatModule } from '../../mat/mat.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserSecurityFormComponent } from '../user-security-form/user-security-form.component';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';

describe('UserSecurityProfileComponent', () => {
  let component: UserSecurityProfileComponent;
  let fixture: ComponentFixture<UserSecurityProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserSecurityProfileComponent,
        UserSecurityFormComponent,
      ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        AuthService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSecurityProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
