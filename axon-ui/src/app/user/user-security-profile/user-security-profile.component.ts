import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { USER_PROFILE_ROUTE } from '../user.routes';

@Component({
  selector: 'app-user-security-profile',
  templateUrl: './user-security-profile.component.html',
  styleUrls: [ './user-security-profile.component.scss' ],
})
export class UserSecurityProfileComponent implements OnInit {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  public routeToUserProfile() {
    this.router.navigate([ '..', USER_PROFILE_ROUTE ], {relativeTo: this.activatedRoute});
  }

}
