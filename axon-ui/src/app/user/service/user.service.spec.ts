import { inject, TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from '../../auth/service/auth.service';
import { CookieModule } from 'ngx-cookie';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserService,
        AuthService,
      ],
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
    });
  });

  it('should be created', inject([ UserService ], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
