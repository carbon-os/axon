import { Injectable } from '@angular/core';

import '../../rxjs-operators';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { UserDto } from '../../../model/types/application/UserDto';
import { AuthService } from '../../auth/service/auth.service';

@Injectable()
export class UserService {

  private static USER_CACHE_EXPIRATION = 1000 * 60 * 60;

  private static USER_URLS = {
    USER: '/api/v1.0/user',
  };

  constructor(private httpClient: HttpClient,
              private authService: AuthService) {
  }

  public update(user: UserDto): Observable<UserDto> {
    const url = UserService.USER_URLS.USER + '/' + user.id;
    return this.httpClient.put<UserDto>(url, user);
  }

  public getCurrentUser(): Observable<UserDto> {
    const jwt = this.authService.getJwtPayload();
    if (!jwt || !jwt.uuid) {
      return Observable.of(null);
    }

    const url = UserService.USER_URLS.USER + '/' + jwt.uuid;
    return this.httpClient.get<UserDto>(url);
  }
}
