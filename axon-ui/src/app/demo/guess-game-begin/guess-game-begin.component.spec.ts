import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessGameBeginComponent } from './guess-game-begin.component';

describe('GuessGameBeginComponent', () => {
  let component: GuessGameBeginComponent;
  let fixture: ComponentFixture<GuessGameBeginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessGameBeginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessGameBeginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
