import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-guess-game-begin',
  templateUrl: './guess-game-begin.component.html',
  styleUrls: [ './guess-game-begin.component.scss' ],
})
export class GuessGameBeginComponent implements OnInit {

  @Output() begin = new EventEmitter<void>();

  @Input() ready = false;

  constructor() { }

  ngOnInit() {
  }

  public sendBegin() {
    this.begin.emit();
  }

}
