import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'predictionText',
})
export class PredictionTextPipe implements PipeTransform {

  transform(prediction: number[], args?: any): any {
    if (!prediction || prediction.length < 2) {
      return 'No prediction made';
    }
    if (prediction[ 0 ] > .6000) {
      return this.formatConfidence(prediction[0]) + ' Human';
    } else if (prediction[ 1 ] > .6000) {
      return this.formatConfidence(prediction[1]) + ' Machine';
    } else {
      return this.formatConfidence(prediction[0]) + ' Human; ' + this.formatConfidence(prediction[1]) + ' Machine... ¯\\_(ツ)_/¯';
    }
  }

  private formatConfidence(prediction: number): string {
    return (prediction * 100).toFixed(2) + '%';
  }
}
