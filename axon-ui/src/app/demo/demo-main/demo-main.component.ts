import { Component, OnInit } from '@angular/core';
import { Prediction } from './prediction';
import { FileUploadService } from '../../core/file-upload.service';

@Component({
  selector: 'app-demo-main',
  templateUrl: './demo-main.component.html',
  styleUrls: [ './demo-main.component.scss' ],
})
export class DemoMainComponent implements OnInit {
  private static PREDICT_URL = '/ai/predict';
  private static UPLOAD_URL = '/ai/file';

  public allowedFileTypes = [ '.wav' ];
  public predictions: Prediction[] = [];

  private currentAudio: HTMLAudioElement;

  public get audioPlaying(): boolean {
    return this.currentAudio && this.currentAudio.duration > 0 && !this.currentAudio.paused;
  }

  constructor(
    private fileUploadService: FileUploadService,
  ) { }

  ngOnInit() {
  }

  public clearAll() {
    this.predictions = [];
  }

  public addToFiles(fileList: FileList) {
    const path = fileList.item(0).webkitRelativePath;
    console.log(path);

    for (let i = 0; i < fileList.length; i++) {

      this.fileUploadService.postFile(DemoMainComponent.UPLOAD_URL, fileList.item(i)).subscribe(_ => {});

      if (this.predictions.findIndex(p => p.description === fileList.item(i).name) > -1) {
        continue;
      }
      this.predictions.push({
        file: fileList.item(i),
        prediction: [],
        description: fileList.item(i).name,
        loading: false,
        complete: false,
        error: null,
        isPlaying: false,
      });
    }
  }

  public runPrediction(prediction: Prediction) {
    const newPred = this.predictions.find(p => p.description === prediction.description);
    newPred.loading = true;
    this.updatePredictions(newPred);

    return this.fileUploadService
      .postFile(DemoMainComponent.PREDICT_URL, prediction.file)
      .subscribe(
        (res: Prediction) => {
          const pred = this.mergeResponse(res, prediction);
          this.updatePredictions(pred);
        },
        (err) => {
          const pred = this.mergeError(err.message, prediction);
          this.updatePredictions(pred);
        },
      );
  }

  public playAudio(filename: string) {
    const newPrediction = this.predictions.find(p => p.description === filename);
    newPrediction.isPlaying = true;
    this.updatePredictions(newPrediction);

    const src = '/ai/file/' + filename;

    this.currentAudio = new Audio();
    this.currentAudio.src = src;
    this.currentAudio.load();
    this.currentAudio.play();

    this.currentAudio.addEventListener('ended', () => {
      const playingPred = this.predictions.find(p => p.description === filename);
      playingPred.isPlaying = false;
      this.updatePredictions(playingPred);
    });
  }

  private updatePredictions(newPrediction: Prediction) {
    const index = this.predictions.findIndex(p => p.description === newPrediction.description);

    if (index < 0) {
      throw new Error('Cannot find prediction description in existing list');
    }

    this.predictions.splice(index, 1, newPrediction);
    this.predictions = [
      ...this.predictions,
    ];
  }

  private mergeError(errMessage: string, prediction: Prediction) {
    return {
      file: prediction.file,
      prediction: prediction.prediction,
      description: prediction.description,
      loading: false,
      complete: true,
      error: errMessage,
      isPlaying: prediction.isPlaying,
    };
  }

  private mergeResponse(res: any, prediction: Prediction) {
    return {
      file: prediction.file,
      prediction: res.prediction,
      description: prediction.description,
      loading: false,
      complete: true,
      error: res.error,
      isPlaying: prediction.isPlaying,
    };
  }

  private removeFile(prediction: Prediction) {
    const index = this.predictions.findIndex(p => p.description === prediction.description);
    this.predictions.splice(index, 1);
    this.predictions = [
      ...this.predictions,
    ];
  }
}
