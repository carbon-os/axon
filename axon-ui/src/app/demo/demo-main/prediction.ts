export interface Prediction {
  file: File;
  prediction: number[];
  description: string;
  loading: boolean;
  complete: boolean;
  error: string;
  isPlaying: boolean;
}
