import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoMainComponent } from './demo-main.component';
import { MatModule } from '../../mat/mat.module';
import { SharedModule } from '../../shared/shared.module';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CookieModule } from 'ngx-cookie';
import { HttpClientModule } from '@angular/common/http';
import { PredictionTextPipe } from './prediction-text.pipe';
import { PredictionIconPipe } from './prediction-icon.pipe';
import { FileUploadService } from '../../core/file-upload.service';

describe('DemoMainComponent', () => {
  let component: DemoMainComponent;
  let fixture: ComponentFixture<DemoMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DemoMainComponent,
        PredictionIconPipe,
        PredictionTextPipe,
      ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        SharedModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        PredictionTextPipe,
        PredictionIconPipe,
        FileUploadService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
