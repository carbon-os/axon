import { Pipe, PipeTransform } from '@angular/core';
import { Prediction } from './prediction';

@Pipe({
  name: 'predictionIcon',
})
export class PredictionIconPipe implements PipeTransform {

  transform(prediction: Prediction, args?: any): string {
    if (prediction.loading) {
      return '';
    }

    if (!prediction.complete) {
      return 'chevron_right';
    }

    if (prediction.error) {
      return 'error';
    }

    return this.getIconForPrediction(prediction.prediction);
  }

  private getIconForPrediction(prediction: number[]): string {
    if (!prediction) {
      return 'help_outline';
    }

    if (prediction[ 0 ] > .6000) {
      return 'person';
    } else if (prediction[ 1 ] > .6000) {
      return 'android';
    } else {
      return 'help_outline';
    }
  }
}
