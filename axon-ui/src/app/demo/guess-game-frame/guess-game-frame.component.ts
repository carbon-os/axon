import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GameFrame } from '../../../model/types/demo/GameFrame';
import { DemoService } from '../service/demo.service';
import { PredictionService } from '../../core/prediction.service';

@Component({
  selector: 'app-guess-game-frame',
  templateUrl: './guess-game-frame.component.html',
  styleUrls: [ './guess-game-frame.component.scss' ],
})
export class GuessGameFrameComponent implements OnInit {

  @Input() frames: GameFrame[];
  @Input() index: number;

  @Output() next = new EventEmitter<void>();
  @Output() previous = new EventEmitter<void>();

  private currentAudio: HTMLAudioElement;

  get frame() {
    return this.frames[ this.index ];
  }

  get isAudioPlaying() {
    return this.currentAudio && !this.currentAudio.paused;
  }

  get playerGuessed() {
    return this.frame.playerGuess !== null && this.frame.playerGuess !== undefined;
  }

  get carbonGuessed() {
    return this.frame.carbonGuess !== null && this.frame.carbonGuess !== undefined;
  }

  get showNextButton() {
    if (this.playerGuessed && this.carbonGuessed && !this.isAudioPlaying) {
      return true;
    }
  }

  constructor(
    private demoService: DemoService,
    private predictionService: PredictionService,
  ) { }

  ngOnInit() {
    this.fireNextFrame();
  }

  playAudio() {
    const src = '/api/v1.0/demo/stream/' + this.frame.file.file_key;

    this.currentAudio = new Audio();
    this.currentAudio.src = src;
    this.currentAudio.load();
    this.currentAudio.play();

    this.currentAudio.addEventListener('ended', () => {
      // do nothing
    });
  }

  playerGuess(guess: number) {
    this.frame.playerGuess = guess;
  }

  markForDeletion() {
    console.log('delete');
    this.frame.toDelete = true;
  }

  private fireNextFrame(): void {
    setTimeout(() => {
      if (!this.frame.carbonGuess) {
        this.carbonPredict();
        this.playAudio();
      }
    }, 100);
  }

  public goToNext() {
    this.demoService.saveGuess(this.frame).subscribe(_ => {/*do nothing*/});
    this.next.emit();

    if (this.index > -1 && this.index < this.frames.length - 1) {
      this.fireNextFrame();
    }
  }

  public goToPrevious() {
    this.previous.emit();
  }

  private carbonPredict() {
    this.predictionService
      .predict(this.frame.file.file_key)
      .map(PredictionService.distill)
      .subscribe(p => this.frame.carbonGuess = p);
  }
}
