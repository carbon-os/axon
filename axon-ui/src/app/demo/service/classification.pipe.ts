import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'classification',
})
export class ClassificationPipe implements PipeTransform {

  transform(value: number, args?: any): any {

    if (value === 0) {
      return 'ROBOT';
    } else if (value === 1) {
      return 'HUMAN';
    } else {
      return value;
    }
  }

}
