import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'success',
})
export class SuccessPipe implements PipeTransform {

  transform(value: boolean, args?: any): any {
    if (value) {
      return 'green';
    } else {
      return 'red';
    }
  }

}
