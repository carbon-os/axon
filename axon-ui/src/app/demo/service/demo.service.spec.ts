import { TestBed, inject } from '@angular/core/testing';

import { DemoService } from './demo.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthLibModule } from '../../auth/auth-lib.module';

describe('DemoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DemoService
      ],
      imports : [
        HttpClientTestingModule,
        AuthLibModule
      ]
    });
  });

  it('should be created', inject([DemoService], (service: DemoService) => {
    expect(service).toBeTruthy();
  }));
});
