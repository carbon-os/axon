import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GameFrame } from '../../../model/types/demo/GameFrame';

import '../../rxjs-operators.ts';
import { AuthService } from '../../auth/service/auth.service';
import { AudioFileDto } from '../../../model/types/demo/AudioFileDto';
import { GuessStatDto } from '../../../model/types/demo/GuessStatDto';
import { GuessDto } from '../../../model/types/demo/GuessDto';

@Injectable()
export class DemoService {

  public static STARTING_FRAME = -1;
  private static DEMO_BASE_URL = '/api/v1.0/demo';
  private static GAME_SIZE = 5;

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient,
  ) { }

  private static audioFileToNewGameFrame(a: AudioFileDto): GameFrame {
    return {
      file: {
        id: a.id,
        file_key: a.file_key,
        fileName: a.fileName,
        mimeType: a.mimeType,
        size: a.size,
        description: a.description,
        classification: a.classification,
      },
      playerGuess: null,
      carbonGuess: null,
      toDelete: false,
    };
  }

  private static gameFrameToGuess(gameFrame: GameFrame, userEmail: string): GuessDto {
    return {
      id: '',
      email: userEmail,
      file_key: gameFrame.file.file_key,
      classification: gameFrame.file.classification,
      carbonGuess: gameFrame.carbonGuess,
      playerGuess: gameFrame.playerGuess,
      toDelete: gameFrame.toDelete,
    };
  }

  public generateRandomGameFramesMock(): Observable<GameFrame[]> {
    return Observable.of([
      {
        file: {
          id: '1',
          file_key: 'Voice_1.wav',
          classification: 0,
          fileName: 'Voice_1.wav',
          mimeType: 'audio/wav',
          size: 105000,
          description: 'some description',
        },
        playerGuess: 0,
        carbonGuess: 0,
        toDelete: false,
        email: 'dan.pk.bar@gmail.com',
      },
      {
        file: {
          id: '1',
          file_key: 'Voice_1.wav',
          classification: 0,
          fileName: 'Voice_1.wav',
          mimeType: 'audio/wav',
          size: 105000,
          description: 'some description',
        },
        playerGuess: 0,
        carbonGuess: 0,
        toDelete: false,
        email: 'dan.pk.bar@gmail.com',
      },
      {
        file: {
          id: '1',
          file_key: 'Voice_1.wav',
          classification: 0,
          fileName: 'Voice_1.wav',
          mimeType: 'audio/wav',
          size: 105000,
          description: 'some description',
        },
        playerGuess: 0,
        carbonGuess: 0,
        toDelete: false,
        email: 'dan.pk.bar@gmail.com',
      },
      {
        file: {
          id: '1',
          file_key: 'Voice_1.wav',
          classification: 0,
          fileName: 'Voice_1.wav',
          mimeType: 'audio/wav',
          size: 105000,
          description: 'some description',
        },
        playerGuess: 0,
        carbonGuess: 0,
        toDelete: false,
        email: 'dan.pk.bar@gmail.com',
      },
      {
        file: {
          id: '1',
          file_key: 'Voice_1.wav',
          classification: 0,
          fileName: 'Voice_1.wav',
          mimeType: 'audio/wav',
          size: 105000,
          description: 'some description',
        },
        playerGuess: 1,
        carbonGuess: 0,
        toDelete: false,
        email: 'dan.pk.bar@gmail.com',
      },
    ]);
  }

  public generateRandomGameFrames(): Observable<GameFrame[]> {
    const url = DemoService.DEMO_BASE_URL + '/random-audio-file/' + DemoService.GAME_SIZE;

    // return DemoService.getMockGameFrames();
    return this.httpClient
      .get<AudioFileDto[]>(url)
      .map(res => res.map(DemoService.audioFileToNewGameFrame));
  }

  public saveGuess(gameFrame: GameFrame): Observable<GuessDto[]> {
    const email = this.authService.getLoggedInUserEmail();
    const guess = DemoService.gameFrameToGuess(gameFrame, email);
    const url = DemoService.DEMO_BASE_URL + '/guess';
    return this.httpClient.post<GuessDto[]>(url, guess);
  }

  public loadHistoricalData(): Observable<GuessStatDto> {
    const url = DemoService.DEMO_BASE_URL + '/guess-stats';
    return this.httpClient.get<GuessStatDto>(url);
  }
}
