import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessGameEndComponent } from './guess-game-end.component';
import { DemoService } from '../service/demo.service';
import { MatModule } from '../../mat/mat.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from '../../auth/service/auth.service';
import { AuthLibModule } from '../../auth/auth-lib.module';

describe('GuessGameEndComponent', () => {
  let component: GuessGameEndComponent;
  let fixture: ComponentFixture<GuessGameEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GuessGameEndComponent,
      ],
      providers: [
        DemoService
      ],
      imports: [
        HttpClientTestingModule,
        MatModule,
        AuthLibModule
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessGameEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
