import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DemoService } from '../service/demo.service';
import { GuessStatDto } from '../../../../../axon-server/src/model/types/demo/GuessStatDto';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { NAV_MODULE_ROUTE } from '../../app.routes';
import { CONTACT_ROUTE } from '../../navigation/navigation.routes';

@Component({
  selector: 'app-guess-game-end',
  templateUrl: './guess-game-end.component.html',
  styleUrls: [ './guess-game-end.component.scss' ],
})
export class GuessGameEndComponent implements OnInit {

  @Output() restart = new EventEmitter<void>();

  @Input() playerScore: number;
  @Input() carbonScore: number;

  public request: Observable<GuessStatDto>;

  public stats: GuessStatDto;

  public playing = {
    rr: false,
    hr: false,
    hh: false,
    rh: false,
  };

  private currentAudio: HTMLAudioElement;

  get won() {
    return this.carbonScore < this.playerScore;
  }

  get lost() {
    return this.carbonScore > this.playerScore;
  }

  get drew() {
    return this.carbonScore === this.playerScore;
  }

  constructor(
    private demoService: DemoService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  refresh() {
    this.loadHistoricalData();
  }

  playAudio(file: string) {
    this.playing[ file ] = true;

    const src = '/api/v1.0/demo/stream/' + this.stats[ file ][ 0 ].file_key;

    this.currentAudio = new Audio();
    this.currentAudio.src = src;
    this.currentAudio.load();
    this.currentAudio.play();

    this.currentAudio.addEventListener('ended', () => {
      this.playing[ file ] = false;
    });
  }

  sendRestart() {
    this.restart.emit();
  }

  routeToContact() {
    this.router.navigate([ NAV_MODULE_ROUTE, CONTACT_ROUTE ]);
  }

  loadHistoricalData() {
    this.request = this.demoService.loadHistoricalData();
    this.request.subscribe(
      res => this.stats = res,
      _ => {},
      this.request = null,
    );
  }
}
