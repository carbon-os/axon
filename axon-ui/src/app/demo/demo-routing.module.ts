import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoMainComponent } from './demo-main/demo-main.component';
import { AuthGuard } from '../auth/service/auth.guard';
import { DEMO_ROUTE, GUESS_GAME_ROUTE, RECORD_GAME_ROUTE } from './service/demo.routes';
import { GuessGameComponent } from './guess-game/guess-game.component';
import { RecordGameComponent } from './record-game/record-game.component';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

/**
 * Default route: Demo page
 *
 * @type {[{path: string; redirectTo: string; pathMatch: string} , {path: string; component: ChatComponent; pathMatch: string}]}
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: GUESS_GAME_ROUTE,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: DEMO_ROUTE,
    component: DemoMainComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: GUESS_GAME_ROUTE,
    component: GuessGameComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: RECORD_GAME_ROUTE,
    component: RecordGameComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class DemoRoutingModule {}
