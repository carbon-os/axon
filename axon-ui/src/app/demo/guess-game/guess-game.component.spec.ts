import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessGameComponent } from './guess-game.component';
import { DemoService } from '../service/demo.service';
import { MatModule } from '../../mat/mat.module';
import { GuessGameEndComponent } from '../guess-game-end/guess-game-end.component';
import { GuessGameFrameComponent } from '../guess-game-frame/guess-game-frame.component';
import { GuessGameBeginComponent } from '../guess-game-begin/guess-game-begin.component';
import { ScoreboardComponent } from '../scoreboard/scoreboard.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ClassificationPipe } from '../service/classification.pipe';
import { ScoreboardIconPipe } from '../scoreboard/scoreboard-icon.pipe';
import { AuthService } from '../../auth/service/auth.service';
import { AuthLibModule } from '../../auth/auth-lib.module';

describe('GuessGameComponent', () => {
  let component: GuessGameComponent;
  let fixture: ComponentFixture<GuessGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GuessGameComponent,
        GuessGameEndComponent,
        GuessGameFrameComponent,
        GuessGameBeginComponent,
        ScoreboardComponent,
        ClassificationPipe,
        ScoreboardIconPipe
      ],
      providers: [
        DemoService,
        ClassificationPipe,
        ScoreboardIconPipe,
      ],
      imports: [
        HttpClientTestingModule,
        MatModule,
        AuthLibModule
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
