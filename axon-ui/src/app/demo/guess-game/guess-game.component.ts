import { Component, OnInit } from '@angular/core';
import { GameFrame } from '../../../model/types/demo/GameFrame';
import { GuessResult } from '../../../model/types/demo/GuessResult';
import { DemoService } from '../service/demo.service';

@Component({
  selector: 'app-guess-game',
  templateUrl: './guess-game.component.html',
  styleUrls: [ './guess-game.component.scss' ],
})
export class GuessGameComponent implements OnInit {

  currentFrame = DemoService.STARTING_FRAME;
  gameFrames: GameFrame[] = [];

  get showBeginPanel(): boolean {
    return this.currentFrame < 0;
  }

  get showEndPanel(): boolean {
    return this.currentFrame > this.gameFrames.length - 1;
  }

  get playerGuessResults(): GuessResult[] {
    return this.gameFrames.map(f => f.playerGuess === null ? 'tbd' : (f.playerGuess === f.file.classification ? 'right' : 'wrong'));
  }

  get carbonGuessResults(): GuessResult[] {
    const guesses = this.gameFrames.map(f => f.carbonGuess === null ? 'tbd' : (f.carbonGuess === f.file.classification ? 'right' : 'wrong'));

    const carbonCount = this.gameFrames.map(f => f.carbonGuess == null ? 0 : 1).reduce((a, b) => a + b, 0);
    const playerCount = this.gameFrames.map(f => f.playerGuess == null ? 0 : 1).reduce((a, b) => a + b, 0);
    if (carbonCount > playerCount) {
      guesses[ carbonCount - 1 ] = 'tbd';
    }
    return guesses;
  }

  get playerScore(): number {
    return this.playerGuessResults.filter(r => r === 'right').length;
  }

  get carbonScore(): number {
    return this.carbonGuessResults.filter(r => r === 'right').length;
  }

  constructor(
    private demoService: DemoService,
  ) { }

  ngOnInit() {
    this.loadGameFrames();
  }

  public incrementFrame(): void {
    this.currentFrame++;
  }

  public decrementFrame(): void {
    this.currentFrame--;
  }

  public restart(): void {
    this.currentFrame = -1;
    this.loadGameFrames();
  }

  private loadGameFrames() {
    this.demoService.generateRandomGameFrames().subscribe(res => {
      this.gameFrames = res;
    });
  }

}
