import { NgModule } from '@angular/core';

import { DemoLibModule } from './demo-lib.module';
import { DemoRoutingModule } from './demo-routing.module';

@NgModule({
  imports: [
    DemoLibModule,
    DemoRoutingModule,
  ],
  exports: [
    DemoLibModule,
    DemoRoutingModule,
  ],
  providers: [],
})
export class DemoModule {
}
