import { Component, Input, OnInit } from '@angular/core';
import { GuessResult } from '../../../model/types/demo/GuessResult';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: [ './scoreboard.component.scss' ],
})
export class ScoreboardComponent implements OnInit {

  @Input() label: string;

  @Input() results: GuessResult[];

  constructor() { }

  ngOnInit() {
  }

}
