import { Pipe, PipeTransform } from '@angular/core';
import { GuessResult } from '../../../model/types/demo/GuessResult';

@Pipe({
  name: 'scoreboardIcon',
})
export class ScoreboardIconPipe implements PipeTransform {

  transform(value: GuessResult, arg: 'icon' | 'color'): any {

    if (arg === 'icon') {
      switch (value) {
        case 'right':
        case 'wrong':
          return 'lens';
        case 'tbd':
          return 'panorama_fish_eye';
      }
    }

    if (arg === 'color') {
      switch (value) {
        case 'right':
          return 'green';
        case 'wrong':
          return 'red';
        case 'tbd':
          return 'grey';
      }
    }

  }

}
