import { NgModule } from '@angular/core';

import { MatModule } from '../mat/mat.module';
import { DemoRoutingModule } from './demo-routing.module';
import { DemoMainComponent } from './demo-main/demo-main.component';
import { DemoService } from './service/demo.service';
import { SharedModule } from '../shared/shared.module';
import { PredictionIconPipe } from './demo-main/prediction-icon.pipe';
import { PredictionTextPipe } from './demo-main/prediction-text.pipe';
import { GuessGameComponent } from './guess-game/guess-game.component';
import { RecordGameComponent } from './record-game/record-game.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { ScoreboardIconPipe } from './scoreboard/scoreboard-icon.pipe';
import { GuessGameBeginComponent } from './guess-game-begin/guess-game-begin.component';
import { GuessGameEndComponent } from './guess-game-end/guess-game-end.component';
import { GuessGameFrameComponent } from './guess-game-frame/guess-game-frame.component';
import { ClassificationPipe } from './service/classification.pipe';
import { SuccessPipe } from './service/success.pipe';
import { AuthLibModule } from '../auth/auth-lib.module';

@NgModule({
  imports: [
    MatModule,
    DemoRoutingModule,
    SharedModule,
    AuthLibModule
  ],
  exports: [
    DemoMainComponent,
  ],
  declarations: [
    DemoMainComponent,
    GuessGameComponent,
    GuessGameBeginComponent,
    GuessGameEndComponent,
    GuessGameFrameComponent,
    ScoreboardComponent,
    RecordGameComponent,
    PredictionIconPipe,
    PredictionTextPipe,
    ScoreboardIconPipe,
    ClassificationPipe,
    SuccessPipe,
  ],
  providers: [
    DemoService,
    PredictionIconPipe,
    PredictionTextPipe,
    ScoreboardIconPipe,
    ClassificationPipe,
    SuccessPipe,
  ],
})
export class DemoLibModule {
}
