import { NgModule } from '@angular/core';

import { LensLibModule } from './lens-lib.module';
import { LensRoutingModule } from './lens-routing.module';

@NgModule({
  imports: [
    LensLibModule,
    LensRoutingModule,
  ],
  exports: [
    LensLibModule,
    LensRoutingModule,
  ],
  providers: [],
})
export class LensModule {
}
