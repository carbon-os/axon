import { NgModule } from '@angular/core';

import { MatModule } from '../mat/mat.module';
import { LensRoutingModule } from './lens-routing.module';
import { LensMainComponent } from './lens-main/lens-main.component';
import { LensService } from './service/lens.service';

@NgModule({
  imports: [
    MatModule,
    LensRoutingModule,
  ],
  exports: [
    LensMainComponent,
  ],
  declarations: [
    LensMainComponent,
  ],
  providers: [
    LensService,
  ],
})
export class LensLibModule {
}
