import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LensMainComponent } from './lens-main/lens-main.component';
import { AuthGuard } from '../auth/service/auth.guard';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

export const VOICE_ROUTE = 'lens';

/**
 * Default route: Lens page
 *
 * @type {[{path: string; redirectTo: string; pathMatch: string} , {path: string; component: ChatComponent; pathMatch: string}]}
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: VOICE_ROUTE,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
  {
    path: VOICE_ROUTE,
    component: LensMainComponent,
    pathMatch: 'full',
    canActivate: [ AuthGuard ],
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class LensRoutingModule {}
