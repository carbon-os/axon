import { TestBed, inject } from '@angular/core/testing';

import { LensService } from './lens.service';

describe('LensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LensService]
    });
  });

  it('should be created', inject([LensService], (service: LensService) => {
    expect(service).toBeTruthy();
  }));
});
