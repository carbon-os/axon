import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LensMainComponent } from './lens-main.component';

describe('LensMainComponent', () => {
  let component: LensMainComponent;
  let fixture: ComponentFixture<LensMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LensMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LensMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
