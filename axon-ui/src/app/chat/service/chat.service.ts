import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as io from 'socket.io-client';
import { MessageService } from './message.service';
import { MessageDto } from '../../../model/types/chat/MessageDto';
import { AuthService } from '../../auth/service/auth.service';
import { NewMessageDto } from '../../../../../axon-server/src/model/types/chat/NewMessageDto';
import { Error } from 'tslint/lib/error';

@Injectable()
export class ChatService {
  private static chatNamespace = 'ws/chat';
  private socket;
  private messages: MessageDto[] = [];
  private messageHistorySubject = new BehaviorSubject<MessageDto[]>([]);
  public messageHistory = this.messageHistorySubject.asObservable();

  private static getChatUrl() {
    throw new Error("We need to configure the api_host in the environment.prod.ts file or figure out a different way to connect");
    // return environment.api_host + ':' + environment.ws_port + '/' + ChatService.chatNamespace;
  }

  constructor(private messageService: MessageService,
              private authService: AuthService) {
    // TODO: uncomment to use
    // this.initializeSocket();
    // this.loadExistingMessages();
  }

  public initializeSocket(): void {
    console.log('connecting to chat service at ' + ChatService.getChatUrl());
    this.socket = io(ChatService.getChatUrl(), {
      extraHeaders: {
        Authorization: 'Bearer ' + this.authService.getJwtPayload(),
      },
    });

    this.socket.on('message', (data) => {
      this.messages.push(data);
      this.messageHistorySubject.next(this.messages);
    });

    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });

    this.socket.on('connect', () => {
      console.log('connected');
    });
  }

  public loadExistingMessages() {
    this.messageService.listMessages().subscribe(msgs => {
      this.messages = msgs;
      this.messageHistorySubject.next(this.messages);
    });
  }

  public sendMessage(textContent: string, threadUuid: string): void {
    console.log('sending message content: ' + textContent);

    const messageDto: NewMessageDto = {
      jwt: this.authService.getEncodedJwt(),
      threadUuid: threadUuid,
      content: {
        text: textContent,
        command: '',
      },
    };

    this.socket.emit('add-message', messageDto);
  }

  public listenForMessages(): Observable<MessageDto[]> {
    return this.messageHistory;
  }

  public disconnect(): void {
    this.socket.disconnect();
  }
}
