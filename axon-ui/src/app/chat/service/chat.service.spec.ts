import { inject, TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';
import { MessageService } from './message.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';

describe('ChatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ChatService,
        MessageService,
        AuthService,
      ],
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
    });
  });

  it('should be created', inject([ ChatService ], (service: ChatService) => {
    expect(service).toBeTruthy();
  }));
});
