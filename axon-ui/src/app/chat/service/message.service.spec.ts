import { inject, TestBed } from '@angular/core/testing';

import { MessageService } from './message.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';

describe('MessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MessageService,
        AuthService,
      ],
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
    });
  });

  it('should be created', inject([ MessageService ], (service: MessageService) => {
    expect(service).toBeTruthy();
  }));
});
