import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import '../../rxjs-operators';
import { MessageDto } from '../../../model/types/chat/MessageDto';

@Injectable()
export class MessageService {

  private messageUrl = '/api/v1.0/message';

  constructor(private httpClient: HttpClient) { }

  public listMessages(): Observable<MessageDto[]> {
    return this.httpClient.get<MessageDto[]>(this.messageUrl);
  }
}
