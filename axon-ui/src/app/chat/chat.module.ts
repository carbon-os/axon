import { NgModule } from '@angular/core';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatLibModule } from './chat-lib.module';

@NgModule({
  imports: [
    ChatLibModule,
    ChatRoutingModule,
  ],
  exports: [
    ChatLibModule,
    ChatRoutingModule,
  ],
  providers: [],
})
export class ChatModule {
}
