import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat/chat.component';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

export const CHAT_ROUTE = 'chat';

/**
 * Default route: Chat Page
 *
 * @type {[{path: string; redirectTo: string; pathMatch: string} , {path: string; component: ChatComponent; pathMatch: string}]}
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: CHAT_ROUTE,
    pathMatch: 'full',
  },
  {
    path: CHAT_ROUTE,
    component: ChatComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class ChatRoutingModule {}
