import { Pipe, PipeTransform } from '@angular/core';
import { MessageDto } from '../../../model/types/chat/MessageDto';

@Pipe({
  name: 'showUserTimestamp',
})
export class ShowUserTimestampPipe implements PipeTransform {

  private static readonly MAX_TIME = 1000 * 60 * 5;

  transform(messages: MessageDto[], i: number): any {
    if (!messages || !i) {
      return true;
    }
    if (i < 0 || i >= messages.length || !messages[ i ].timestamp) {
      return true;
    }

    return messages[ i ].user !== messages[ i - 1 ].user
      ||
      messages[ i ].timestamp - messages[ i - 1 ].timestamp > ShowUserTimestampPipe.MAX_TIME;
  }

}
