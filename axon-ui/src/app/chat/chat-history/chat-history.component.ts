import { Component, OnInit } from '@angular/core';
import { ChatService } from '../service/chat.service';
import { Observable } from 'rxjs/Observable';
import { MessageDto } from '../../../model/types/chat/MessageDto';

@Component({
  selector: 'app-chat-history',
  templateUrl: './chat-history.component.html',
  styleUrls: [ './chat-history.component.scss' ],
})
export class ChatHistoryComponent implements OnInit {
  public messages$: Observable<MessageDto[]>;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.messages$ = this.chatService.listenForMessages();
  }
}
