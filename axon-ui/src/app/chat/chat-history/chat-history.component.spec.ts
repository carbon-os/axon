import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatHistoryComponent } from './chat-history.component';
import { MatModule } from '../../mat/mat.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChatService } from '../service/chat.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';
import { MessageService } from '../service/message.service';
import { ShowUserTimestampPipe } from './show-user-timestamp.pipe';

describe('ChatHistoryComponent', () => {
  let component: ChatHistoryComponent;
  let fixture: ComponentFixture<ChatHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChatHistoryComponent,
        ShowUserTimestampPipe,
      ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        RouterTestingModule,
        HttpClientModule, HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        ChatService,
        MessageService,
        AuthService,
        ShowUserTimestampPipe,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
