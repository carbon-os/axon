import { NgModule } from '@angular/core';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatInputComponent } from './chat-input/chat-input.component';
import { ChatHistoryComponent } from './chat-history/chat-history.component';
import { ChatComponent } from './chat/chat.component';
import { ChatService } from './service/chat.service';
import { MatModule } from '../mat/mat.module';
import { MessageService } from './service/message.service';
import { ShowUserTimestampPipe } from './chat-history/show-user-timestamp.pipe';

@NgModule({
  imports: [
    MatModule,
    ChatRoutingModule,
  ],
  exports: [
    ChatComponent,
    ShowUserTimestampPipe
  ],
  declarations: [
    ChatInputComponent,
    ChatHistoryComponent,
    ChatComponent,
    ShowUserTimestampPipe
  ],
  providers: [
    ChatService,
    MessageService,
    ShowUserTimestampPipe
  ],
})
export class ChatLibModule {
}
