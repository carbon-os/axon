import { Component, OnInit } from '@angular/core';
import { ChatService } from '../service/chat.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: [ './chat-input.component.scss' ],
})
export class ChatInputComponent implements OnInit {
  public newMessage: string;
  public chatInput: FormGroup;

  get newMessageControlEmpty() {
    return this.chatInput.get('newMessageControl').hasError('required');
  }

  constructor(private chatService: ChatService) {
    this.chatInput = new FormGroup({
      newMessageControl: new FormControl('', [ Validators.required ]),
    });
  }

  ngOnInit() {
  }

  public send() {
    if (!this.chatInput.valid) {
      return;
    }

    const message = this.chatInput.get('newMessageControl').value;
    this.chatService.sendMessage(message, 'all');
    this.chatInput.setValue({'newMessageControl': ''});
  }
}
