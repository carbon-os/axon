import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatComponent } from './chat.component';
import { MatModule } from '../../mat/mat.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChatHistoryComponent } from '../chat-history/chat-history.component';
import { ChatInputComponent } from '../chat-input/chat-input.component';
import { ChatService } from '../service/chat.service';
import { ShowUserTimestampPipe } from '../chat-history/show-user-timestamp.pipe';
import { MessageService } from '../service/message.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from '../../auth/service/auth.service';
import { CookieModule } from 'ngx-cookie';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChatComponent,
        ChatHistoryComponent,
        ChatInputComponent,
        ShowUserTimestampPipe,
      ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        AuthService,
        ChatService,
        MessageService,
        ShowUserTimestampPipe,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
