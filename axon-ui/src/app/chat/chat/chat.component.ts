import { AfterViewChecked, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: [ './chat.component.scss' ],
})
export class ChatComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollableHistory') private scrollableHistory: ElementRef;

  scrollToBottom(): void {
    try {
      this.scrollableHistory.nativeElement.scrollTop = this.scrollableHistory.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
