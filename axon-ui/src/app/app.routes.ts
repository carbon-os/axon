import { Routes } from '@angular/router';
import { AuthGuard } from './auth/service/auth.guard';
import { LOGIN_STATE } from '../model/states/login.state';
import { WELCOME_STATE } from '../model/states/welcome.state';
import { LANDING_STATE } from '../model/states/landing.state';
import { NotFoundComponent } from './navigation/not-found/not-found.component';
import { NOT_FOUND_STATE } from '../model/states/not-found.state';

export const USER_MODULE_ROUTE = 'u';
export const AUTH_MODULE_ROUTE = 'a';
export const NAV_MODULE_ROUTE = 'n';
export const CHAT_MODULE_ROUTE = 'c';
export const DEMO_MODULE_ROUTE = 'd';
export const VOICE_MODULE_ROUTE = 'v';
export const LENS_MODULE_ROUTE = 'l';

export const HOME_PAGE_ROUTE = DEMO_MODULE_ROUTE;

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: NAV_MODULE_ROUTE,
    pathMatch: 'full',
  },
  {
    path: LANDING_STATE,
    redirectTo: NAV_MODULE_ROUTE,
    pathMatch: 'full',
  },
  {
    path: LOGIN_STATE,
    redirectTo: AUTH_MODULE_ROUTE,
    pathMatch: 'full',
  },
  {
    path: WELCOME_STATE,
    redirectTo: USER_MODULE_ROUTE,
    pathMatch: 'full',
  },
  {
    path: NAV_MODULE_ROUTE,
    loadChildren: 'app/navigation/navigation.module#NavigationModule',
  },
  {
    path: AUTH_MODULE_ROUTE,
    loadChildren: 'app/auth/auth.module#AuthModule',
  },
  {
    path: USER_MODULE_ROUTE,
    loadChildren: 'app/user/user.module#UserModule',
    canActivate: [ AuthGuard ],
  },
  // {
  //   path: CHAT_MODULE_ROUTE,
  //   loadChildren: 'app/chat/chat.module#ChatModule',
  //   canActivate: [ AuthGuard ],
  // },
  {
    path: DEMO_MODULE_ROUTE,
    loadChildren: 'app/demo/demo.module#DemoModule',
    canActivate: [ AuthGuard ],
  },
  {
    path: VOICE_MODULE_ROUTE,
    loadChildren: 'app/voice/voice.module#VoiceModule',
    canActivate: [ AuthGuard ],
  },
  {
    path: LENS_MODULE_ROUTE,
    loadChildren: 'app/lens/lens.module#LensModule',
    canActivate: [ AuthGuard ],
  },
  {
    path: NOT_FOUND_STATE,
    component: NotFoundComponent,
  },
];
