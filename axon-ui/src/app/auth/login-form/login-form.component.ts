import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';
import { AuthService } from '../service/auth.service';
import { AUTH_MODULE_ROUTE, HOME_PAGE_ROUTE } from '../../app.routes';
import { CHECK_EMAIL_ROUTE, LOGIN_ROUTE } from '../auth-routing.module';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: [ './login-form.component.scss' ],
})
export class LoginFormComponent implements OnInit {
  public loginForm: FormGroup;
  public hidePassword = true;
  public passwordlessLogin = true;
  public error = false;
  public redirectUrl: string;

  get emailControlEmpty() {
    return this.loginForm.get('emailControl').hasError('required');
  }

  get emailErrorMessage() {
    return this.loginForm.get('emailControl').hasError('required') ? 'You must enter a value' :
      this.loginForm.get('emailControl').hasError('email') ? 'Not a valid email' : '';
  }

  get passwordErrorMessage() {
    return this.loginForm.get('passwordControl').hasError('minlength') ? 'Must be at least 10 characters' : '';
  }

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.initializeFormGroup();
    this.activatedRoute.queryParams
      .filter(params => params.redirectUrl)
      .subscribe(params => {
        this.redirectUrl = params.redirectUrl;
      });
  }

  public initializeFormGroup() {
    this.loginForm = new FormGroup({
      emailControl: new FormControl('', [ Validators.required, Validators.email ]),
      passwordControl: new FormControl('', []),
    });
  }

  public submit() {
    console.log('logging in...');
    this.error = false;
    const uad: UserAuthenticationDto = {
      email: this.loginForm.get('emailControl').value,
      password: this.loginForm.get('passwordControl').value,
      redirectUrl: this.redirectUrl,
    };
    if (this.passwordlessLogin) {
      this.sendLoginLinkToEmail(uad);
    } else {
      this.loginWithPassword(uad);
    }
  }

  public routeToNextPage() {
    if (this.redirectUrl) {
      this.router.navigate(this.redirectUrl.split('/'));
    } else {
      this.router.navigate([ HOME_PAGE_ROUTE ]);
    }
  }

  public routeToCheckEmailPage() {
    this.router.navigate([ '..', CHECK_EMAIL_ROUTE ], {relativeTo: this.activatedRoute});
  }

  reload() {
    window.location.reload();
  }

  public togglePasswordlessLogin() {
    this.passwordlessLogin = !this.passwordlessLogin;
  }

  private sendLoginLinkToEmail(uad: UserAuthenticationDto) {
    this.authService
      .requestLoginLink(uad)
      .subscribe(
        _ => this.routeToCheckEmailPage(),
        err => this.error = err.error,
      );
  }

  private loginWithPassword(uad: UserAuthenticationDto) {
    this.authService
      .renewJwt(uad)
      .subscribe(
        _ => this.routeToNextPage(),
        err => this.error = err.error,
      );
  }
}
