import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { MatModule } from '../mat/mat.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './service/auth.service';
import { JwtHandlerComponent } from './jwt-handler/jwt-handler.component';
import { AuthGuard } from './service/auth.guard';
import { CheckEmailComponent } from './check-email/check-email.component';
import { AuthInterceptor } from './service/auth.interceptor';

@NgModule({
  imports: [
    RouterModule,
    MatModule,
  ],
  declarations: [
    LoginComponent,
    SignupComponent,
    SignupFormComponent,
    LoginFormComponent,
    CheckEmailComponent,
    JwtHandlerComponent,
  ],
  exports: [
    SignupFormComponent,
    LoginFormComponent,
    JwtHandlerComponent,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    AuthService,
    AuthGuard,
  ],
})
export class AuthLibModule {
}
