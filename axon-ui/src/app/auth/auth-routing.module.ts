import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { JwtHandlerComponent } from './jwt-handler/jwt-handler.component';
import { CheckEmailComponent } from './check-email/check-email.component';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

export const LOGIN_ROUTE = 'login';
export const SIGNUP_ROUTE = 'invite';
export const CHECK_EMAIL_ROUTE = 'check-email';
export const JWT_HANDLER_ROUTE = 'jwt';

/**
 *
 * Default route: Login page
 *
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: LOGIN_ROUTE,
    pathMatch: 'full',
  },
  {
    path: LOGIN_ROUTE,
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: SIGNUP_ROUTE,
    component: SignupComponent,
    pathMatch: 'full',
  },
  {
    path: CHECK_EMAIL_ROUTE,
    component: CheckEmailComponent,
    pathMatch: 'full',
  },
  {
    path: JWT_HANDLER_ROUTE,
    component: JwtHandlerComponent,
    pathMatch: 'full',
  },
  {
    path: '*',
    redirectTo: NOT_FOUND_STATE,
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class AuthRoutingModule {}
