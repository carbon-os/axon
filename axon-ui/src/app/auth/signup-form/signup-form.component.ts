import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CHECK_EMAIL_ROUTE } from '../auth-routing.module';

import { PRIVACY_POLICY_ROUTE, TOS_ROUTE } from '../../navigation/navigation.routes';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: [ './signup-form.component.scss' ],
})
export class SignupFormComponent implements OnInit {

  @Input() submitLabel = 'Sign Up';
  public signupForm: FormGroup;

  public hidePassword = true;
  public requestComplete = false;

  get emailControlEmpty() {
    return this.signupForm.get('emailControl').hasError('required');
  }

  get emailErrorMessage() {
    return this.signupForm.get('emailControl').hasError('required') ? 'You must enter a value' :
      this.signupForm.get('emailControl').hasError('email') ? 'Not a valid email' : '';
  }

  get passwordErrorMessage() {
    return this.signupForm.get('passwordControl').hasError('minlength') ? 'Must be at least 8 characters' : '';
  }

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private authService: AuthService) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      emailControl: new FormControl('', [ Validators.required, Validators.email ]),
      // passwordControl: new FormControl('', [ Validators.required, Validators.minLength(8) ]),
    });
  }

  public submit() {
    const uad: UserAuthenticationDto = {
      email: this.signupForm.get('emailControl').value,
      // password: this.signupForm.get('passwordControl').value,
    };
    this.authService
      .requestLoginLink(uad)
      .subscribe(
        _ => this.routeToCheckEmailPage(),
        err => console.error('error with signup: ' + JSON.stringify(err)),
      );
  }

  public routeToTos() {
    this.router.navigate([ TOS_ROUTE ]);
  }

  public routeToCheckEmailPage() {
    this.router.navigate([ '..', CHECK_EMAIL_ROUTE ], {relativeTo: this.activatedRoute});
  }

  public routeToPrivacyPolicy() {
    this.router.navigate([ PRIVACY_POLICY_ROUTE ]);
  }
}
