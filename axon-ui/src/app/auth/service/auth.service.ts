import { Injectable } from '@angular/core';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';
import { HttpClient } from '@angular/common/http';
import '../../rxjs-operators';
import { CookieOptions, CookieService } from 'ngx-cookie';
import { Observable } from 'rxjs/Observable';
import { JsonWebTokenPayload } from '../../../model/types/application/JsonWebTokenPayload';
import { EncodedJwt } from '../../../../../axon-server/src/model/types/application/EncodedJwt';

@Injectable()
export class AuthService {

  public static readonly JWT_KEY = 'jwt';

  private static readonly JWT_COOKIE_OPTIONS: CookieOptions = {
    // secure: true,
    // domain: environment.api_host,
  };

  private static AUTH_URLS = {
    JWT: '/api/v1.0/auth/jwt',
    LOGIN_LINK: '/api/v1.0/auth/get-login-link',
    RESET_PASSWORD: '/api/v1.0/auth/update-password',
  };

  private latestJwt: JsonWebTokenPayload;

  private static jwtValid(jwt: JsonWebTokenPayload): boolean {
    // console.log('checking if JWT is still valid...');
    // console.log(JSON.stringify(jwt));
    // if (jwt && jwt.estimated_expiration) {
    //   console.log('estimated expiration: ' + jwt.estimated_expiration);
    //   console.log(jwt.estimated_expiration > Date.now());
    //   console.log(jwt.estimated_expiration - Date.now());
    // }

    return jwt && jwt.estimated_expiration && jwt.estimated_expiration > Date.now();
  }

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService) { }

  public renewJwt(uad: UserAuthenticationDto): Observable<JsonWebTokenPayload> {
    // console.log('posting new jwt to the server...');
    return this.httpClient
      .post<JsonWebTokenPayload>(AuthService.AUTH_URLS.JWT, uad)
      .do(jwt => {
        if (jwt) {
          // console.log('saving jwt: ' + JSON.stringify(jwt));
          this.latestJwt = jwt;
          this.cookieService.putObject(AuthService.JWT_KEY, jwt);
        } else {
          console.error('could not get jwt from server');
        }
      });
  }

  public isUserLoggedIn(): boolean {
    const jwt = this.getJwtPayload();
    return !!jwt;
  }

  public getLoggedInUserEmail(): string {
    const jwt = this.getJwtPayload();
    if (!jwt) {
      throw new Error('You should not be calling the getLoggedInUser method if the user does not have a valid jwt');
    }
    return jwt.email;
  }

  public getLoggedInUserGuid(): string {
    const jwt = this.getJwtPayload();
    if (!jwt) {
      throw new Error('You should not be calling the getLoggedInUser method if the user does not have a valid jwt');
    }
    return jwt.uuid;
  }

  public requestLoginLink(uad: UserAuthenticationDto): Observable<any> {
    return this.httpClient.post(AuthService.AUTH_URLS.LOGIN_LINK, uad);
  }

  public getJwtPayload(): JsonWebTokenPayload {
    return AuthService.jwtValid(this.latestJwt) ? this.latestJwt : this.hardRefreshFromCookies();
  }

  public getEncodedJwt(): EncodedJwt {
    const jwt = this.getJwtPayload();
    return jwt != null ? jwt.access_token : null;
  }

  public setPasswordUpdate(): void {
    this.latestJwt.hasPassword = true;
    // console.log('putting new jwt');
    this.cookieService.putObject(AuthService.JWT_KEY, this.latestJwt);
  }

  public deleteCurrentJwt(): void {
    // console.log('deleting jwt...');
    this.latestJwt = null;
    this.cookieService.remove(AuthService.JWT_KEY);
  }

  public resetPassword(uad: UserAuthenticationDto): Observable<Object> {
    return this.httpClient.put(AuthService.AUTH_URLS.RESET_PASSWORD, uad);
  }

  private hardRefreshFromCookies(): JsonWebTokenPayload {
    const jwt = this.cookieService.getObject(AuthService.JWT_KEY);
    // console.log('loading jwt from cookies: ' + jwt);
    if (!AuthService.jwtValid(<JsonWebTokenPayload>jwt)) {
      // console.log('jwt is not valid, going to delete');
      this.deleteCurrentJwt();
    } else {
      this.latestJwt = <JsonWebTokenPayload>jwt;
      return this.latestJwt;
    }
  }
}
