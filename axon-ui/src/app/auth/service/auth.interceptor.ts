import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { LOGIN_STATE } from '../../../model/states/login.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private router: Router) { }

  public intercept(req: HttpRequest<any>,
                   next: HttpHandler): Observable<HttpEvent<any>> {

    if (!req.url.startsWith('/api')) {
      return next.handle(req);
    }

    req = req.clone({
      setHeaders: {
        'Content-Type': 'application/json',
      },
    });

    const jwt = this.authService.getEncodedJwt();

    if (jwt) {
      req = req.clone({
        setHeaders: {
          'Authorization': `Bearer ${jwt}`,
        },
      });
    }

    return next
      .handle(req)
      .do(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // do stuff with response if you want
          }
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              console.log('unauthorized to make request');
              this.authService.deleteCurrentJwt();
              this.router.navigate([ LOGIN_STATE ]);
            }
          }
        },
      );
  }

}
