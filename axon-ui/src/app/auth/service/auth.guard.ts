import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { LOGIN_STATE } from '../../../model/states/login.state';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isUserLoggedIn()) {
      // console.log('user is logged in');
      return true;
    } else {
      // console.log('user is not logged in');
      this.router.navigate([ LOGIN_STATE ], {queryParams: {redirectUrl: state.url}});
      return false;
    }
  }
}
