import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthLibModule } from './auth-lib.module';

@NgModule({
  imports: [
    AuthLibModule,
    AuthRoutingModule,
  ],
  exports: [
    AuthLibModule,
    AuthRoutingModule,
  ],
})
export class AuthModule {}
