import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { UserAuthenticationDto } from '../../../model/types/application/UserAuthenticationDto';

@Component({
  selector: 'app-jwt-handler',
  templateUrl: './jwt-handler.component.html',
  styleUrls: [ './jwt-handler.component.scss' ],
})
export class JwtHandlerComponent implements OnInit {

  public error = false;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private authService: AuthService) { }

  ngOnInit() {
    this.subscribeToRoute();
  }

  private subscribeToRoute() {
    this.activatedRoute.queryParams.subscribe(params => {
      console.log('logging in using params...' + JSON.stringify(params));

      if (params[ 'e' ] && params[ 't' ]) {

        const uad: UserAuthenticationDto = {
          email: params[ 'e' ],
          emailToken: params[ 't' ],
        };

        const redirectUrl = params[ 'r' ].split('/');

        this.authService
          .renewJwt(uad)
          .subscribe(
            jwt => this.router.navigate(redirectUrl),
            err => {
              this.error = true;
            },
          );
      }
    });
  }
}
