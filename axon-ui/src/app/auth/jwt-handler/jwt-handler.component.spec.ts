import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JwtHandlerComponent } from './jwt-handler.component';

import { AuthService } from '../service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CookieModule } from 'ngx-cookie';
import { RouterTestingModule } from '@angular/router/testing';

describe('JwtHandlerComponent', () => {
  let component: JwtHandlerComponent;
  let fixture: ComponentFixture<JwtHandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        JwtHandlerComponent,
      ],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        CookieModule.forRoot(),
      ],
      providers: [
        AuthService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JwtHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
