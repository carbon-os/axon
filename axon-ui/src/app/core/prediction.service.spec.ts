import { TestBed, inject } from '@angular/core/testing';

import { PredictionService } from './prediction.service';
import { FileUploadService } from './file-upload.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PredictionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PredictionService,
        FileUploadService,
      ],
      imports: [
        HttpClientTestingModule,
      ],
    });
  });

  it('should be created', inject([ PredictionService ], (service: PredictionService) => {
    expect(service).toBeTruthy();
  }));
});
