import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FileUploadService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  public postFile<T>(url: string, fileToUpload: File): Observable<T> {

    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    //
    // const headers = new HttpHeaders();
    // headers.append('Content-Type', 'multipart/form-data');

    return this.httpClient.post<T>(url, formData);
  }

  // upload(formData, options, id) {
  //   let headers = this.tokenService.currentAuthHeaders;
  //   headers.delete('Content-Type');
  //   let options = new RequestOptions({ headers: headers });
  //
  //   return this.httpClient.request({
  //     method: 'post',
  //     url: `http://localhost:3000/api/projects/${id}/upload`,
  //     body: formData,
  //     headers: options.headers
  //   }).map(res => res.json());
  // }
}
