import { TestBed, inject } from '@angular/core/testing';

import { FileUploadService } from './file-upload.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

describe('FileUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
      ],
      providers: [
        FileUploadService,
      ],
    });
  });

  it('should be created', inject([ FileUploadService ], (service: FileUploadService) => {
    expect(service).toBeTruthy();
  }));
});
