import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadService } from './file-upload.service';
import { PredictionService } from './prediction.service';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  exports: [],
  providers: [
    FileUploadService,
    PredictionService
  ],
})
export class CoreModule {}
