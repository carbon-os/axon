import { Injectable } from '@angular/core';
import { FileUploadService } from './file-upload.service';
import { Observable } from 'rxjs/Observable';
import { Prediction } from '../../model/types/predict/Prediction';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PredictionService {

  private static PREDICT_FILE_URL = '/ai/predict';
  private static PREDICT_URL = '/api/v1.0/demo/predict';

  constructor(
    private fileUploadService: FileUploadService,
    private httpClient: HttpClient,
  ) { }

  public static distill(prediction: Prediction): number {
    if (PredictionService.leansHuman(prediction)) {
      return 1;
    } else if (PredictionService.leansRobot(prediction)) {
      return 0;
    } else {
      return 1; // if it's split exactly 50/50 we'll say "Human" in the demo
    }
  }

  public static leansHuman(prediction: Prediction) {
    return prediction.prediction[ 0 ] > prediction.prediction[ 1 ];
  }

  public static leansRobot(prediction: Prediction) {
    return prediction.prediction[ 0 ] < prediction.prediction[ 1 ];
  }

  public predictFile(file: File): Observable<Prediction> {
    return this.fileUploadService.postFile<Prediction>(PredictionService.PREDICT_URL, file);
  }

  public predict(file_key: string): Observable<Prediction> {
    const url = PredictionService.PREDICT_URL + '/' + file_key;
    return this.httpClient.get<Prediction>(url);
  }
}
