import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatDatepickerModule,
  MatProgressSpinnerModule,
  MatChipsModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


const MATERIAL_MODULES = [
  CommonModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatListModule,
  MatInputModule,
  FlexLayoutModule,
  MatSidenavModule,
  MatCardModule,
  MatDialogModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatProgressSpinnerModule,
  MatChipsModule
];

@NgModule({
  imports: [
    ...MATERIAL_MODULES,
  ],
  exports: [
    ...MATERIAL_MODULES,
  ],
})
export class MatModule {
}
