import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NAV_MODULE_ROUTE } from '../../app.routes';
import { CONTACT_ROUTE, PRIVACY_POLICY_ROUTE, TOS_ROUTE } from '../navigation.routes';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: [ './footer.component.scss' ],
})
export class FooterComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public routeToPrivacyPolicy() {
    this.router.navigate([ NAV_MODULE_ROUTE, PRIVACY_POLICY_ROUTE ]);
  }

  public routeToTos() {
    this.router.navigate([ NAV_MODULE_ROUTE, TOS_ROUTE ]);
  }

  public routeToContact() {
    this.router.navigate([ NAV_MODULE_ROUTE, CONTACT_ROUTE ]);
  }
}
