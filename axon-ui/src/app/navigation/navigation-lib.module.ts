import { NgModule } from '@angular/core';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SplashComponent } from './splash/splash.component';
import { RouterModule } from '@angular/router';

import { TosComponent } from './tos/tos.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AboutComponent } from './about/about.component';
import { MatModule } from '../mat/mat.module';
import { ContactComponent } from './contact/contact.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  imports: [
    RouterModule,
    MatModule
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    NotFoundComponent
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SplashComponent,
    TosComponent,
    PrivacyPolicyComponent,
    AboutComponent,
    ContactComponent,
    NotFoundComponent
  ],
})
export class NavigationLibModule {
}
