import { Component, OnInit } from '@angular/core';
import { FadeInAnimation } from '../animations/fade-in.animation';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: [ './not-found.component.scss' ],
  animations: [ FadeInAnimation ],
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
