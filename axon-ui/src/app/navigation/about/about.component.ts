import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AUTH_MODULE_ROUTE, HOME_PAGE_ROUTE, NAV_MODULE_ROUTE } from '../../app.routes';
import { FadeInAnimation } from '../animations/fade-in.animation';
import { CONTACT_ROUTE, SPLASH_ROUTE } from '../navigation.routes';
import { SIGNUP_ROUTE } from '../../auth/auth-routing.module';
import { ImageMove } from '../animations/image-move.animation';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: [ './about.component.scss' ],
  animations: [
    FadeInAnimation,
    ImageMove
  ],
})
export class AboutComponent implements OnInit {


  routeToHome() {
    this.router.navigate([ HOME_PAGE_ROUTE ]);
  }

  routeToContact() {
    this.router.navigate([ NAV_MODULE_ROUTE, CONTACT_ROUTE ]);
  }

  routeToSignup() {
    this.router.navigate([ AUTH_MODULE_ROUTE, SIGNUP_ROUTE ]);
  }


  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public navigateToSplash() {
    this.router.navigate([ NAV_MODULE_ROUTE, SPLASH_ROUTE ]);
  }

}
