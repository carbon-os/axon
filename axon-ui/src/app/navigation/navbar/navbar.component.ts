import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LOGIN_ROUTE, SIGNUP_ROUTE } from '../../auth/auth-routing.module';
import { AUTH_MODULE_ROUTE, DEMO_MODULE_ROUTE, LENS_MODULE_ROUTE, NAV_MODULE_ROUTE, USER_MODULE_ROUTE, VOICE_MODULE_ROUTE } from '../../app.routes';
import { CONTACT_ROUTE, SPLASH_ROUTE } from '../navigation.routes';
import { AuthService } from '../../auth/service/auth.service';
import { USER_PROFILE_ROUTE } from '../../user/user.routes';
import { DEMO_ROUTE, GUESS_GAME_ROUTE, RECORD_GAME_ROUTE } from '../../demo/service/demo.routes';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [ './navbar.component.scss' ],
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router,
              private authService: AuthService) {
  }

  get userLoggedIn(): boolean {
    return this.authService.isUserLoggedIn();
  }

  ngOnInit() {
  }

  public navigateToProfile() {
    this.router.navigate([ USER_MODULE_ROUTE, USER_PROFILE_ROUTE ]);
  }

  public navigateToDemo() {
    this.router.navigate([ DEMO_MODULE_ROUTE, DEMO_ROUTE ]);
  }

  public navigateToGuess() {
    this.router.navigate([ DEMO_MODULE_ROUTE, GUESS_GAME_ROUTE ]);
  }

  public navigateToContact() {
    this.router.navigate([ NAV_MODULE_ROUTE, CONTACT_ROUTE ]);
  }

  public navigateToRecord() {
    this.router.navigate([ DEMO_MODULE_ROUTE, RECORD_GAME_ROUTE ]);
  }

  public navigateToVoice() {
    this.router.navigate([ VOICE_MODULE_ROUTE ]);
  }

  public navigateToLens() {
    this.router.navigate([ LENS_MODULE_ROUTE ]);
  }

  public logout() {
    this.authService.deleteCurrentJwt();
    this.router.navigate([ NAV_MODULE_ROUTE, SPLASH_ROUTE ]);
  }

  public navigateToSplash() {
    this.router.navigate([ NAV_MODULE_ROUTE, SPLASH_ROUTE ]);
  }

  public navigateToSignup() {
    this.router.navigate([ AUTH_MODULE_ROUTE, SIGNUP_ROUTE ]);
  }

  public navigateToLogin() {
    this.router.navigate([ AUTH_MODULE_ROUTE, LOGIN_ROUTE ]);
  }
}
