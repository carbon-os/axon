import { trigger, state, animate, transition, style, query, stagger } from '@angular/animations';

export const ImageMove =
  // trigger name for attaching this animation to an element using the [@triggerName] syntax
  trigger('photosAnimation', [
    transition('* => *', [
      query('img', style({transform: 'translateX(-100%)'})),
      query('img', style({transform: 'translateY(-100%)'})),
    ]),
  ]);
