import { NgModule } from '@angular/core';

import { NavigationRoutingModule } from './navigation-routing.module';
import { NavigationLibModule } from './navigation-lib.module';

@NgModule({
  imports: [
    NavigationRoutingModule,
    NavigationLibModule
  ],
  exports: [
    NavigationRoutingModule,
    NavigationLibModule
  ],
})
export class NavigationModule {
}
