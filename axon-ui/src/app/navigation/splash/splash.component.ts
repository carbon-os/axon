import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FadeInAnimation } from '../animations/fade-in.animation';
import { NAV_MODULE_ROUTE } from '../../app.routes';
import { ABOUT_ROUTE } from '../navigation.routes';
import { ImageMove } from '../animations/image-move.animation';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss'],
  animations: [
    FadeInAnimation,
    ImageMove
  ],
})
export class SplashComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  navigateToAbout() {
    this.router.navigate([ NAV_MODULE_ROUTE, ABOUT_ROUTE ]);
  }
}
