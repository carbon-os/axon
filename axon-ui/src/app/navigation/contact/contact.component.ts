import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SIGNUP_ROUTE } from '../../auth/auth-routing.module';
import { AUTH_MODULE_ROUTE, HOME_PAGE_ROUTE } from '../../app.routes';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  routeToHome() {
    this.router.navigate([ HOME_PAGE_ROUTE ]);
  }

  routeToSignup() {
    this.router.navigate([ AUTH_MODULE_ROUTE, SIGNUP_ROUTE ]);
  }

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

}
