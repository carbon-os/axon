import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SplashComponent } from './splash/splash.component';
import { AboutComponent } from './about/about.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TosComponent } from './tos/tos.component';
import { ABOUT_ROUTE, CONTACT_ROUTE, PRIVACY_POLICY_ROUTE, SPLASH_ROUTE, TOS_ROUTE } from './navigation.routes';
import { ContactComponent } from './contact/contact.component';
import { NOT_FOUND_STATE } from '../../model/states/not-found.state';

/**
 * Default route: Splash Page
 *
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: SPLASH_ROUTE,
    pathMatch: 'full',
  },
  {
    path: SPLASH_ROUTE,
    component: SplashComponent,
    pathMatch: 'full',
  },
  {
    path: ABOUT_ROUTE,
    component: AboutComponent,
    pathMatch: 'full',
  },
  {
    path: PRIVACY_POLICY_ROUTE,
    component: PrivacyPolicyComponent,
    pathMatch: 'full',
  },
  {
    path: TOS_ROUTE,
    component: TosComponent,
    pathMatch: 'full',
  },
  {
    path: CONTACT_ROUTE,
    component: ContactComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class NavigationRoutingModule {}
