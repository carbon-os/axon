import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TosComponent } from './tos.component';
import { MatModule } from '../../mat/mat.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TosComponent', () => {
  let component: TosComponent;
  let fixture: ComponentFixture<TosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TosComponent ],
      imports: [
        BrowserAnimationsModule,
        MatModule,
        RouterTestingModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
