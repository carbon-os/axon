export const ABOUT_ROUTE = 'about';
export const PRIVACY_POLICY_ROUTE = 'privacy-policy';
export const SPLASH_ROUTE = 'home';
export const TOS_ROUTE = 'terms-of-service';
export const CONTACT_ROUTE = 'contact';
