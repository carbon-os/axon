import { NgModule } from '@angular/core';

import { MatModule } from '../mat/mat.module';
import { VoiceRoutingModule } from './voice-routing.module';
import { VoiceMainComponent } from './voice-main/voice-main.component';
import { VoiceService } from './service/voice.service';

@NgModule({
  imports: [
    MatModule,
    VoiceRoutingModule,
  ],
  exports: [
    VoiceMainComponent,
  ],
  declarations: [
    VoiceMainComponent,
  ],
  providers: [
    VoiceService,
  ],
})
export class VoiceLibModule {
}
