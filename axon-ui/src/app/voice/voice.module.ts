import { NgModule } from '@angular/core';

import { VoiceLibModule } from './voice-lib.module';
import { VoiceRoutingModule } from './voice-routing.module';

@NgModule({
  imports: [
    VoiceLibModule,
    VoiceRoutingModule,
  ],
  exports: [
    VoiceLibModule,
    VoiceRoutingModule,
  ],
  providers: [],
})
export class VoiceModule {
}
