import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoiceMainComponent } from './voice-main.component';

describe('VoiceMainComponent', () => {
  let component: VoiceMainComponent;
  let fixture: ComponentFixture<VoiceMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoiceMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoiceMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
