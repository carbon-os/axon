import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: [ './file-input.component.scss' ],
})
export class FileInputComponent implements OnInit {

  @Input() label = 'Select Files';

  @Input() accept: string[];

  @Output() select = new EventEmitter<FileList>();

  constructor() { }

  ngOnInit() {
  }

  public handleFileInput(files: FileList): void {
    this.select.emit(files);
  }
}
