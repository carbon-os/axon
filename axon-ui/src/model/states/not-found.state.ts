import { APP_STATE } from './state';

export const NOT_FOUND_STATE: APP_STATE = 'not-found';
