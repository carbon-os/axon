We symlink to the server models so we can share certain models and app states with the server.

use:
```bash
cd /carbon-os/workspace/axon/axon-ui/src/model
ln -s /carbon-os/workspace/axon/axon-server/src/model/states states
```
