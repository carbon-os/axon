import { SignupPage } from './signup.po';

describe('signup page', () => {
  let page: SignupPage;

  beforeEach(() => {
    page = new SignupPage();
    page.navigateTo();
  });

  it('should have an email input box', () => {
    expect(page.getEmailInput()).not.toBeNull();
  });

  it('should have a submit button that says Sign Up', () => {
    expect(page.getSubmitButton()).toEqual('Sign Up');
  });
});
