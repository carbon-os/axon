import { browser, by, element } from 'protractor';
import { promise } from 'selenium-webdriver';

export class SignupPage {

  public async navigateTo(): Promise<void> {
    return await browser.get('/a/signup');
  }

  public getEmailInput(): promise.Promise<string> {
    return element(by.css('#email')).getText();
  }

  public getSubmitButton(): promise.Promise<string> {
    return element(by.css('#submit')).getText();
  }
}
