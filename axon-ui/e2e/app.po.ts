import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/n/home');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getTitle() {
    return element(by.css('#title')).getText();
  }

  getSubtitle() {
    return element(by.css('#subtitle')).getText();
  }
}
