# Angular UI

Quickstart:
```bash
yarn install
```

Develop:
```bash
yarn dev:watch
## -or-
yarn test:watch
## -or-
yarn e2e:watch
```

Test:
```bash
yarn lint
yarn test
yarn e2e
```

Build:
```bash
yarn build
```
