# CarbonOS
#### turing test; by robots for humans

## Quick Start
You can execute similar commands in both axon-ui and axon-server:

```bash
yarn install
yarn dev:watch
```

## Issue Tracking
Mark-up relevant places in the code like this: `TODO:axon-13`, where `axon` is the project name and `13` is the story id.
Wire up the create-a-story link so the bar for creating new tasks is low: ie. Cmd+Space+"carbon" brings up: `https://gitlab.com/carbon-os/axon/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=`  

## Full Start
If this is the first time going through this, make sure your dev machine is up to date and follow: [DEV_DEPENDENCIES.md](https://gitlab.com/psdm/axon/blob/master/DEV_DEPENDENCIES.md)

#### 1. clone the codebase

```bash
cd /carbon-os/workspace
```

using git over ssh if you've set up an SSH key ([recommended](https://docs.gitlab.com/ee/ssh/))
```bash
git clone git@gitlab.com:psdm/axon.git
```

or https:
```bash
git clone https://gitlab.com/psdm/axon.git
```

#### 2. install dependencies
```bash
cd /carbon-os/workspace/axon/axon-server
yarn install

cd /carbon-os/workspace/axon/axon-ui
yarn install
```

#### 3. run the nest server
```bash
cd /carbon-os/workspace/axon/axon-server
yarn dev:watch
```

#### 4. run the angular app
open a new tab in the terminal
```bash
cd /carbon-os/workspace/axon/axon-ui
yarn dev:watch
```

visit the app on [http://localhost:4200](http://localhost:4200)

Note that you can now change parts of the codebase in axon-server and axon-ui and see the app rebuild in the terminal and your changes reflected locally.


## Deployment
```bash
./scripts/test.sh
./scripts/build.sh
./scripts/stop-server.sh axon-stage
./scripts/deploy.sh axon-stage
./scripts/start-server.sh axon-stage
```


# Miscellaneous Notes
*various notes to track setup that only happens once but we may want to remember for the future*

##### Setting up SSH Keys with Gitlab CI
1. https://docs.gitlab.com/ee/ci/ssh_keys/
1. https://docs.gitlab.com/ee/ci/ssh_keys/#verifying-the-ssh-host-keys